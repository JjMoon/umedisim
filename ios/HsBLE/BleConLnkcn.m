//
//  BleCon.m
//  MediSimETT
//
//  Created by JJTTMOON on 2017. 2. 27..
//  Copyright © 2017년 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UMediSim-Swift.h"

#import "BleConLnkcn.h"
#import <React/RCTLog.h>

@interface BleConLnkcn() {
  //RCTResponseSenderBlock theCallback;
}

@end

////////////////////////////////////////////////////     _//////////_     [ BleCon ]    _//////////_
@implementation BleConLnkcn
////////////////////////////////////////////////////     _//////////_

-(instancetype)init {
  self = [super init];
  
  
  return self;
}


RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(testLog:(NSString *)str )
{
  RCTLogInfo(@" BleConLnkcn :: testLog >>>>       %@ ", str);
}

RCT_EXPORT_METHOD(initBle)
{
  RCTLogInfo(@" BleConLnkcn :: initBle >>>>     ");
  [BleManLnkcn.singl bleInitialJob];
}

RCT_EXPORT_METHOD(startScan:(NSString *)devName)
{
  RCTLogInfo(@"\n\n  Start Scan ...  dev name :: %@...  ", devName);
  BleManLnkcn.singl.devName = devName;
  
  [BleManLnkcn.singl startScan];
}

RCT_EXPORT_METHOD(stopScan)
{
  [BleManLnkcn.singl stopScan];
}

RCT_EXPORT_METHOD(finishConnection)
{
  [BleManLnkcn.singl finishConnection];
}

RCT_EXPORT_METHOD(connectCheck:(RCTResponseSenderBlock)callback) {
  BleManLnkcn.singl.connectCheckCB = callback;
  [BleManLnkcn.singl connectCheck];
  //callback(@[[NSNull null], [BleManLnkcn.singl fetch]]);
}

//RCT_EXPORT_METHOD(connected:(RCTResponseSenderBlock)callback) {
//  [BleManLnkcn.singl connectHMSoft];
//  NSArray *rArr;
//  if (BleManLnkcn.singl.connected) {
//    rArr = [NSArray arrayWithObjects:[NSNumber numberWithInteger:1], nil];
//    callback(@[[NSNull null], rArr]);
//  } else {
//    rArr = [NSArray arrayWithObjects:[NSNumber numberWithInteger:0], nil];
//    callback(@[[NSNull null], rArr]);
//  }
//}


RCT_EXPORT_METHOD(packet:(RCTResponseSenderBlock)callback) {
  callback(@[[NSNull null], [BleManLnkcn.singl fetch]]);
}


RCT_EXPORT_METHOD(fetchPacket:(RCTResponseSenderBlock)callback) { // 콜백 저장 후 사용.
  BleManLnkcn.singl.rctCallback = callback;
}

@end
