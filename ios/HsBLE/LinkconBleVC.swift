//
//  HsBleController.swift
//  LinkconBleFW
//
//  Created by Jongwoo Moon on 2016. 10. 20..
//
//

import Foundation
import UIKit
import CoreBluetooth


/**
 
 링크콘에서 제공한 라이브러리를 실제 사용하는 방법입니다. 
 
 > 일단 libBLE.a 파일을 Frameworks 폴더에 복사합니다.
 
 > BLESerialComManager.h 를 포함시킵니다. (싱글톤으로 쓰실 거면 없어도 될 듯)
   포트 여러개를 열어야 하면 인스턴스를 생성해야 하므로 아래처럼 그냥 객체로 생성해서 쓰시면 됩니다.
   이때 initBLECentralManager 함수를 public으로 만들어줘야 합니다. BLESerialComManager.h 에 포함시켜 줍니다.
 
 > 순서는 아래 설명을 참고하세요


 */

class MainVC : UIViewController, BLESerialComManagerDelegate {

    var comMan = BLESerialComManager()
    var port = BLEPort()

    var allPorts = [BLEPort]()

    @IBOutlet weak var bttnConnectKit: UIButton!

    @IBAction func actionOpenPort(_ sender: AnyObject) {
        print("  actionOpenPort  ")

        comMan.startEnumeratePorts(5.0)  /// 여기서 포트 검색 startScan 과 같은 의미.. 를 시작합니다.
    }


    /// 연결하는 부분..
    @IBAction func actionConnect(_ sender: AnyObject) {
        print("\n\n\n startOpen..   port num : \(allPorts.count)\n\n ")

        let p6021 = allPorts.filter({ (prt) -> Bool in
            prt.name == "TheName"  /// 여기에 내가 정한 이름을 넣으면 되겠죠..
        })

        if p6021.count == 0 {
            print("p6021.count == 0")
            return
        }

        let param = paramsPackage4Open()
        port = p6021.first!
        comMan.startOpen(port, with: param)

    }

    /// 데이터를 보냅니다.
    @IBAction func checkKit(_ sender: UIButton) {
        let bytes: [UInt8] = [ 0x02,0x03 ]  //
        let dt = NSData(bytes: bytes as [UInt8], length: 2)
        print("\n\n  송신 Data : \(dt)  \n\n")
        comMan.write(dt as Data!, to: port)
    }



    /// 여기가 처음이겠죠..  매니저를 생성하고 딜리깃을 설정합니다.
    override func viewDidLoad() {
        comMan.delegate = self
        var openParams = paramsPackage4Configure()
        openParams.lengthOfPackage = 10
        comMan.configure(openParams)
        comMan.initBLECentralManager()
    }


    /// 아래는 프로토콜 함수들입니다.  함수들이 다 설명해 줍니다.

    func bleSerilaComManagerDidStateChange(_ bleSerialComManager: BLESerialComManager!) {
        print("bleSerilaComManagerDidStateChange :: ")
    }

    func bleSerialComManager(_ bleSerialComManager: BLESerialComManager!, didDataReceivedOn port: BLEPort!, withLength length: UInt32) {
    }

    func bleSerilaComManagerDidEnumComplete(_ bleSerialComManager: BLESerialComManager!) {
        print("bleSerilaComManagerDidEnumComplete :: ")
    }

    func bleSerilaComManager(_ bleSerialComManager: BLESerialComManager!, didFound port: BLEPort!) {
        print("\n\n\n didFound port :: \(port.name)  \n\n\n")

        allPorts.append(port)
    }

    func bleSerialComManager(_ bleSerialComManager: BLESerialComManager!, didClosedPort port: BLEPort!) {
        print("didClosedPort :: ")
    }

    /// 포트에 접속하면 이 콜백이 불립니다.  이때부터 써주시면 됩니다..
    func bleSerilaComManager(_ bleSerialComManager: BLESerialComManager!, didOpen port: BLEPort!, withResult result: resultCodeType) {
        print("didOpen port :: ")
    }
}
