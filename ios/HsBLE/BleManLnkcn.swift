//
//  BleMan.swift
//  MediSimETT
//
//  Created by JJTTMOON on 2017. 4. 12..
//  Copyright © 2017년 Facebook. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

private let _singletoneObj = BleManLnkcn()
////////////////////////////////////////////////////     _//////////_     [ BleHm10Man ]    _//////////_
@objc
class BleManLnkcn : NSObject, BLESerialComManagerDelegate {
  
  var connectCheckCB: RCTResponseSenderBlock? = nil
  var rctCallback: RCTResponseSenderBlock? = nil
  var devName = "", isETT = false
  
  var arrPeri = [BLEPort]()
  var comMan = BLESerialComManager()
  var port: BLEPort?
  
  var connected: Bool {
    get { return port != nil && (port?.state == PORT_STATE.STATE_OPENED || port?.state == PORT_STATE.STATE_OPENING) }
  }
  
  var connectResult: RCTResponseSenderBlock?
  
  /// singletone obj : Stt.singl
  class var singl: BleManLnkcn { return _singletoneObj }
  
  override init() {
    super.init()
    print("\n\n\n  BleHm10Man  init \n\n")
  }
  
  ////////////////////////////////////////////////////       [ Connect ]    _//////////_
  func startScan() {
    //let index = devName.index(devName.startIndex, offsetBy: 6) 스트링 인덱스 소스..
    //let nameHead = devName.substring(to: index) // MediET
    //isETT = nameHead == "MediET"
    isETT = devName.hasPrefix("MediETT")
    print("  \n\n\n\n\n  \n\n\n\n\n  \(#function)   isETT ? \(isETT)  >> \n\n\n\n\n ")
    port = nil;
    arrPeri = [BLEPort]()
    comMan.startEnumeratePorts(7.0)  /// 여기서 포트 검색 startScan 과 같은 의미.. 를 시작합니다.
  }
  
  func stopScan() {
    print("  \n\n\n\n\n  \n\n\n\n\n  \(#function)     >> \n\n\n\n\n ")
    comMan.stopEnumeratePorts()
    port = nil;
    arrPeri = [BLEPort]()
  }
  
  func finishConnection() {
    print("\n\n  >> \(#function)  ")
    
    if connected {
      print("\n\n  >> \(#function)   현재 연결 중. ..  끊기.. ")
      comMan.closePort(port!)
    }
  }
  
  ////////////////////////////////////////////////////       [ 패킷 처리 ]    _//////////_
  var receivedPack: [UInt8] = [0, 0, 0], fetched = true
  
  func fetch() -> [UInt8] { // 타이머로 가져가는 형태..  안 씀..
    if fetched { // 이미 가져갔으므로 0 리턴.
      return [0, 0, 0]
    }
    // 새로 수신된 패킷이 있슴.
    fetched = true
    return receivedPack
  }
  
  func logString(_ str: String) {
    print(" >>>  \(str)")
  }
  
  func bleInitialJob() {
    comMan.delegate = self
    var openParams = paramsPackage4Configure()
    openParams.lengthOfPackage = 10
    comMan.configure(openParams)
    comMan.initBLECentralManager()
  }
  
  func connectCheck() {
    let thePort = arrPeri.filter({ (peri) -> Bool in return peri.name == devName })
    if thePort.count > 0 { // 찾았슴..
      let param = paramsPackage4Open()
      port = thePort.first!
      
      comMan.startOpen(self.port!, with: param)
      print("  >> \(#function)   연결 시작..    \(devName) \n\n\n")
      
      if connectCheckCB != nil {
        print("connectCheckCB != nil ")
        connectCheckCB!([NSNull()])
      }
    }
  }
  
  /// 아래는 프로토콜 함수들입니다.  함수들이 다 설명해 줍니다.
  func bleSerilaComManagerDidStateChange(_ bleSerialComManager: BLESerialComManager!) {
    print("bleSerilaComManagerDidStateChange :: ")
  }
  
  func bleSerialComManager(_ bleSerialComManager: BLESerialComManager!, didDataReceivedOn port: BLEPort!, withLength length: UInt32) {
    let receivedData = [UInt8](comMan.readData(from: port, withLength: Int32(length)))
    print("\n\n  데이터 수신  ...  \(receivedData) L : \(length)   on : \(port.name)  ")
    
    fetched = false
    if rctCallback != nil {
      print("\(#function) :: rctCallback != nil  JS 로 보냄.. .. \n")
      rctCallback!([NSNull(), receivedData])
      rctCallback = nil
    }
  }
  
  func bleSerilaComManagerDidEnumComplete(_ bleSerialComManager: BLESerialComManager!) {
    print("bleSerilaComManagerDidEnumComplete :: ")
  }
  // 포트 검출...
  func bleSerilaComManager(bleSerialComManager: BLESerialComManager!, didFoundPort port: BLEPort!) {
    print("\n\n\n didFound port :: \(port.name)   didFoundPort   \n\n\n")
    
    
    arrPeri.append(port)
  }
  func bleSerilaComManager(_ bleSerialComManager: BLESerialComManager!, didFound port: BLEPort!) {
    print("\n\n\n didFound port :: \(port.name)  \n\n\n")
    
    arrPeri.append(port)
  }
  
  func bleSerialComManager(_ bleSerialComManager: BLESerialComManager!, didClosedPort port: BLEPort!) {
    print("didClosedPort :: ")
  }
  
  /// 포트에 접속하면 이 콜백이 불립니다.  이때부터 써주시면 됩니다..
  func bleSerilaComManager(_ bleSerialComManager: BLESerialComManager!, didOpen port: BLEPort!, withResult result: resultCodeType) {
    print("didOpen port :: ")
  }
  
  func bleSerilaComManagerDidEnumComplete(bleSerialComManager: BLESerialComManager!) {
    print("bleSerilaComManagerDidEnumComplete :: ")
    
  }
}
