//
//  BleCon.h
//  MediSimETT
//
//  Created by JJTTMOON on 2017. 2. 27..
//  Copyright © 2017년 Facebook. All rights reserved.
//

#ifndef BleCon_h
#define BleCon_h
#endif /* BleCon_h */

#import <React/RCTBridgeModule.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "UMediSim-Bridging-Header.h"


@interface BleCon : NSObject <RCTBridgeModule>

@end


/// Called when a new peripheral is discovered while scanning. Also gives the RSSI (signal strength)
//func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?)
