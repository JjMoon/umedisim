//
//  BleMan.swift
//  MediSimETT
//
//  Created by JJTTMOON on 2017. 2. 27..
//  Copyright © 2017년 Facebook. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

private let _singletoneObj = BleHm10Man()
////////////////////////////////////////////////////     _//////////_     [ BleHm10Man ]    _//////////_
@objc
class BleHm10Man : NSObject, BluetoothSerialDelegate {
  
  var rctCallback: RCTResponseSenderBlock? = nil
  
  var arrPeri = [CBPeripheral]()
  var connected: Bool {
    get { return serialObj.isReady }
  }
  
  var connectResult: RCTResponseSenderBlock?
  
  /// singletone obj : Stt.singl
  class var singl: BleHm10Man { return _singletoneObj }
  
  override init() {
    super.init()
    print("\n\n\n  BleHm10Man  init \n\n")
    
    serialObj = BluetoothSerial(delegate: self)
  }
  
  ////////////////////////////////////////////////////       [ Connect ]    _//////////_
  
  
  ////////////////////////////////////////////////////       [ 패킷 처리 ]    _//////////_
  var receivedPack: [UInt8] = [0, 0, 0], fetched = true
  
  func fetch() -> [UInt8] { // 타이머로 가져가는 형태..  안 씀..
    if fetched { // 이미 가져갔으므로 0 리턴.
      return [0, 0, 0]
    }
    // 새로 수신된 패킷이 있슴.
    fetched = true
    return receivedPack
  }
  
  func logString(_ str: String) {
    print(" >>>  \(str)")
  }
  
  ////////////////////////////////////////////////////       [ BLE Connection, delegates ]
  func startScan() {
    serialObj.startScan()
  }
  
  func connectHMSoft() {
    print("\n\n\n  \(#function) connectHMSoft   start scan")
    arrPeri = [CBPeripheral]()
    serialObj.startScan()
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      print("  >> arrPeri.count :: \(self.arrPeri.count)")
      
      self.connectResult?([12, 23, self.arrPeri.count]);
      
      //let hmSoft = self.arrPeri.filter({ (peri) -> Bool in return peri.name == "HMSoft" })
      let hmSoft = self.arrPeri.filter({ (peri) -> Bool in return peri.name == "bde spp dev" })
      
      print("  >> \(#function) connectHMSoft   ")
      
      if hmSoft.count > 0 {
        serialObj.connectToPeripheral(hmSoft[0])
        print("  >> \(#function) connectHMSoft  연결 시작..   connectToPeripheral(hmSoft[0])  \n\n\n")
      }
    }
  }
  
  /// Called when de state of the CBCentralManager changes (e.g. when bluetooth is turned on/off)
  func serialDidChangeState() {
  }
  
  /// Called when a peripheral disconnected
  func serialDidDisconnect(_ peripheral: CBPeripheral, error: NSError?) {
    
  }
  
  // ** Optionals **
  
  /// Called when a message is received
  func serialDidReceiveString(_ message: String) {
    
  }
  
  /// Called when a message is received   수신된 패킷 저장..
  func serialDidReceiveBytes(_ bytes: [UInt8]) {
    fetched = false
    receivedPack = bytes
    
    if rctCallback != nil {
      //print("\(#function) :: received ..   calback .. ")
      rctCallback!([NSNull(), receivedPack])
    }
  }
  
  /// Called when a message is received
  func serialDidReceiveData(_ data: Data) {
    
  }
  
  /// Called when the RSSI of the connected peripheral is read
  func serialDidReadRSSI(_ rssi: NSNumber) {
    
  }
  
  /// Called when a new peripheral is discovered while scanning. Also gives the RSSI (signal strength)
  func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?) {
    
    print("\n\n\n  serialDidDiscoverPeripheral ::  \(peripheral)  RSSI : \(RSSI) ")
    arrPeri.append(peripheral)
    
  }
  
  /// Called when a peripheral is connected (but not yet ready for cummunication)
  func serialDidConnect(_ peripheral: CBPeripheral) {
    // connected = true
    print("\n\n\n\n\n  serialDidConnect  \(peripheral.name)")
  }
  
  /// Called when a pending connection failed
  func serialDidFailToConnect(_ peripheral: CBPeripheral, error: NSError?) {
    
  }
  
  /// Called when a peripheral is ready for communication
  func serialIsReady(_ peripheral: CBPeripheral) {
    
  }
  
}
