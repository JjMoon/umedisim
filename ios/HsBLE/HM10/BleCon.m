//
//  BleCon.m
//  MediSimETT
//
//  Created by JJTTMOON on 2017. 2. 27..
//  Copyright © 2017년 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UMediSim-Swift.h"

#import "BleCon.h"
#import <React/RCTLog.h>

@interface BleCon() {
  //RCTResponseSenderBlock theCallback;
}

@end

////////////////////////////////////////////////////     _//////////_     [ BleCon ]    _//////////_
@implementation BleCon
////////////////////////////////////////////////////     _//////////_

-(instancetype)init {
  self = [super init];
  
  
  return self;
}


RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location)
{
  RCTLogInfo(@"Pretending to create an event %@ at %@", name, location);
}


//RCT_EXPORT_METHOD(log:(NSString *)str val:(nonnull NSNumber *)val)
RCT_EXPORT_METHOD(log:(NSString *)str val:(NSNumber *)val)
{
  RCTLogInfo(@"Log  %@ at %@", str, val);
  //[BleHm10Man.singl logStringWithStr:str];
  [BleHm10Man.singl logString:str];
}


RCT_EXPORT_METHOD(startScan)
{
  RCTLogInfo(@"\n\n  Start Scan ...  Macro...");
  [BleHm10Man.singl startScan];
}

RCT_EXPORT_METHOD(connectHMSoft)
{
  RCTLogInfo(@"\n\n  Connect to HM Soft  ...  Macro...");
  [BleHm10Man.singl connectHMSoft];
}


RCT_EXPORT_METHOD(connect:(RCTResponseSenderBlock)callback) {
  [BleHm10Man.singl connectHMSoft];
  BleHm10Man.singl.connectResult = callback;
  //callback(@[[NSNull null], [BleHm10Man.singl fetch]]);
}

RCT_EXPORT_METHOD(connected:(RCTResponseSenderBlock)callback) {
  [BleHm10Man.singl connectHMSoft];
  NSArray *rArr;
  if (BleHm10Man.singl.connected) {
    rArr = [NSArray arrayWithObjects:[NSNumber numberWithInteger:1], nil];
    callback(@[[NSNull null], rArr]);
  } else {
    rArr = [NSArray arrayWithObjects:[NSNumber numberWithInteger:0], nil];
    callback(@[[NSNull null], rArr]);
  }
}


RCT_EXPORT_METHOD(packet:(RCTResponseSenderBlock)callback) {
  callback(@[[NSNull null], [BleHm10Man.singl fetch]]);
}


RCT_EXPORT_METHOD(packetAlt:(RCTResponseSenderBlock)callback) { // 콜백 저장 후 사용.
  BleHm10Man.singl.rctCallback = callback;
}

  //RCTLogInfo(@"\n\n\n 패킷 전송. ");
  //NSArray *arr = [NSArray arrayWithObjects:[NSNumber numberWithInteger:5], [NSNumber numberWithInteger:11], nil];
  //callback(@[[NSNull null], BleHm10Man.singl.receivedPack]);

  //BleHm10Man.singl.senderBlck = callback;

@end
