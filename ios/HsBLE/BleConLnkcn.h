//
//  BleCon.h
//  MediSimETT
//
//  Created by JJTTMOON on 2017. 2. 27..
//  Copyright © 2017년 Facebook. All rights reserved.
//

#ifndef BleConLnkcn_h
#define BleConLnkcn_h
#endif /* BleConLnkcn_h */

#import <React/RCTBridgeModule.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "UMediSim-Bridging-Header.h"


@interface BleConLnkcn : NSObject <RCTBridgeModule>

@end


/// Called when a new peripheral is discovered while scanning. Also gives the RSSI (signal strength)
//func serialDidDiscoverPeripheral(_ peripheral: CBPeripheral, RSSI: NSNumber?)
