package com.umedisim;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bde.parentcyTransport.ACSUtility;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MainActivity extends ReactActivity {

    public static MainActivity Inst;

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */

    BleLinkconPackage linkconPackage;

    @Override
    protected String getMainComponentName() {
        return "UMediSim";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println(" __ "); System.out.println(" __ ");
        System.out.println("  >>>>> >>>>> >>>>> >>>>> >>>>> >>>>> >>>>> >>>>> >>>>>   MainActivity 생성  ");
        System.out.println(" __ "); System.out.println(" __ ");

        Inst = this;
    }


}
