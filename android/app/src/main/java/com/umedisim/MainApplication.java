package com.umedisim;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.chirag.RNMail.RNMail;
//import com.parkerdan.htmltopdf.RNHTMLtoPDFPackage;
import com.christopherdro.htmltopdf.RNHTMLtoPDFPackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
import io.realm.react.RealmReactPackage;
// import com.react.rnspinkit.RNSpinkitPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  BleLinkconPackage linkconPackage = new BleLinkconPackage();

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNMail(),
            new RNHTMLtoPDFPackage(),
            new RNViewShotPackage(),
              new RealmReactPackage(),
            linkconPackage
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();


    SoLoader.init(this, /* native exopackage */ false);
  }
}
