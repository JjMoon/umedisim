package com.umedisim;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bde.parentcyTransport.ACSUtility;
import com.bde.parentcyTransport.ACSUtility.blePort;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.facebook.react.uimanager.PixelUtil;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;



/**
 * Created by moon on 2017. 4. 3..
 */

public class BleLinkconModule extends ReactContextBaseJavaModule {

    // ETT, BP 관련.
    private String portName;
    private boolean isETT;

    private ReactContext mReactContext;

    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";

    List<BluetoothDevice> deviceList;
    private DeviceAdapter deviceAdapter;

    private ACSUtility utility;
    private boolean isPortOpen = false, isReceiveHex = false, isSendHex = false, isClear = true;
    private ACSUtility.blePort mCurrentPort, mSelectedPort;
    private String mReceivedData, mSendData;
    private int mTotalReceiveSize = 0, mLastSecondTotalReceiveSize = 0;
    private int mTotalSendSize = 0;
    private int mInternal = 0;
    private boolean utilAvaliable;

    private ArrayList<blePort> arrPort;

    public BleLinkconModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;

        arrPort = new ArrayList<blePort>();
    }

    @Override
    public String getName() {
        return "BleLinkconModuleAndroid";
    }

    @ReactMethod
    public void initialJob() {
        //Toast.makeText(getReactApplicationContext(), "Initial Job in Android", 5).show();
        System.out.println(" __ "); System.out.println(" __ ");
        System.out.println("  initialJob ");
        System.out.println(" __ "); System.out.println(" __  임시로  bde spp dev    new ACSUtility  ");

        utility = new ACSUtility(MainActivity.Inst, userCallback);
        
        arrPort.clear();
    }

    private Callback connectCallback, fetchCallback;

    @ReactMethod
    public void connect(String devName, int deviceID, Callback errorCallback, Callback successCallback) {
        try {
            //portName = devName + deviceID;

            if (devName.equals("ETT")) {
                isETT = true;
                //portName = "bde spp dev"; // 아직 시험 케이스.
                portName = "MediETT_" + deviceID; // MediBP_10001
                System.out.println(" connectETT  >>>  " + portName);
            } else {
                isETT = false;
                portName = "MediBP_" + deviceID; // MediBP_10001
                System.out.println(" connectBP  >>>  " + portName);
            }

            Toast.makeText(getReactApplicationContext(), portName, Toast.LENGTH_SHORT).show(); // 입력된 기기 번호 확인용

            //utility = new ACSUtility(MainActivity.Inst, userCallback);
            utility.enumAllPorts(5);
            populateList();
            connectCallback = successCallback;

            connectTo();

        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }
    
    private void connectTo() {
        for (blePort p: arrPort) {
            if (p._device.getName().equals(portName)) {
                Toast.makeText(getReactApplicationContext(), "Connecting to  .. " + p._device.getName(), Toast.LENGTH_SHORT).show();
                utility.openPort(p);
                break;
            }
        }
    }

    @ReactMethod
    public void sendBpPacket(ReadableArray pack) {
        System.out.println(" send packet >>>  " + pack);

        Toast.makeText(getReactApplicationContext(), "Pack  " + pack, Toast.LENGTH_LONG).show();

        byte[] arrPck = new byte[9];

        for (int k=0; k < pack.size(); k++) {
            arrPck[k] = (byte)pack.getInt(k);
        }

        utility.writePort(arrPck);
    }

    @ReactMethod
    public void finishConnection() {
        try {
            if (utility.isPortOpen(mCurrentPort))
                utility.closePort();
            else {
                Toast.makeText(getReactApplicationContext(), "Port was closed !!", 2).show();
                mCurrentPort = null;
            }
        } catch (IllegalViewOperationException e) {
            Toast.makeText(getReactApplicationContext(), "Cut BLE job Failed", 3).show();
        }
    }

    @ReactMethod
    public void fetch(Callback errorCallback, Callback successCallback) {
        try {
            System.out.println("Fetch ");

            fetchCallback = successCallback;

        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }
    }


    private void populateList() {
        /* Initialize device list container */

        deviceList = new ArrayList<BluetoothDevice>();
        deviceAdapter = new DeviceAdapter(MainActivity.Inst, deviceList);

        Log.d(TAG, "populateList :: 디바이스 개수 : " + deviceList.size() );

        //devRssiValues = new HashMap<String, Integer>();

        //ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        //newDevicesListView.setAdapter(deviceAdapter);
        //newDevicesListView.setOnItemClickListener(mDeviceClickListener);

        /*
        Return the set of BluetoothDevice objects that are bonded (paired) to the local adapter.
		If Bluetooth state is not STATE_ON, this API will return an empty set.
		After turning on Bluetooth, wait for ACTION_STATE_CHANGED with STATE_ON to get the updated value.
         */
        /*Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
        for (BluetoothDevice pairedDevice : pairedDevices) {
            boolean result = false;

         result = mBtAdapter.isBLEDevice(pairedDevice);
            if (result == true) {
                addDevice(pairedDevice, 0);
            //}
        }*/
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants;
    }

    private ACSUtility.IACSUtilityCallback userCallback = new ACSUtility.IACSUtilityCallback() {

        @Override
        public void didFoundPort(blePort newPort) {
            // TODO Auto-generated method stub

            Log.d(TAG, "didFoundPort :: 포트 발견 : " + newPort._device.getName() );
            Log.d(TAG, "didFoundPort :: 포트 발견 : " + portName);

            // Toast.makeText(getReactApplicationContext(), "Found.. " + newPort._device.getName(), Toast.LENGTH_SHORT).show();
            if (newPort._device != null && newPort._device.getName() != null) {
//                if (newPort._device.getName().equals(portName)) {
//                    Log.d(TAG, "didFoundPort :: 포트 발견 :  연결 시도 " + portName);
//
//                    Toast.makeText(getReactApplicationContext(), "Connecting to  .. " + newPort._device.getName(), Toast.LENGTH_SHORT).show();
//
//                    utility.openPort(newPort);
//                } else {
//                    Toast.makeText(getReactApplicationContext(), "Found : " + newPort._device.getName(), Toast.LENGTH_SHORT).show();
//                }
                arrPort.add(newPort);
                connectTo();
            }
        }

        @Override
        public void didFinishedEnumPorts() {
            // TODO Auto-generated method stub

            Log.d(TAG, "didFinishedEnumPorts :: 포트 검색 종료 ");

        }

        @Override
        public void didOpenPort(blePort port, Boolean bSuccess) {
            // TODO Auto-generated method stub
            Log.d(TAG, "The port is open ? " + bSuccess);
            if (bSuccess)  {

                Toast.makeText(getReactApplicationContext(), portName + " is Opened !!!", 5).show();

                isPortOpen = true;
                mCurrentPort = port;

                if (connectCallback != null) {
                    System.out.println(" __  연결 완료 콜백 호출   " + mCurrentPort._device.getName());
                    connectCallback.invoke();
                }


//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        // TODO Auto-generated method stub
//                        updateUiObject();
//                        showSuccessDialog();
//                        getProgressDialog().cancel();
//                    }
//                });
            } else {
//                getProgressDialog().cancel();
//                showFailDialog();
            }

        }



        @Override
        public void didClosePort(blePort port) {
            // TODO Auto-generated method stub
            isPortOpen = false;
            mCurrentPort = null;

            Toast.makeText(getReactApplicationContext(), "Port did closed !! ", 2).show();
//            if (getProgressDialog().isShowing()) {
//                getProgressDialog().dismiss();
//            }
            //Toast.makeText(MainActivity.this, "Disconnected from Peripheral", Toast.LENGTH_SHORT).show();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    updateUiObject();
                }
            });
        }

        @Override
        public void didPackageReceived(blePort port, byte[] packageToSend) {
            // System.out.println("  didPackageReceived  " +  packageToSend.length);

            if (packageToSend.length == 8) { // ETT
                WritableArray params = Arguments.createArray();
                for (int k=0; k<8; k++) {
                    params.pushInt(packageToSend[k] & 0xFF);
                }
                sendEvent(mReactContext, "received", params);
            }
            if (packageToSend.length == 7) { // BP
                WritableArray params = Arguments.createArray();
                for (int k=0; k<7; k++) {
                    params.pushInt(packageToSend[k] & 0xFF);
                }
                sendEvent(mReactContext, "receivedBP", params);
            }
        }

        @Override
        public void heartbeatDebug() {
            // TODO Auto-generated method stub

        }

        @Override
        public void utilReadyForUse() {
            // TODO Auto-generated method stub
            Log.d(TAG, "utilReadyForUse..  enumAllPorts");
            utilAvaliable = true;
            utility.enumAllPorts(10);
            Log.d(TAG, "utilReadyForUse..  enumAllPorts  ...  done ");
        }

        @Override
        public void didPackageSended(boolean succeed) {
            // TODO Auto-generated method stub
            if (succeed) {
                //Toast.makeText(MainActivity.this, "���ݷ��ͳɹ�", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(MainActivity.this, "���ݷ���ʧ��", Toast.LENGTH_SHORT).show();
            }
        }

    };


    private void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableArray params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    private void updateUiObject() {
        // TODO Auto-generated method stub
//        if (isPortOpen) {
//            mOpenOrCloseBtn.setText("Close");
//        }
//        else {
//            mOpenOrCloseBtn.setText("Open");
//        }
//        mSendCountTV.setText(mTotalSendSize + "");
//        mReceiveCountTV.setText(mTotalReceiveSize + "");
//        //mReceiveRateTV.setText((mTotalReceiveSize - mLastSecondTotalReceiveSize) + " B/s");
//
//        mReceiveDataTV.setText(mReceivedData);
//
//
//        if (mSelectedPort != null) {
//            mSelectedPortTV.setText(mSelectedPort._device.getName());
//        }
//        scrollToBottom();
		/*if (isPortOpen) {

		}*/
    }

    class DeviceAdapter extends BaseAdapter {
        Context context;
        List<BluetoothDevice> devices;
        LayoutInflater inflater;

        public DeviceAdapter(Context context, List<BluetoothDevice> devices) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            this.devices = devices;
        }

        @Override
        public int getCount() {
            return devices.size();
        }

        @Override
        public Object getItem(int position) {
            return devices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
//            ViewGroup vg;
//
//            if (convertView != null) {
//                vg = (ViewGroup) convertView;
//            } else {
//                vg = (ViewGroup) inflater.inflate(R.layout.device_element, null);
//            }
//
//            BluetoothDevice device = devices.get(position);
//            final TextView tvadd = ((TextView) vg.findViewById(R.id.address));
//            final TextView tvname = ((TextView) vg.findViewById(R.id.name));
//            final TextView tvpaired = (TextView) vg.findViewById(R.id.paired);
//            //final TextView tvrssi = (TextView) vg.findViewById(R.id.rssi);
//
//            //tvrssi.setVisibility(View.VISIBLE);
//            /*byte rssival = (byte) devRssiValues.get(device.getAddress()).intValue();
//            if (rssival != 0) {
//                tvrssi.setText("Rssi = " + String.valueOf(rssival));
//            }*/
//
//            tvname.setText(device.getName());
//            tvadd.setText(device.getAddress());
//            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
//                tvname.setTextColor(Color.GRAY);
//                tvadd.setTextColor(Color.GRAY);
//                tvpaired.setTextColor(Color.GRAY);
//                tvpaired.setVisibility(View.VISIBLE);
//                //tvrssi.setVisibility(View.GONE);
//            } else {
//                tvname.setTextColor(Color.WHITE);
//                tvadd.setTextColor(Color.WHITE);
//                tvpaired.setVisibility(View.GONE);
//                //tvrssi.setVisibility(View.VISIBLE);
//                //tvrssi.setTextColor(Color.WHITE);
//            }
//
//            if (utility.isPortOpen(utility.new blePort(device))){
//                Log.i(TAG, "connected device::"+device.getName());
//                tvname.setTextColor(Color.WHITE);
//                tvadd.setTextColor(Color.WHITE);
//                tvpaired.setVisibility(View.VISIBLE);
//                tvpaired.setText("connected");
//                // tvrssi.setVisibility(View.GONE);
//            }
            return null; // vg;
        }
    }
}
