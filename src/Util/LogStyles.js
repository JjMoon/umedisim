import _ from 'lodash';

export const exampleStyle = [
  'background: linear-gradient(#D33106, #571402)',
  'border: 1px solid #3E0E02',
  'color: white',
  'display: block',
  'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
  'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5),' +
  ' 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
  'line-height: 40px',
  'text-align: center',
  'font-weight: bold',
].join(';');

export const imprBig = [
  'background: linear-gradient(#DBA, #943)',
  'border: 1px solid #3E0E02',
  'color: white',
  'display: block',
  'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
  'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5),' +
  ' 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
  'line-height: 48px',
  'text-align: center',
  'font-weight: bold',
  'font-size: 36px',
].join(';');

export const impr = [
  'background: linear-gradient(#D85, #35A)',
  'border: 1px solid #3E0E02',
  'color: white',
  'display: block',
  'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
  'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5),' +
  ' 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
  'font-size: 16px',
  'line-height: 36px',
].join(';');

export const apis = ['color: #729', 'font-size: 14px', 'line-height: 20px'].join(';');

export const norm = ['color: #027', 'font-size: 12px', 'line-height: 14px'].join(';');

export const line = _.repeat('++=====++ ', 13);

export const lineSty = [
  'color: #F55',
  'display: block',
  'font-size: 9px',
  'line-height: 20px',
].join(';');


export const pale = 'background: #EEE; color: #789; line-height: 16px;'; // font-weight: bold;

export const anltc = 'background: #DEF; color: #987; line-height: 36px;'; // font-weight: bold;
