/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneOper / GraphBrth.js
 *
 * Created by Jongwoo Moon on 2017. 1. 22..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */


import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import ReactNativeART, { Surface, Group, Shape } from 'ReactNativeART';
import _ from 'lodash';
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
// import * as d3Array from 'd3-array';
// import * as graphUtil from '../model/HtGraphUtil';
import * as actions from '../actions';


const lang = require('../data/langOper.json');

const d3 = { scale, shape };
const maxVolumn = 1100; // 호흡 최대 부피..  mL
const strokeWidth = 5; // viewWidth = 500, viewHeight = 150,

/**
 * Unit Text
 */
class UnitText extends Component {
  render() {
    return (
      <View style={styunit.container}>
        <Text style={styunit.title}>{this.props.title}</Text>
        <Text style={styunit.txt}>{this.props.content}</Text>
      </View>
    );
  }
}

// //////////////////////////////////////////////////   _//////////////////_     custom style ###
const styunit = StyleSheet.create({
  container: {
    margin: 3,
    marginTop: 5, marginBottom: 5,
    backgroundColor: '#EEF',
    borderRadius: 5,
    alignItems: 'center',
  },
  title: {
    fontSize: 13,
    color: '#53A',
    fontWeight: '500',
    textAlign: 'center',
    margin: 5,
    alignSelf: 'stretch',
  },
  txt: {
    fontSize: 18,
    color: '#318',
    fontWeight: '200',
    textAlign: 'center',
    marginTop: 5, marginBottom: 5,
    alignSelf: 'stretch',
  },
});

/**
 * Ett Breath Graph
 */
class GraphBrth extends Component {
  state = { };

  // //////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    console.log(`GraphBrth :: Screen   ${this.props.main.scr.width} `);
  }

  graphScreenShot() {
    console.log('\n\n\n snapshot  ');
    this.snapshot('graph');
  }

  tickFunction() {
    // console.log(`Tick ...   ${this.props.breath.length}`);
    this.setChartString();
  }

  createScaleX(start, end, width) {
    return d3.scale.scaleTime()
      .domain([new Date(start * 1000), new Date(end * 1000)])
      .range([0, width]);
  }

  drawGraph() {
    const { time, size } = this.props; // time={{ s: new Date(), e: new Date() }}
    const { w, h } = size;

    console.log(time);
    console.log(`  www : ${w}  ${h}`);

    // 시작 시간..
    const scTime = d3.scale.scaleTime()
        .domain([time.s, time.e])
        .range([20, w - 50]), // .range([0, viewWidth - 100]),
      scY = d3.scale.scaleLinear()
        .domain([0, maxVolumn])
        .range([0, h]);
    // console.log(`Start : ${startTime} ~~  End : ${endTime}  ${arrForGraph.length}  `);

    return d3.shape.line()
      .x((v) => scTime(v.tsObj))
      .y((v) => scY(maxVolumn - v.blly));
  }

  setChartString() {
    const pitch = strokeWidth * 1.8;
    let strongStr = '', normalStr = '', weakStr = '';
    for (let i = 0; i < this.props.breath.length; i++) {
      const br = this.props.breath[i],
        xco = (pitch * i) + strokeWidth + 1, yco = 150 - ((120 * br.state.maxVol) / 2000);
      const stri = `M${xco} 150 ${xco} ${yco}`;
      // console.log(` good : ${br.state.judge}`);

      switch (br.state.judge) {
        case 'STRONG':
          strongStr += stri;
          break;
        case 'WEAK':
          weakStr += stri;
          break;
        default:
          normalStr += stri;
          break;
      }
    }
    this.setState({ strongStr, weakStr, normalStr });
  }

  // //////////////////////////////////////////////////   _//////////////////_   횟수, 주기
  renderLeftSigns() {
    const { langKey } = this.props.uiState;
    const cnt = (this.props.breath.length > 0) ? this.props.breath.length - 1 : 0;

    if (this.props.breath.length > 2) {
      // console.log(_.first(this.props.breath).state.periodInSec);
    }
    const arrHasPeriod = _.filter(this.props.breath, (bLgc) => bLgc.state !== undefined && bLgc.state.periodInSec > 0);
    // console.log(`  arrHasPeriod ::  ${arrHasPeriod.length}`);
    const arrPeriod = _.map(arrHasPeriod, (obj) => obj.state.periodInSec);

    let period = arrHasPeriod.length > 0 ?
      _.reduce(arrPeriod, (val, n) => val + n, 0) / arrHasPeriod.length : 0;
    period = `${_.round(period, 1)} s`;

    return (
      <View style={sty.leftContainer}>
        <UnitText title={lang[langKey].frequency} content={cnt} />
        <UnitText title={lang[langKey].period} content={period} />
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_
  graphSurface() {
    const { time, size, parent } = this.props; // time={{ s: new Date(), e: new Date() }}
    const { w, h } = size;
    // const timeSpanSec = totalTimeInSec * 1000, endTime = new Date(),
    //   startTime = new Date(endTime.getTime() - timeSpanSec);
    const arrForGraph = _.filter(this.props.arrPack, (o) => 
       time.s < o.tsObj.getTime()  // o.ts; // 시작 이후..
    );

    console.log(`  arrForGraph ::  ${arrForGraph.length}  ${parent}`);

    const { strongStr, weakStr, normalStr } = this.state;
    const yW = Math.round(w * 0.67), yS = Math.round(h * 0.33);
    const weakArea = `M0 ${h}, ${w} ${h}, ${w} ${yW}, 0 ${yW}  `;
    const normArea = `M0 ${yW}, ${w} ${yW}, ${w} ${yS}, 0 ${yS}  `;
    const strnArea = `M0 0, ${w} 0, ${w} ${yS}, 0 ${yS}  `;
    let graphShape = null;

    if (this.props.uxhistory.playMode || parent === 'MainReport') {
      graphShape = (<Shape
        d={this.drawGraph()(arrForGraph)}
        stroke="#599"
        strokeWidth={2}
      />);
    }

    return (
      <Surface
        ref="graph"
        height={h}
        style={sty.surfaceContainer}
      >
        <Group x={0} y={0}>
          <Shape d={weakArea} fill="#EFEFEF" />
          <Shape d={normArea} fill="#DFF5DF" />
          <Shape d={strnArea} fill="#F5ECEC" />

          <Shape
            d={strongStr} stroke="#E88"
            strokeWidth={strokeWidth}
          />
          <Shape
            d={normalStr} stroke="#8E8"
            strokeWidth={strokeWidth}
          />
          <Shape
            d={weakStr} stroke="#CC4"
            strokeWidth={strokeWidth}
          />
          {graphShape}
        </Group>
      </Surface>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    return (
      <View style={sty.mainContainer}>
        {this.renderLeftSigns()}
        {this.graphSurface()}
      </View>
    );
  }
}

// //////////////////////////////////////////////////   _//////////////////_     custom style ###
const sty = StyleSheet.create({
  mainContainer: {
    flexDirection: 'row', margin: 5,
    backgroundColor: '#FFE',
  },
  surfaceContainer: {
    flex: 70,
    // backgroundColor: '#EEE',
    borderRadius: 10,
    margin: 1,
  },
  leftContainer: {
    flex: 10,
    alignItems: 'stretch',
    justifyContent: 'center',
    // backgroundColor: '#DD8',
  },
  title: {
    fontSize: 15,
    color: '#53A',
    fontWeight: '500',
    textAlign: 'center',
    margin: 3,
    alignSelf: 'stretch',
  },
  txt: {
    fontSize: 20,
    color: '#A33',
    backgroundColor: '#5550',
    fontWeight: '300',
    textAlign: 'left',
    margin: 5,
    marginLeft: 25,
    alignSelf: 'stretch',
  },
});

const mapStateToProps = (state) => ({
    main: state.main,
    uiState: state.uiState,
    arrPack: state.arrPacket, // 이게 패킷 리스트 임..
    breath: state.breath,
    uxhistory: state.uxhistory,
  });

export default connect(mapStateToProps, actions)(GraphBrth);


/*


// Debugging 용 뷰...
renderLeftDebug() {
  const timeSpanSec = totalTimeInSec * 1000, endTime = new Date(),
    startTime = new Date(endTime.getTime() - timeSpanSec);

  const arrForGraph = _.filter(this.props.arrPack, (o) => {
    return startTime < o.tsObj.getTime();  // o.ts; // 시작 이후..
  });

  const timespaninmilisec = endTime.getTime() - startTime.getTime();
  const { replay } = this.props.uxhistory;
  if (replay.exam === null) { // replay.exam === undefined
    return;
  }
  if (replay.exam.packets.length === 0) {
    return;
  }

  const { lagTime, curPacketLag, exam, examStarted, replayStarted } = this.props.uxhistory.replay;
  const { isPacket, contentJson } = exam.packets[replay.idx];
  const packetNum = exam.packets.length;

  return (
    <View style={sty.leftContainer}>
      <UnitText title='ea' content={`${arrForGraph.length}  ${arrForGraph.length}`} />
      <UnitText title={`P : ${packetNum}`} content={`${lagTime}`} />
    </View>
  );
}
*/
