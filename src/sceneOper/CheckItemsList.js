/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneOper / CheckItemsList.js
 *
 * Created by Jongwoo Moon on 2017. 2. 14..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */


import React, { Component } from 'react';
import { ListView, View, Text } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import CheckItem from './CheckItem';


/**
 * 실행 화면에서 실습 중에 체크하는 리스트 뷰..
 */
class CheckItemsList extends Component {
  constructor(props) {
    super(props);

    console.log(this.activeItems());

    // 여기서 각 항목을 추가함..
    const defaultItemsOri = require('../data/CheckItemsDefault.json');
    // // { "shr": "기록 명령", "long": "수행한 술기를 의말하는가?", "score": 10, "auto": false }
    const defaultItems = _.filter(defaultItemsOri, o => !o.auto);

    this.props.clearCheckList();

    for (const itm of defaultItems) {
      this.props.addCheckList(itm);
    }
    this.setInitialDateSource();
  }

  setInitialDateSource() {
    // console.log(this.props.EttSetting.manCheckItem);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    this.time = new Date();
    this.state = { // 기본 값과 함께 사용자가 추가한 항목도 가져와야 함.
      // dataSource: ds.cloneWithRows(this.props.chkItems),
      dataSource: ds.cloneWithRows(this.activeItems()),
    };
  }

  activeItems() {
    return _.filter(this.props.EttSetting.manCheckItem, o => o.active);
  }

  getDataSource(datum) {
    return this.state.dataSource.cloneWithRows(datum);
  }

  renderRow(dt) {
    return (
      <CheckItem
        item={dt}
        // {/*  redraw={this.redraw.bind(this)}    자동 닫침 안됨..  */}
      />
    );
  }

  renderRowOld(dt) {
    return (
      <View style={{ flex: 1 }}>
        <Text style={{ textAlign: 'center' }}>{dt.shr}</Text>
      </View>
    );
  }

  render() {
    this.time = new Date();
    return (
      <ListView
        style={{ flex: 1, alignSelf: 'stretch' }}
        enableEmptySections
        dataSource={this.getDataSource(this.activeItems())}
        renderRow={this.renderRow.bind(this)}
      />
    );
  }
}

const mapStateToProps = state => ({
  main: state.main,
  EttSetting: state.EttSetting,
  chkItems: state.chkItems,
});

export default connect(mapStateToProps, actions)(CheckItemsList);
