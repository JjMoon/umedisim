/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / sceneOper / CheckItem.js
 *
 * Created by Jongwoo Moon on 2017. 2. 15. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.

    상태 : 확인 선택, Expanded 상태

 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableHighlight, Slider } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';
//const lang = require('../data/langOper.json');

/// 실행 화면에서 실습 중에 체크 아이템...
class CheckItem extends Component {

  state = {
    expanded: false,
    disable: false,
    partScore: 0
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.setState({ partScore: this.props.item.score });
  }

  componentWillUnmount() {
    //console.log(`   CheckItem :: componentWillUnmount   ${this.props.item.shr}`);
    //this.setState({ disable: false });
  }

  componentWillReceiveProps(props) {
    //xx this.setState({
    //   expanded: props.expanded
    // });
  }
  // { "shr": "기록 명령", "long": "수행한 술기를 의말하는가?", "score": 10, "auto": false }

  ////////////////////////////////////////////////////   _//////////////////_   확장 버튼 바인드
  expand() {
    if (this.state.disable) {
      return;
    }
    const { shr, score } = this.props.item;

    //xx this.props.expandChkList(shr);



    const partScoreOnDisplay = expanded ? score : Math.round(score * 0.5);

    const expanded = !this.state.expanded;

    this.setState(
      {
        expanded,
        //xx expanded: this.props.item.expanded,
        partScore: partScoreOnDisplay

      }
    );

    console.log(`\n\n expand  ::  ${this.state.expanded}  \n\n`);
    console.log(this.state);
  }

  confirmCheck() {
    if (this.state.disable) { return; } // Disable 처리
    console.log(` confirm check ::  ${this.state.expanded}  ${this.state.partScore}`);
    if (this.state.expanded) {
      this.expand();
    } // 이미 확장된 상태라면 닫기..
    this.props.item.disable = true;
    this.setState({ disable: true });
    // //  매뉴얼 체크 히스토리에 추가....
    this.props.addHistory({ ...this.props.item,
      pass: true, scoreMarked: this.state.partScore, disable: true, ts: new Date()
    });
  }

  getDataSource(datum) {
     return this.state.dataSource.cloneWithRows(datum);
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    //xx if (this.props.uxhistory.playMode && this.props.item.expanded) {
    if (this.props.uxhistory.playMode && this.state.expanded) {
      return this.expandedRender();
    }
    return this.twoButtons();
  }

  scoreInputBox() {
    const { score } = this.props.item;
    return (
      <View style={[sty.contnr, { marginLeft: 10, marginRight: 10 }]}>
        <Slider
          style={{ flex: 5 }}
          value={score * 0.5}
          minimumValue={0}
          maximumValue={score}
          onValueChange={
            (v) => {
              this.setState({ partScore: Math.round(v) });
            }}
        />
        <Text style={sty.partScore}>{this.state.partScore}</Text>
      </View>
    );
  }

  expandedRender() {
    const { long } = this.props.item;
    return (
      <View style={sty.mainContnr}>
        {this.twoButtons()}

        <View>
          <Text style={sty.longTxt}>{long}</Text>
          {this.scoreInputBox()}
        </View>
      </View>
    );
  }

  twoButtons() { // 메인 2 버튼 렌더링.
    const { shr } = this.props.item;
    const { uicommon, langKey } = this.props.uiState;
    const { expanded, disable } = this.state;
    const bgColor = disable ? HtColor.bgDarkGray : HtColor.bttnDarkBG;
    const { apply } = uicommon[langKey];
    const checkBttnText = expanded ? `${this.state.partScore} ${apply}` : shr; // 부분 점수 적용.
    const expandBttnText = expanded ? '><' : ':::'; // 확장 버튼 표시 텍스트..

    if (this.props.uxhistory.playMode) { // 활성 상태
      return (
        <View style={[sty.contnr]}>
          <TouchableHighlight
            style={[sty.buttonBase, { flex: 25, backgroundColor: bgColor }]}
            underlayColor={'#CCDDEE'}
            onPress={this.confirmCheck.bind(this)}
          >
            <Text style={sty.title}>{checkBttnText}</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={[sty.buttonBase, { flex: 5, backgroundColor: bgColor }]}
            underlayColor={'#CCDDEE'}
            onPress={this.expand.bind(this)}
          >
            <Text style={sty.expand}>{expandBttnText}</Text>
          </TouchableHighlight>
        </View>
      );
    }
    return (
      <View style={[sty.contnr]}>
        <TouchableHighlight
          style={[sty.buttonBase, { flex: 25, backgroundColor: '#55A' }]}
          underlayColor={'#DEF'}
        >
          <Text style={sty.title}>{checkBttnText}</Text>
        </TouchableHighlight>
      </View>
    );
  }

}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  mainContnr: {
    backgroundColor: HtColor.bgBrightGray,
    borderRadius: 8,
    marginTop: 5, marginBottom: 5,
  },
  contnr: {
    flex: 1, flexDirection: 'row', alignSelf: 'stretch',
    height: 40
  },
  buttonBase: {
    justifyContent: 'center',
    padding: 3, margin: 5, borderRadius: 4,
  },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  expand: {
    textAlign: 'center',
    color: HtColor.textWhite,
  },
  title: {
    textAlign: 'center',
    color: HtColor.textWhite,
  },
  longTxt: {
    textAlign: 'center',
    color: HtColor.txtDarkGray,
    fontWeight: '400',
    fontSize: 14,
    margin: 5, marginLeft: 10, marginRight: 10,
  },
  partScore: {
    flex: 1,
    fontSize: 20,
    fontWeight: '500',
    color: HtColor.txtDarkGray,
    textAlign: 'center',
    marginTop: 5
  }
});

const mapStateToProps = state => {
  return {
    main: state.main,
    uiState: state.uiState,
    uxhistory: state.uxhistory
  };
};

export default connect(mapStateToProps, actions)(CheckItem);
