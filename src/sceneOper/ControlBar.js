/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneOper / ControlBar.js
 *
 * Created by Jongwoo Moon on 2017. 2. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen, bgBlue } = HtColor;

class ControlBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isplaying: false,
      modalVisible: false
    };
    console.log('\n ====== ====== ====== ======  [[ ControlBar :: constructor ]] ');
  }

  playAndPause() {
    console.log(`playAndPause() send >>>>>>>  ${this.state.isplaying}  ${this.state.modalVisible}`);

    if (this.state.isplaying) { // mainView 로 보내는 콜백..
      this.props.stopCallback();
    } else {
      this.props.playCallback();
    }

    this.setState({
      isplaying: !this.state.isplaying,
      // modalVisible: this.state.isplaying,
    });
    console.log(`playAndPause() send >>>>>>>  ${this.state.isplaying}  ${this.state.modalVisible}`);

    const p = this.state.isplaying;
    //if (!p) {
    this.props.startStopExamin(!p);
    //}
  }

  getPlayButton() {
    const { height } = this.props.main.scr, size = height * 0.11;
    if (this.state.isplaying) {
      return (
        <Image
          style={{ width: size, height: size }}
          source={require('../resource/imageCommon/pausebutton.png')}
        />
      );
    }
    return (
      <Image
        style={{ width: size, height: size }}
        source={require('../resource/imageCommon/playbutton.png')}
      />
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex } = this.props;
    return (
      <View style={{ flex, backgroundColor: bgBlue, margin: 2 }}>
        <View style={[styl.rowContainer, { flex: 100 }]}>
          <TouchableOpacity onPress={this.playAndPause.bind(this)}>
            {this.getPlayButton()}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    color: bttnBrightTextGreen,
    fontSize: 30,
    fontWeight: '600',
    textAlign: 'left',
    margin: 20,
    marginTop: 35,
    alignSelf: 'stretch'
  },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    theCounter: state.timerCount,
  };
};

export default connect(mapStateToProps, actions)(ControlBar);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
