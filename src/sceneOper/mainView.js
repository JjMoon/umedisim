/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneOper / mainView.js
 *

 패킷 샘플 ::  [2, 192, 20, 5, 5, 5, 5, 3]

 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */


import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import * as actions from '../actions';
import TitleBarOperate from './TitleBarOperate';
import ControlBar from './ControlBar';
// import Popup from 'react-native-popup';
import ImageSection from './ImageSection';
import * as styl from '../uicompo/HtStyles';
import HistoryList from '../simulations/HistoryList';
import Progress from '../simulations/Progress';
import CheckItemsList from './CheckItemsList';
import * as M from '../model';

const packGenerator = new M.HtPacketGenerator({ options: 'nono' });
const ettLogic = new M.EttAutoCheck();

const lang = require('../data/langOper.json');

/** */
class SceneOperate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cnt: 0, dueTimeInSec: 0,
      error: null,
      res: null,
      value: {
        format: 'png',
        quality: 0.9,
        result: 'file',
        snapshotContentContainer: false,
      },
    };
  }

  addMetaAppObject() {
    M.RealmGObj.write(() => {
      M.RealmGObj.create('MetaApp', {
        keyName: 'SomeTestAgain',
        valInt: 33.5,
        valString: 'SomeTest valString',
      });
    });
  }

  // //////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    this.props.changeMainState('oper');
    this.props.examListViewMode();

    this.props.setViewLayout('imageLeft'); // default

    ettLogic.init(this.props.EttSetting, this.props.uiState.langKey);
    this.props.propagateSetting(this.props.EttSetting, ettLogic); // 세팅을 필요한 리듀서에  < 전파.. >
  }

  componentWillUpdate() {
    // LayoutAnimation.easeInEaseOut(); //spring(); // easeInEaseOut();
  }

  componentWillUnmount() {
    console.log('\n\n\n sceneOper :: mainView  ::   componentWillUnmount ');
    clearInterval(this.interval);
    clearInterval(this.stopWatch);
  }

  // //////////////////////////////////////////////////   _//////////////////_   이미지 표현 부분.
  renderImageSection() {
    console.log('renderImageSection');
    // ref={(rf) => { this.graphObj = rf; }}
    return (
      <View style={{ flex: 100 }}>
        <ImageSection
          flex={100}
        />
        <ControlBar
          flex={13}
          title={lang.Title}
          playCallback={this.startPlay.bind(this)}
          stopCallback={this.stopPlay.bind(this)}
        />
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   컨트롤 바 콜백 부분...
  startPlay() {
    console.log('  Started ....');
    if (this.props.bleRdcr.conState === 'Connected') {
      return;
    }
    if (!this.props.main.connETT) {
      this.interval = setInterval(this.tickFunction.bind(this), 320);
    }
    this.stopWatch = setInterval(() => {
      console.log(`\n\n\n Time : ${this.state.dueTimeInSec}  \n\n\n`);
      this.setState({
        dueTimeInSec: this.state.dueTimeInSec + 1,
      });
    }, 1000);
  }

  stopPlay() {
    const arrBrth = this.props.breath;
    console.log(`  Stopped ....${arrBrth.length}`);
    clearInterval(this.interval);
    clearInterval(this.stopWatch);
    this.props.makeBrthHistory(arrBrth);
    Actions.sceneReportView();
  }

  tickFunction() {
    console.log('\n ------------------------------------------------------- >> Tick');
    this.props.addCounter(1);
    this.setState({
      cnt: this.state.cnt + 1,
    });
    const pack = packGenerator.genNewPacket(); // console.log(pack);
    this.monitorState(pack); // 패킷 처리..
  }

  // //////////////////////////////////////////////////   _//////////////////_   Packet 처리
  monitorState(pck) {
    // let { arrPacket } = this.props;
    this.props.newBlePacket(pck);

    // 로직 객체를 통해 메인 상태 변경을 감지한다.
    // const arrPack = this.props.arrPacket; // 다시 받아서..

    //
    // const pckObj = arrPack[arrPack.length - 1]; // 타임 스탬프 붙인 객체.
    // this.props.changeNeckAngle(pckObj.nckAng);
    // this.props.changeChinPush(pckObj.glcnbl[1]);
    // this.props.changeNeckHeight(pckObj.nckDst);
    // this.props.checkTeeth(pckObj.tth);
    // this.props.checkBronchus(pckObj.glcnbl[0]);
    // this.props.checkBelly(pckObj.glcnbl);
  }


  // //////////////////////////////////////////////////   _//////////////////_   List View .. 2열..
  renderListViews(isVoid) {
    if (isVoid) {
      return (
        <View style={{ flexDirection: 'row', flex: 1, alignSelf: 'stretch' }} />
      );
    }
    return (
      <View style={{ flexDirection: 'row', flex: 90, alignSelf: 'stretch' }}>
        <View style={{ flex: 4, alignSelf: 'stretch', alignItems: 'stretch' }} >
          <HistoryList />
        </View>
        <View style={{ flex: 5, alignSelf: 'stretch', alignItems: 'stretch' }} >
          <CheckItemsList />
        </View>
      </View>
    );
  }

  renderOperationView() {
    if (this.props.uiState.viewState === 'imageRight') {
      return (
        <View style={[styl.rowContainer, { flex: 75 }]}>
          {this.renderListViews()}
          {this.renderImageSection()}
          {this.renderListViews(true)}
        </View>
      );
    }
    return (
      <View style={[styl.rowContainer, { flex: 75 }]}>
        {this.renderListViews(true)}
        {this.renderImageSection()}
        {this.renderListViews()}
      </View>
    );
  }

  // showPopup() {
  //   this.popup.confirm({
  //     title: 'title',
  //     content: 'Are you ready?',
  //   });
  //   // this.popup.tip({ content: 'come on!', });
  // }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { dueTimeInSec } = this.state;
    return (
      <View style={{ marginTop: 1, flex: 100 }}>
        <TitleBarOperate
          swapBttnOn
          back={'Back'}
          flex={10}
          dueTimeInSec={dueTimeInSec}
        />
        <View style={{ flex: 7 }}>
          <Progress />
        </View>
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}
        {this.renderOperationView() /* { flex: 80 } */}
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />

        {/** Popup component
        <Popup
          style={{ width: 200 }}
          ref={(popup) => { this.popup = popup; }}
          isOverlay
          isOverlayClickClose
        />  */}
        {/** or <Popup ref={popup => this.popup = popup } isOverlay={false} isOverlayClickClose={false}/> */}

      </View>
    );
  }
}

const sty = StyleSheet.create({
  mainContainer: {
    alignItems: 'stretch', justifyContent: 'center', flex: 1, margin: 0, marginTop: 20,
  },
  container: {
    alignItems: 'stretch',
    justifyContent: 'center',

  },
  imageView: {
    backgroundColor: '#389',
    alignSelf: 'stretch',
  },
  historySection: {
    backgroundColor: '#ABC',
    alignSelf: 'stretch',
  },
  seperateLine: {
    height: 2, backgroundColor: '#AAAAAA',
  },

});

const mapStateToProps = state => ({
  main: state.main,
  breath: state.breath,
  bleRdcr: state.bleRdcr,
  arrPacket: state.arrPacket,
  theCounter: state.timerCount,
  uiState: state.uiState,
  EttSetting: state.EttSetting,
});

export default connect(mapStateToProps, actions)(SceneOperate);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
