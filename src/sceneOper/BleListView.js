/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / SceneConnect / BleListView.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { ListView } from 'react-native';
import { connect } from 'react-redux';
import HtBleView from './HtBleView';

class BleListView extends Component {
  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(this.props.listItems);
    //this.dataSource = this.props.listItems;
  }

  getDataSource() {
     return this.dataSource.cloneWithRows(this.props.listItems);
  }

  renderRow(dt) {
    console.log(dt);
    return (
      <HtBleView txtTitle={dt} />
    );
  }

  render() {
    const { flex } = this.props;
    return (
      <ListView
        style={{ flex, alignSelf: 'stretch', }}
        dataSource={this.dataSource}
        renderRow={this.renderRow}
      />
    );
  }
}

const mapStateToProps = state => {
  return { listItems: state.arrPacket };
};

export default connect(mapStateToProps)(BleListView);
