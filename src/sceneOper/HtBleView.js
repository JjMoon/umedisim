/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / SceneConnect / HtBleView.js
 *
 * Created by Jongwoo Moon on 2017. 1. 17..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

export default class HtBleView extends Component {

  render() {
    return (
      <View style={sty.viewStyle}>
        <Text style={[styl.ttl]}>
          {this.props.ts}
        </Text>
        <Text style={[styl.content]}>
          Some Text ...
        </Text>
      </View>
    );
  }
}

const sty = {
  viewStyle: {
    borderRadius: 5,
    margin: 2,
    backgroundColor: HtColor.bgCyan
  },
};
