/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneOper / ImageSection.js
 *
 * Created by Jongwoo Moon on 2017. 2. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, Animated } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtColor } from '../uicompo';
import GraphBrth from './GraphBrth';

const smlImgR = 1.2;

const styl = require('../uicompo/HtStyles'),
  bronUri = {
    ok: require('../resource/uiSigns/lungOK.bmp'),
    err: require('../resource/uiSigns/lungNG.bmp'),
  },
  bellyMaskUri = {
    ok: require('../resource/uiSigns/bellyMaskOK.bmp'),
    err: require('../resource/uiSigns/bellyMaskNG.bmp'),
  },
  bellyBronUri = {
    ok: require('../resource/uiSigns/bellyBronOK.bmp'),
    err: require('../resource/uiSigns/bellyBronNG.bmp'),
  };

// F, A. 정상 / 과다. 종류  3 >> 12가지..
const markAngle = require('../resource/ett/mark_angle.png');
const markFlat = require('../resource/ett/mark_flat.png');
const markTooth = require('../resource/ett/mark_tooth.png');
const F_LMA_NORM = require('../resource/ett/flat_lma.png');
const A_LMA_NORM = require('../resource/ett/angle_lma.png');
const F_ETC_NORM = require('../resource/ett/flat_combi_lung.png');
const A_ETC_NORM = require('../resource/ett/angle_combi_lung.png');

const chinPushImg = require('../resource/ett/chin_push.png');

const img = {
  F: { base: require('../resource/ett/flat_base.png') },
  A: { base: require('../resource/ett/angle_base.png') },
  P: { base: require('../resource/ett/pillow_base.png') },
  F_ETT_NORM: require('../resource/ett/flat_tube.png'),
  F_ETT_OVER: require('../resource/ett/flat_over.png'),
  F_ETT_GULL: require('../resource/ett/flat_gullet.png'),
  A_ETT_NORM: require('../resource/ett/angle_tube.png'),
  A_ETT_OVER: require('../resource/ett/angle_over.png'),
  A_ETT_GULL: require('../resource/ett/angle_gullet.png'),

  F_LMA_NORM, F_LMA_OVER: F_LMA_NORM, F_LMA_GULL: F_LMA_NORM, // LMA 는 과도 삽관 동일 이미지..
  A_LMA_NORM, A_LMA_OVER: A_LMA_NORM, A_LMA_GULL: A_LMA_NORM,

  F_ETC_NORM, F_ETC_OVER: F_ETC_NORM, // ETC도 과도 삽관 없슴.
  F_ETC_GULL: require('../resource/ett/flat_combi_gull.png'),

  A_ETC_NORM, A_ETC_OVER: A_ETC_NORM,
  A_ETC_GULL: require('../resource/ett/angle_combi_gull.png'),
  Animation: require('../resource/lung/lung_ani.png'),
};
/**
 *   ETT Image Section
 */
class ImageSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isplaying: false, breathImg: null, aniScale: new Animated.Value(90),
    };
    this.expand = true;
    console.log('\n ====== ====== ====== ======  [[ ControlBar :: constructor ]] ');
  }


  componentDidMount() {
    this.timer = setInterval(() => this.animationHandling(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  animationHandling() {
    const targetSize = this.expand ? 130 : 90;
    Animated.timing(this.state.aniScale,
      { toValue: targetSize, duration: 1000 },
    ).start();
    this.expand = !this.expand;
  }

  graphScreenShot() {
    console.log('graphScreenShot in ImageSection');
    // this.graphObj.graphScreenShot();
  }

  renderImagePart() {
    const { ux } = this.props.uxhistory;
    const { ettMode } = this.props.main;
    const { neck, insert, chinPush, gullet, teeth } = this.props.uiState.img;

    const blly = this.props.arrPacket.length > 0 ? _.last(this.props.arrPacket).blly : null;
    const lungAniOpa = blly > 100 ? 1.0 : 0;

    const imgW = 540, imgH = 384, big = 0.75, bigImgWid = big * imgW, bigImgHgt = big * imgH;
    const imgSty = { position: 'absolute', width: bigImgWid, height: bigImgHgt, resizeMode: 'contain' };

    // 바탕 이미지
    const baseImg = img[neck].base;
    // 튜브 이름..
    const tubeNeckFAonly = (neck === 'P') ? 'A' : neck;
    let tubeInsert = (insert === 'UNDR') ? 'NORM' : insert;
    tubeInsert = gullet ? 'GULL' : tubeInsert; // 식도 케이스..
    const tubeName = `${tubeNeckFAonly}_${ettMode}_${tubeInsert}`, tubeImg = img[tubeName];
    const markImg = (neck === 'F') ? markFlat : markAngle;
    const toothOpacity = (teeth > 60) ? 1 : teeth / 100;
    const chinPushCompo = chinPush ?
      (<Image style={[imgSty, { bottom: 90, left: 167, width: 37 * 0.7, height: 73 * 0.7 }]} source={chinPushImg} />) :
      null;

    return (
      <View style={{ flex: 8, width: bigImgWid, height: bigImgHgt, marginRight: 5 }}>
        <Image
          style={{ position: 'absolute', width: bigImgWid, height: bigImgHgt,
            marginRight: 5, resizeMode: 'contain' }}
          source={baseImg}
        />
        <Image style={imgSty} source={tubeImg} />
        <Image style={imgSty} source={markImg} />

        {chinPushCompo}

        <Image style={[imgSty, { opacity: toothOpacity }]} source={markTooth} />

        <Animated.Image
          style={{ position: 'absolute', bottom: 0, left: 240, opacity: lungAniOpa, alignSelf: 'center',
            width: this.state.aniScale, height: this.state.aniScale }}
          source={img.Animation}
        />

      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   Main 이미지.
  renderBigImage() {
    const { arrPacket, uiState } = this.props;
    const curPacket = _.last(arrPacket);
    const { teeth } = uiState.img;

    const toothOpacity = (teeth > 60) ? 1 : teeth / 100;
    let tthVal = 0, neckAngle = 0;

    if (!this.props.uxhistory.playMode) {
      return this.renderDefaultView();
    }

    if (typeof curPacket !== 'undefined') { // && ux.curPacket.tth > 10
      tthVal = curPacket.tth;
      neckAngle = curPacket.nckAng;
    }

    return (
      <View style={{ flex: 7, alignItems: 'center', justifyContent: 'center' }}>
        <View
          style={{ flex: 3, flexDirection: 'row', alignItems: 'center', opacity: toothOpacity }}
        >
          <Text style={sty.toothText}>{tthVal} N</Text>
        </View>

        {this.renderImagePart()}

        <View style={{ flex: 2, marginTop: 5 }}>
          <Text style={sty.nectAngleText}>{neckAngle}°</Text>
        </View>
      </View>
    );
  }

  renderDefaultView() {
    const imgW = 540, imgH = 384, big = 0.75;
    const bigImgWid = big * imgW, bigImgHgt = big * imgH;
    const toothOpacity = 0.05, tthVal = 0, tthImgR = 0.5;
    const tthWidth = 77 * tthImgR, tthHeight = 111 * tthImgR;
    return (
      <View style={{ flex: 7, alignItems: 'center', justifyContent: 'center' }}>
        <View
          style={{ flex: 3, flexDirection: 'row', alignItems: 'center', opacity: toothOpacity }}
        >
          <Image
            style={{ width: tthWidth, height: tthHeight, alignSelf: 'stretch', resizeMode: 'contain' }}
            source={require('../resource/uiSigns/tooth-1.bmp')}
          />
          <Text style={sty.toothText}>{tthVal}</Text>
        </View>
        <Image
          style={{ flex: 8, width: bigImgWid, height: bigImgHgt, resizeMode: 'contain' }}
          source={img.F.base}
        />
        <View style={{ flex: 2 }} />
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   사이드의 3개 이미지 교체 관련...
  renderSideImage() {
    const { bronchus, bellyMask, bellyBronchus } = this.props.uiState.img;
    const bronSource = bronchus ? bronUri.err : bronUri.ok,
      bellyMaskSource = bellyMask ? bellyMaskUri.err : bellyMaskUri.ok,
      bellyBronSource = bellyBronchus ? bellyBronUri.err : bellyBronUri.ok;
    return (
      <View style={[styl.container, { flex: 3 }]} >
        <Image
          style={sty.smallImage}
          source={bronSource}
        />
        <Image
          style={sty.smallImage}
          source={bellyMaskSource}
        />
        <Image
          style={sty.smallImage}
          source={bellyBronSource}
        />
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() { // 540 X 384
    const { flex } = this.props;
    const totalTimeInSec = 20, e = new Date(), s = new Date(e.getTime() - (totalTimeInSec * 1000));
    // const { flex } = this.props, imgW = 540, imgH = 384, sml = 0.25, big = 0.7;
    return (
      <View style={{ flex, backgroundColor: '#FAFFFA' }}>
        <View style={[styl.rowContainer, sty.grayBoarder, { flex: 70 }]}>
          {this.renderSideImage()}
          {this.renderBigImage()}
        </View>
        <View style={[sty.grayBoarder, { flex: 30 }]}>
          <GraphBrth
            parent={'ImageSection'}
            time={{ s, e }}
            size={{ w: 500, h: 150 }}
            ref={(rf) => { this.graphObj = rf; }}
          />
        </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  // / -------------------------  -------------------------  // <Text>
  grayBoarder: {
    borderWidth: 1, margin: 1,
    borderColor: 'gainsboro', borderRadius: 5,
  },
  toothText: {
    fontSize: 30, color: '#A33',
    backgroundColor: '#5550', fontWeight: '300',
    textAlign: 'left', margin: 5, marginLeft: 25,
    alignSelf: 'stretch',
  },
  nectAngleText: {
    flex: 100, margin: 5, marginLeft: 40,
    alignSelf: 'stretch', width: 100, height: 40,
    color: '#155', backgroundColor: '#0000',
    fontSize: 35, fontWeight: '300', textAlign: 'center',
  },
  smallImage: {
    width: smlImgR * 111, height: smlImgR * 92, margin: 5,
    resizeMode: 'contain',
  },
});

const mapStateToProps = state => ({
  main: state.main,
  arrPacket: state.arrPacket,
  uiState: state.uiState,
  uxhistory: state.uxhistory,
  theCounter: state.timerCount,
  breath: state.breath,
});

export default connect(mapStateToProps, actions, null, { withRef: true })(ImageSection);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
