/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneMain / TitleBarOperate.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen } = HtColor;

/** Title Bar in ETT Operate */
class TitleBarOperate extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ TitleBarOperate :: constructor ]] ');
  }

  componentWillUnmount() {
    clearInterval(this.stopWatch);
  }

  swap() {
    if (this.props.uiState.viewState === 'imageRight') {
      this.props.setViewLayout('imageLeft'); // default
    } else {
      this.props.setViewLayout('imageRight'); // 이미지를 오른쪽에 둠.
    }
  }

  // buttonRender() {
  //   if (this.props.examCase) {
  //     return;
  //   }
  //   return (
  //     <View>
  //       <TouchableOpacity style={{ flex: 5 }} onPress={this.swap.bind(this)}>
  //         <Text style={sty.miniTitle}>View Swap</Text>
  //       </TouchableOpacity>
  //       <TouchableOpacity style={{ flex: 5 }} onPress={Actions.sceneSetting}>
  //         <Text style={sty.miniTitle}>Setting!</Text>
  //       </TouchableOpacity>
  //     </View>
  //   );
  // }

  renderTitleBackButton() {
    const { title } = this.props;
    return ( // Actions.sceneOperate;
      <TouchableOpacity style={{ flex: 5 }} onPress={Actions.pop}>
        <Text style={[sty.title, { flex: 30 }]}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  }

  renderSwapButton() {
    const smallButtonFlex = 5;
    if (!this.props.swapBttnOn) {
      return null;
    }
    return (
      <TouchableOpacity
        style={{ alignSelf: 'flex-end', flex: smallButtonFlex }}
        onPress={this.swap.bind(this)}
      >
        <Image
          style={{ width: 60, height: 30, marginLeft: 20, marginTop: 20, resizeMode: 'stretch' }}
          source={require('../resource/imageCommon/bidirectional_arrow.png')}
        />
      </TouchableOpacity>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { title, back, flex, dueTimeInSec } = this.props;
    const smallButtonFlex = 5;
    const min = dueTimeInSec > 59 ? `${parseInt(dueTimeInSec / 60)} : ` : null;
    const sec = `${dueTimeInSec % 60}`;
    return (
      <View style={{ flex, backgroundColor: 'dodgerblue' }}>
        <View style={[styl.rowContainer, { flex: 1 }]}>

          <TouchableOpacity style={{ flex: smallButtonFlex }} onPress={Actions.pop}>
            <Text style={[sty.miniTitle, { textAlign: 'left', flex: 1, marginLeft: 20 }]}>
              {back}
            </Text>
          </TouchableOpacity>

          <Text style={[sty.title, { flex: 40 }]}>
            {title}
          </Text>

          <Text style={sty.timer}>{min}{sec}</Text>

          {this.renderSwapButton()}

        </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  // / -------------------------  -------------------------  // <Text>
  title: {
    flex: 30,
    color: bttnBrightTextGreen,
    fontSize: 30,
    fontWeight: '600',
    textAlign: 'center',
    margin: 10,
    marginTop: 45,
    alignSelf: 'stretch',
  },
  timer: {
    flex: 5, fontSize: 25, fontWeight: '400',
    color: bttnBrightTextGreen,
    textAlign: 'center',
    margin: 5, marginTop: 50,
    alignSelf: 'stretch',
  },
  miniTitle: {
    color: bttnBrightTextGreen,
    fontSize: 18,
    fontWeight: '800',
    margin: 5, marginTop: 55,
    alignSelf: 'stretch',
  },
});


const mapStateToProps = state => ({
  // main: state.main,
  // theCounter: state.timerCount,
  uiState: state.uiState,
});

export default connect(mapStateToProps, actions)(TitleBarOperate);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
