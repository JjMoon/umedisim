import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, LayoutAnimation, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Meteor from 'react-native-meteor';

import * as actions from '../actions';
import { HtRectBttn, HtColor } from '../uicompo';
import * as M from '../model';
// import BleManView from '../simulations/BleManView';
import BleManView from '../simulations/BleManView';
import { norm, line, lineSty, impr, imprBig } from '../Util';


const mdl = new M.SettingModule();
const ettLogic = new M.EttAutoCheck();
const SERVER_URL = 'ws://localhost:3000/websocket';
// const SERVER_URL = 'https://medicionsvr.herokuapp.com';

/**
* Bit Src Project / UMediSim
* git@bitbucket.org:JjMoon/umedisim.git
*
* src / sceneMain / mainView.js
*
* Created by Jongwoo Moon on 2017. 1. 10..
*/
class SceneMain extends Component {
  constructor(props) {
    super(props);
    console.log('%c SceneMain :: constructor 앱 시작', imprBig);
    this.state = {
      cnt: 1, isOpen: false,
      scanning: false, connecting: false, connected: false, connectColor: '#CCC',
      viewWillDisappear: null, // langIdx: 0, langObj
    };
    this.props.appStarted();
    this.props.changeMainState('init');
    this.settingDataCheck();
  }

  debugMethod() {
    console.log(this.props.EttSetting);
  }

  settingDataCheck() { // 데이터가 없으면 기본 값을 쓰는 부분...
    mdl.checkInit('bpRealTestSetting', null);
    mdl.checkInit('bpTrainSetting', null);
    mdl.checkInit('ettGeneralSet', (obj) => {
      this.props.setEttCheckItemValues({ general: obj });
    });
    mdl.checkInit('ettAutoCheckValues', (obj) => {
      this.props.setEttCheckItemValues({ autoCheckItem: obj });
    });
    mdl.checkInit('ettManualItems', (obj) => {
      this.props.setEttCheckItemValues({ manCheckItem: obj });
    });
    mdl.checkInit('system', (obj) => {
      this.props.languageChange(obj.languageKey);
      this.props.setBleDevNum({ ettDevNum: obj.ettDevNum });
      this.props.setBleDevNum({ bpDevNum: obj.bpDevNum });
      this.props.editStdName(obj.stdName);
      this.props.editStdEmail(obj.stdEmail);
    });

    // 세팅 정보 읽어서 리듀서에 등록.
    mdl.retrieveBpSetting('bpTrainSetting', (obj) => {
      this.props.setBpSetting({ train: obj });
    });
    mdl.retrieveBpSetting('bpRealTestSetting', (obj) => {
      this.props.setBpSetting({ real: obj });

      ettLogic.init(this.props.EttSetting, this.props.uiState.langKey);
      this.props.propagateSetting(this.props.EttSetting, ettLogic); // 세팅을 필요한 리듀서에  < 전파.. >

      // this.testCase();  // 이거 테스트 케이스임... 비동기로 해야 하므로 여기에 ...
    });
  }

  // sendRnEmail(addr, path) {
  //   console.log(` Send Mail to ${addr}  with ${path}`);
  //   this.renderDebug('sendRnEmail');
  //
  //   Mailer.mail({
  //     subject: 'ETT 결과지',
  //     recipients: [addr],
  //     body: '<h1>Report</h1>',
  //     isHTML: true, // iOS only, exclude if false
  //     attachment: {
  //       path, // The absolute path of the file from which to read data.
  //       type: 'pdf', // Mime Type: jpg, png, doc, ppt, html, pdf
  //       name: 'report.pdf', // Optional: Custom filename for attachment
  //     },
  //   }, (error, event) => {
  //     if (error) {
  //       console.log(error);
  //       this.renderDebug('error');
  //     }
  //   });
  // }


  // //////////////////////////////////////////////////   _/////////////////_   component life cycle
  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut(); // spring();
  }

  componentWillMount() {
    console.log('%c sceneMain :: Main :: componentWillMount  ', norm);
    Meteor.connect(SERVER_URL);
    // this.props.changeMainState('init');

    // DeviceEventEmitter.addListener('received', this.receivedPacket.bind(this));
    // DeviceEventEmitter.addListener('receivedBP', this.receivedBpPacket.bind(this));
  }

  componentWillUnmount() {
    console.log('%c sceneMain :: Main :: componentWillUnmount  ', norm);
    if (this.state.viewWillDisappear !== null) {
      this.state.viewWillDisappear();
    }
  }

  componentDidMount() {
    console.log('%c sceneMain :: Main :: componentDidMount  ', norm);
    // console.log(this.refs.appname); // object
    this.props.setBleCompo(this.bleMan.getWrappedInstance());
    for (const r of M.ExamRlmObj.objects('RmEttResult')) { // 결과 출력.. // TODO crash .. undefined is not a function
      r.showMyself();
    }
    for (const r of M.ExamRlmObj.objects('RmBpResult')) { // 결과 출력..
      r.showMyself();
    }
  }

  // //////////////////////////////////////////////////  _//////////////////_   REALM DB
  realmDBTest() {
    const rmExams = M.ExamRlmObj.objects('RmExam');
    console.log(`\n\n Realm Examination : Length >> ${rmExams.length}`);
    // RmExam.removeAll(); // 모두 삭제 명령...
    // RmExam.newExam(`${Math.round(Math.random() * 100)}`);  // 새로 생성...

    // for (const exm of rmExams) {
    //   // exm.showMyself(true);
    // }
    // console.log('\n\n');
  }

  onPressTabbar() {
    console.log('Tab Bar');
  }

  async navigate2operate(mode, isETT) {
    console.log(`navigate2operate : ${mode}   isETT ? ${isETT}`);
    try {
      if (isETT) {
        await this.props.setEttMode(mode);
        Actions.sceneOperate();
      } else {
        await this.props.setBpMode(mode);
        Actions.sceneBpOper();
      }
    } catch (error) {
      console.log('\n\n\n navigate2operate error\n\n\n');
    }
  }

  getMainButton(idx) {
    const { conState, iETT } = this.props.bleRdcr;
    const conStt = conState === 'Connected';
    const { langMain, langKey } = this.props.uiState;
    const { ETTexplain, LMAexplain, BPTrainingExplain, BPRealTestExplain, IVexplain } = langMain[langKey];
    let title, expln, onClickAction;

    switch (idx) {
      case 1:
        title = 'ETT';
        expln = ETTexplain;
        if (conStt && !iETT) { return null; }
        // expln = '후두경을 사용하여 기관내의 성문에 \n튜브를 삽관하는 훈련입니다.';
        // if (conStt || debugMode) {
        onClickAction = () => {
          this.navigate2operate('ETT', true);
        }; // }
        break;
      case 2:
        title = 'LMA';
        expln = LMAexplain;
        if (conStt && !iETT) { return null; }
        // expln = '후두경을 사용하여 기관내의 성문에 \n튜브를 삽관하는 훈련입니다.';
        onClickAction = () => {
          this.navigate2operate('LMA', true);
        };
        break;
      case 3:
        title = 'ETC';
        expln = LMAexplain;
        if (conStt && !iETT) { return null; }
        // expln = '후두경을 사용하여 기관내의 성문에 \n튜브를 삽관하는 훈련입니다.';
        onClickAction = () => {
          this.navigate2operate('ETC', true);
        };
        break;

      case 11:
        title = 'BP\nReal';
        expln = BPRealTestExplain;
        if (conStt && iETT) { return null; }
        // if (conStt || debugMode) {
        onClickAction = () => {
          // this.props.changeMainState('bpTrain');
          this.navigate2operate('REAL', false);
        }; // }
        break;
      case 12:
        title = 'BP\nTrain';
        expln = BPTrainingExplain;
        if (conStt && iETT) { return null; }
        // if (conStt || debugMode) {
        onClickAction = () => {
          // this.props.changeMainState('bpTrain');
          this.navigate2operate('TRAIN', false);
        };
        // }
        break;
      case 33:
        title = 'IV'; expln = IVexplain;
        // onClickAction = () => //BleLinkconModule.show('Awesome', BleLinkconModule.SHORT);
        break;
      default:
        title = 'LMA';
        expln = '후두경을 사용하지 않고 후두마스크를 \n성문 앞의 위치까지 삽입하는 훈련입니다.';
        // onClickAction = () => this.retrieve();
        break;
    } // onPressCallback={Actions.sceneOperate}

    if (conState === 'Scanning') { onClickAction = null; }

    const { height } = this.props.main.scr;
    const bttnWidth = height * 0.15;
    const bttnColor = conStt ? HtColor.bgBlue : '#CCC';
    return (
      <View style={sty.bttnView} >
        <HtRectBttn
          // ref={r => this.appname = r}
          len={bttnWidth}
          borderR={10}
          bgCol={bttnColor}
          txtCol={'#EEFFEE'}
          txtRatio={0.3}
          ptext={title}
          onPressCallback={onClickAction}
        />
        <Text style={sty.explainText}>
          {expln}
        </Text>
      </View>
    );
  }

  getSettingButton(idx) {
    const { uicommon, langKey } = this.props.uiState; // 언어팩..
    const { allReports, globalSetting, ettSetting, bpSetting } = uicommon[langKey];

    let title, onClickAction;
    switch (idx) {
      case 0: // 디버깅 용..
        title = `${this.state.cnt}`;
        onClickAction = this.debugMethod.bind(this);
        break;
      case -1:
        title = allReports;
        onClickAction = Actions.sceneReports;
        break;
      case 1:
        title = globalSetting;
        onClickAction = Actions.sceneGlobalSetting;
        break;
      case 2:
        title = ettSetting;
        onClickAction = Actions.sceneSetting;
        break;
      case 3:
        title = bpSetting;
        onClickAction = Actions.sceneBpSetting;
        break;
      default:
        title = '언 어\n변 경';
        onClickAction = () => {
          console.log(`\n\n\n  language key  ${this.state.langIdx}`);
          const langIdx = this.state.langIdx === 0 ? 1 : 0;
          this.changeLanguage(langIdx);
        };
        break;
    } // onPressCallback={Actions.sceneOperate}
    const { height } = this.props.main.scr;
    const bttnWidth = height * 0.1;
    return (
      <HtRectBttn
        len={bttnWidth}
        borderR={5}
        bgCol={HtColor.bgBlue}
        txtCol={'#EEFFEE'}
        txtRatio={0.25}
        ptext={title}
        onPressCallback={onClickAction}
      />
    );
  }

  // 언어 변경 함수..
  changeLanguage(idx) {
    this.props.languageChange(idx);
    setTimeout(() => {
      const { langMain, langKey } = this.props.uiState;
      console.log(`set state ............................${idx}`);
      this.setState({ langIdx: idx, langObj: langMain[langKey] });
    }, 70);
  }

  buttonAction() {
    console.log(' asdfas fd ');
  }

  getMainButtonsOrActivityIndicator() {
    const { conState } = this.props.bleRdcr;
    if (conState === 'Scanning') {
      return (
        <ActivityIndicator
          animating
          style={[sty.centering, { height: 80 }]}
          size="large"
        />
      );
    }
    return (
      <View style={[sty.rowContainer, { marginTop: 50, margin: 20 }]}>
        {this.getMainButton(1)}
        {this.getMainButton(2)}
        {this.getMainButton(3)}
        {this.getMainButton(11)}
        {this.getMainButton(12)}
      </View>
    );
  }

  // //////////////////////////////////////////////////  _//////////////////_   render
  render() {
    const { langMain, langKey } = this.props.uiState;
    const { AppName, SubTitle, MainDirective } = langMain[langKey];
    if (this.props.main.scr === undefined) {
      return <View style={{ flex: 1 }} />;
    }
    const logoScale = 0.3; // spaceRight = 20,
    return (
      <View
        style={sty.viewContainer}
      >
        <Image
          source={require('../resource/background/bg_artwork.jpg')}
          style={sty.backgroundImage}
        />
        <BleManView ref={r => this.bleMan = r} />

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}

        <View style={[sty.container, { flex: 62 }]}>
          <Text style={[sty.titleCommon, sty.title]}>{AppName}</Text>
          <Text style={[sty.titleCommon, sty.subTitle]}>{SubTitle}</Text>
          <Text style={[sty.titleCommon, sty.comText, { marginTop: 30 }]}>{MainDirective}</Text>
          {/*  <<<< 메인 버튼 >>>> */}

          {this.getMainButtonsOrActivityIndicator()}

        </View>

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}

        <View style={[sty.container, { flex: 10, flexDirection: 'row', alignSelf: 'center' }]}>
          <View style={[sty.bottom3button, { justifyContent: 'flex-start' }]} >
            {this.getSettingButton(-1)}
          </View>
          <Image // 로고 이미지 .. ...
            style={{ flex: 2, width: 424 * logoScale, height: 106 * logoScale, marginTop: 20 }}
            source={require('../resource/imageCommon/logo.png')}
          />
          <View style={sty.bottom3button} >
            {this.getSettingButton(1)}
            {this.getSettingButton(2)}
            {this.getSettingButton(3)}
          </View>
        </View>
      </View>
    );
  }

  toggle() {
    // console.log('toggle');
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  updateMenuState(isOpen) {
    // console.log(`updateMenuState   >> isOpen ${isOpen}`);
    this.setState({ isOpen });
  }
}

const sty = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    justifyContent: 'center',
    backgroundColor: '#FFF0',
  },
  viewContainer: {
    alignItems: 'stretch', justifyContent: 'center', margin: 0, marginTop: 0,
    flex: 1, alignSelf: 'stretch',
  },
  backgroundImage: {
    position: 'absolute', width: '100%', height: '100%', top: 0, left: 0,
    // alignItems: 'stretch', justifyContent: 'center', margin: 0, marginTop: 0,
    // flex: 1, alignSelf: 'stretch', width: null, height: null,
    // resizeMode: 'stretch', // or 'stretch'
  },
  bottom3button: {
    flex: 5, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',
    marginHorizontal: 20,
  },
  rowContainer: { flexDirection: 'row', alignSelf: 'stretch' },
  seperateLine: { height: 2, backgroundColor: '#AAAAAA' },
  bttnView: { flex: 3, alignItems: 'center' },
  //  View Component Styles...
  titleCommon: {
    textAlign: 'center', alignSelf: 'stretch', fontWeight: '300', margin: 5 },
  title: { fontSize: 50, fontWeight: '700', color: HtColor.bgDarkGray, margin: 30 },
  subTitle: { fontSize: 25, fontWeight: '100', color: HtColor.bgDarkGray },
  comText: { fontSize: 20, fontWeight: '100', color: HtColor.bgDarkGray },
  explainText: { // 설명 텍스트 스타일..
    textAlign: 'center', // ('auto', 'left', 'right', 'center', 'justify')
    fontSize: 17, fontWeight: '100', margin: 10, color: '#113355',
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
});

const mapStateToProps = state => ({
  main: state.main,
  bleRdcr: state.bleRdcr,
  OperBP: state.OperBP,
  EttSetting: state.EttSetting,
  theCounter: state.timerCount,
  arrPacket: state.arrPacket,
  uiState: state.uiState,
});

export default connect(mapStateToProps, actions)(SceneMain);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
