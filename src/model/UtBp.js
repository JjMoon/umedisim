/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / model / UtBp.js
 *
 * Created by Jongwoo Moon on 2017. 4. 20..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';

export function getCurBpSettingObj(bpReducer) {
  const { mode, setting } = bpReducer;
  return (mode === 'REAL') ? setting.real : setting.train;
}

export function periodInSec(sta, end, rnd) {
  return _.round((end.getTime() - sta.getTime()) * 0.001, rnd);
}
