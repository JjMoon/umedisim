/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / model / BpLogic.js
 *
 * Created by Jongwoo Moon on 2017. 4. 18. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import _ from 'lodash';

const bpSample = require('./json/bp_sample.json');

class BpPrsrLogic {
  constructor(max, min) {
    console.log('\n\n  BpPrsrLogic 생성  \n');
    console.log(`    BpPrsrLogic 조건 설정  ${max}   ${min} \n\n`);
    this.vars = { stateID: 0, preVal: 0, started: new Date(), ended: new Date(),
      count: 0 };
    this.cond = { max, min };
  }

  processPressure(prsr) {
    console.log(`  processPressure : ${prsr}`);
    const { stateID, started, ended, count } = this.vars;
    const { max, min } = this.cond;

    let newStateID = stateID, newSta = started, newEnd = ended, cnt = count;

    switch (stateID) {
      case 0: // 초기 증가 상태..
        if (max < prsr) { // 맥스를 넘어서면 넘김...
          newStateID = 50;
        }
        break;
      case 50: // 맥스 이상의 상태..
        if (prsr < max) { // 맥스보다 작아지면 이때부터 체크..
          newSta = new Date();
          newStateID = 100;
          console.log(` \n\n\n  최고점 통과.. 시간 체크...  Started at ${newSta}  \n\n\n `);
        }
        break;
      case 100: // 맥스와 민 사이의 상태..
        if (prsr > max) { // 다시 50으로...
          newStateID = 50;
        }
        if (prsr < min) { // 다음 단계로.. 측정 완료...
          newStateID = 150;
          newEnd = new Date();
          cnt++;
        }
        break;
      case 150: // min 이하의 상태..  min 보다 클때는 그냥 무시... 맥스를 찍어야 다시 시간 계산..
        if (prsr > max) { // 다시 돌려보내기..
          newStateID = 50;
        }
        break;
      default:
        break;
    }

    if (stateID !== newStateID) {
      console.log(` \n\n\n  상태 변화.... from ${stateID} ==> ${newStateID}  \n\n\n `);
    }

    this.vars = { ...this.vars, stateID: newStateID, started: newSta, ended: newEnd,
      count: cnt };
  }

  finalize() {
    const { started, ended, count } = this.vars;
    const { max, min } = this.cond;
    if (count === 0) {
      console.log(`  \n\n\n  finalize   ERROR   !!!!!   \n\n   ${started}  ${ended} `);
      return;
    }
    console.log(`  \n\n\n  finalize    ${started}  ${ended} `);
    const dueSec = (ended - started) * 0.001;
    const dimSpeed = (max - min) / dueSec;
    console.log(`   실행 시간... ${dueSec}  감압 속도 : ${dimSpeed} `);
    this.dimSpeed = dimSpeed; // BpResultReal, 등 객체에서 사용...
  }
}

class BpLogic {
  // set: { autoCheckItem: {}, manCheckItem: [], general: [] }, ettLogic: null

  init(props) {
    console.log('\n\n ====== ====== ====== ======  [[ BpLogic :: init ]] .....');
    const { main, OperBP } = props;
    //console.log(main); console.log(OperBP); console.log(bpSample);
    this.p = { main, OperBP };
  }

  getDifference(given, answr) {
    const { high, low, bpm } = given;
    const { high: aH, low: aL, bpm: aB } = answr;

    const highDE = getDiffExactPerc(high, aH);
    const lowDE = getDiffExactPerc(low, aL);
    const bpmDE = getDiffExactPerc(bpm, aB);

    return { highDE, lowDE, bpmDE };
  }

  getBpData() {
    const { mode, setting } = this.p.OperBP;
    const setObj = (mode === 'REAL') ? setting.real : setting.train;
    let periodGap = null; // 1 : on, 2 : random, 3: off  청진 간극..
    switch (setObj.period) { // period 는 두 모드 공통. 같은 이름 임..
      case 1:
        periodGap = true;
        break;
      case 2:
        periodGap = (((_.round(Math.random() * 37, 0) % 2) % 2) === 0);
        break;
      default: break;
    }
    const bpStr = ['High', 'Normal', 'Low'];

    // TRAIN 은 혈압 세팅 없슴. 랜덤..
    const bpLevel = (mode === 'REAL') ? setObj.bpLevel : getRandom(0, 2);
    const sample = bpSample[bpStr[bpLevel]];
    console.log(`\n\n\n  BpLogic :: BP Mode : ${mode}  ${bpLevel}  ${sample.length}`);
    //console.log(`\n bpLevel (0고/1정/2저): ${bpLevel}   sample.length : ${sample.length}`);
    const len = sample.length, ran100 = _.round((Math.random() * 1000) % 100, 0),
      obj = sample[ran100 % len];
    if (periodGap) { // null 아니면 up, down 이 있다..
      const diff = obj.H - obj.L, btwUpDn = randoMinimize(0.2 * diff, 15), // 20% 정도 간극.
        up = (obj.H) - randoMinimize(0.1 * diff, 10), down = up - btwUpDn;
      periodGap = { up, down };
    } else {
      periodGap = null;
    }
    console.log(`      periodGap :: ${periodGap}`);
    return { high: randomize(obj.H), low: randomize(obj.L), bpm: randomize(obj.P),
      info: bpStr[bpLevel], periodGap };
  } // curBpVal: { high: 0, low: 0, bpm: 0 },
}

function getDiffExactPerc(given, answer) { // 오차, 정확도
  if (answer === 0) { return { diff: given, cor: 0 }; }
  const ratio = given / answer;
  const cor = _.round((ratio > 1) ? (1 / ratio) * 100 : (ratio * 100), 0);
  const diff = Math.abs(given - answer);
  return { diff, cor };
}

function getRandom(min, max) {  // 2, 3, 4
  const rVal = _.random() * max * 10, denom = (max - min) + 1;
  const val0 = rVal % denom;
  console.log(`  getRandom :: ${rVal} % ${denom}`);
  return val0 + min;
}

function randomize(val) { // 8 이내에서 + - 시킴..
  const ranVal = _.round((Math.random() * 100) % 8, 0);
  return (ranVal % 2) === 0 ? val + ranVal : val - ranVal;
}

function randoMinimize(val, min) { // 8 이내에서 + - 시킴..
  const ranVal = randomize(val);
  return (ranVal < min) ? (min + getRandom(0, 5)) : ranVal;
}

export { BpLogic, BpPrsrLogic };
