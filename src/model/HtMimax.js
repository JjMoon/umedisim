/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / model / HtMimax.js
 *
 * Created by Jongwoo Moon on 2017. 1. 20..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

class HtMimax {
  constructor(ttl, uiMin, uiMax) {
    this.uiVal = { min: uiMin, max: uiMax };
    this.normVal = { min: 0, max: 100 };
    this.title = ttl;
  }

  setMinValue(uiV, normV) {
    this.uiVal.min = uiV;
    this.normVal.min = normV;
  }

  setMaxValue(uiV, normV) {
    this.uiVal.max = uiV;
    this.normVal.max = normV;
  }


}

 export { HtMimax };
