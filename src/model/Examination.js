/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git

  src / model / Examination.js

  Realm Database Schemes of Examination

 * Created by Jongwoo Moon on 2017. 2. 23..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import Realm from 'realm';
import _ from 'lodash';
import { RmHistory, RmRsltBs, RmEttResult, RmBpResult } from './RlmEttSchemes';

///-------------------------   -------------------------     ### Rm Packet ###
class RmPacket extends Realm.Object {
  theObject() {
    return JSON.parse(this.contentJson);
  }

  getMyTS() {
    if (this.tsObj === undefined) {
      this.tsObj = new Date(this.ts);
    }
    if (this.getMyTsInMS === undefined) {
      this.getMyTsInMS = this.tsObj.getTime();
    }
    return this.getMyTsInMS;
  }

  showMyself() {
    console.log(`___ packet ___   ${this.ts}  packet ? ${this.isPacket}`);
    console.log(`     ${this.contentJson}`);
    this.obj = JSON.parse(this.contentJson);
    if (this.isPacket) {
      console.log(`     Packet :: this.obj 로 사용 ${this.obj.thr}  ${this.obj.blly}`);
    } else {
      console.log(`     Check  :: this.obj 로 사용 ${this.obj.thr}  ${this.obj.blly}`);
    }
  }

  isManual() {
    return !this.isPacket;
  }
}

RmPacket.schema = {
  name: 'RmPacket',
  properties: {
    ts: 'string',
    isPacket: 'bool',
    contentJson: { type: 'string', default: '{}' }
  }
};
// { "thr" : [ false, false, false ], "glcnbl": [ false, false, false ],
//   "nckAng": 3, "nckDst": 4, "blly": 0, "tth": 2 },
// { expanded: false, pass: false, scoreMarked: 0, disable: false };
// { "shr": "기록 명령", "long": "수행한 술기를 의말하는가?", "score": 10, "auto": false }


// 클래스.
class RmExam extends Realm.Object {
  // packets : RmPacket 객체.

  static newExam(nm) {
    ExamRlmObj.write(() => {
      return ExamRlmObj.create('RmExam', {
        examName: nm,
        startTime: (new Date()).toString(),
        endTime: (new Date()).toString()
      });
    });
  }

  static saveExam(exam) {
    ExamRlmObj.write(() => {
      return ExamRlmObj.create('RmExam', {
        examName: exam.nm,
        startTime: exam.startTime,
        endTime: (new Date()).toString(),
        packets: exam.packets
      });
    });
  }

  static removeAll() {
    ExamRlmObj.write(() => {
      const allObj = ExamRlmObj.objects('RmExam');
      ExamRlmObj.delete(allObj);
    });
  }

  setName(examName) {
    ExamRlmObj.write(() => {
      this.examName = examName;
    });
  }

  deleteMyself() {
    ExamRlmObj.write(() => {
      ExamRlmObj.delete(this);
    });
  }

  examDuration() {
    this.staObj = new Date(this.startTime);
    this.endObj = new Date(this.endTime);
    //console.log(`  duration >> ${this.staObj.getTime()}  ${this.endObj.getTime()}`);
    return _.round((this.endObj.getTime() - this.staObj.getTime()) * 0.001, 1);
  }

  addPacket(pck, isPacket = true) {
    const contentJson = JSON.stringify(pck);
    const nowStr = (new Date()).toString();

    ExamRlmObj.write(() => {
      const nPck = ExamRlmObj.create('RmPacket', {
        ts: nowStr,
        isPacket, contentJson
      }, true);
      this.endTime = nowStr;
      this.packets.push(nPck);
    });
  }

  showMyself(printAllPacket) {
    //const startTS = new Date(this.startTime);
    console.log('RmExam showMyself  _____ _____ _____ _____ _____ _____ _____');
    console.log(`RmExam Object << name : ${this.examName}`);
    console.log(`RmExam    started at ${this.startTime}  to ${this.endTime}`);
    console.log(`  패킷 수 : ${this.packets.length}  duration : ${this.examDuration()}`);

    if (printAllPacket) {
      for (const p of this.packets) {
        p.showMyself();
      }
    }
  }

  totalScore() { // 패킷 내에 점수가 있슴.
    return _.chain(this.packets)
      .filter((pck) => { return pck.isManual(); })
      .map((obj) => { return obj.theObject().score; })
      .reduce((val, n) => { return val + n; }, 0)
      .value();
  }
}

// Realm Scheme
RmExam.schema = {
  name: 'RmExam',
  properties: {
    examName: 'string',
    startTime: 'string',
    endTime: 'string', // { type: 'int', default: 34567 },
    packets: { type: 'list', objectType: 'RmPacket' }
  }
};

const ExamRlmObj = new Realm({ schema: [RmPacket, RmExam, RmHistory, RmRsltBs, RmEttResult, RmBpResult] });

export { ExamRlmObj, RmPacket, RmExam, RmHistory, RmEttResult, RmBpResult };
