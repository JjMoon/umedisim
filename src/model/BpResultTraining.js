/**
* Bit Src Project / UMediSim
* git@bitbucket.org:JjMoon/umedisim.git
*
* src / model / BpResultTraining.js
*
* Created by Jongwoo Moon on 2017. 4. 20..
* Copyright © 2017년 Jongwoo Moon. All rights reserved.
*/
'use strict';

//import _ from 'lodash';
import * as M from '../model';

class BpResultTraining {
  getResultScore(state) {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ BpResultTraining :: getResultScore  ...\n');
    const { uxOnce, curBpVal, curAnswr, playObj, prsrLogic } = state;
    const { touched, cuffed } = uxOnce;
    const setObj = M.getCurBpSettingObj(state);
    const { startedAt, finAt } = playObj; // 설정 혈압 값.

    // 최종 어레이
    const finScrs = [];

    // "커프 설치"   cuffed: { v: false, ts: null },
    finScrs.push(cuffed.v);
    // "상완 촉진"
    finScrs.push(touched.v);
    // "청진 간극"   간극 유무를 맞춰야 함..  //console.log(`   herehere >> ${curBpVal.periodGap}   ${curAnswr.periodGap}  `);
    const gapVal = !(curBpVal.periodGap === null);
    finScrs.push(gapVal === curAnswr.periodGap);

    // "적정 가압"
    const limitHighBP = curBpVal.high + setObj.pressStandard;
    console.log(`  허용 최대값 : ${limitHighBP}  > ?  ${uxOnce.maxPres.v}`);
    finScrs.push(uxOnce.maxPres.v < limitHighBP);

    //"적정 감압 속도",
    // 적정 감압 속도 [7]  2 ~ 5 사이면 합격..
    const dimSuccess = (prsrLogic.dimSpeed > 2 && prsrLogic.dimSpeed < 5);
    console.log(`\n\n   감압 합격 ${dimSuccess}      dimSpeed  ${prsrLogic.dimSpeed} 2, 5 사이 ?...\n\n `);
    finScrs.push(dimSuccess);

    // "실습 시간 준수"
    const limitTotalTime = setObj.examTime * 60,
      usedTime = M.periodInSec(startedAt, finAt, 0);
    console.log(`  허용 최대 시간 : ${limitTotalTime} > ${usedTime}`);
    finScrs.push(usedTime < limitTotalTime);

    const newResult = { finScrs };
    return { ...state, newResult };
  }
}

export default BpResultTraining;
