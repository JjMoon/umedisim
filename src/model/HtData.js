/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/medisim_ett.git

  src / model / HtData.js

  Realm Database Schemes etc

 * Created by Jongwoo Moon on 2017. 1. 24..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import Realm from 'realm';

/**
  Realm Schema
  앱 Global 데이터 저장. 키/밸류 쌍으로 저장.
*/
const MetaAppSchema = {
  name: 'MetaApp',
  properties: {
    keyName: 'string',
    valInt: { type: 'int', default: 0 },
    valFlt: { type: 'float', default: 0 },
    valString: 'string'
  }
};

class AppMetaData {
  static addObject(key, vInt, vFloat, vString = 'Non') {
    RealmGObj.write(() => {
      RealmGObj.create('MetaApp', {
        keyName: key,
        valInt: vInt, valFlt: vFloat,
        valString: vString
      });
    });
  }

  static removeAll() {

  }

  showMyself() {
    console.log(`AppMetaData :: ${this.keyName} ${this.valInt}    ${this.valString}`);
  }
}

AppMetaData.schema = MetaAppSchema;

//const RealmGObj = new Realm({ schema: [AppMetaData] });


export { MetaAppSchema, AppMetaData };

//
// class Person {
//   get ageSeconds() {
//     return Math.floor((Date.now() - this.birthday.getTime()));
//   }
//   get age() {
//     return ageSeconds() / 31557600000;
//   }
// }
//
// Person.schema = PersonSchema;
//
// // Initialize a Realm with Car and Person models
// const realm = new Realm({ schema: [CarSchema, PersonSchema] });
