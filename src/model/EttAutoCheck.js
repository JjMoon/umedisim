/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / model / EttAutoCheck.js
 *
 * Created by Jongwoo Moon on 2017. 4. 13. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import _ from 'lodash';

const langAutoChk = require('../data/langAutoCheckItems.json');

/** */
class EttAutoCheck {
  // set: { autoCheckItem: {}, manCheckItem: [], general: [] }, ettLogic: null
  init(setObj, langKey) {
    this.set = setObj;
    this.langKey = langKey;

    this.general = {};
    for (let i = 0; i < setObj.general.length; i++) {
      const obj = setObj.general[i];
      console.log(obj);
      this.general = { ...this.general, [obj.key]: obj };
    }
    // this.setNeckAng = _.filter(setObj.general, (o) => { return o.key === 'neckAngleInDegree'; })[0];
  }

  // 제한 시간 초과 여부   timeOutLimit
  getTimeoutRatio(startedAt) {
    const ttlTime = (new Date() - startedAt) * 0.001;
    const ratio = (this.general.timeOutLimit.value * 60 > ttlTime) ? 1 : 0;
    console.log(`  timeout Check : ${this.general.timeOutLimit.value} min > ${ttlTime}  ?? `);
    return ratio;
  }

  // 호흡간 시간 간격. 호흡 시작 시간만으로 판단..
  getBrthPeriod(arrBrth) {
    const bnum = arrBrth.length, arrStartTime = _.map(arrBrth, (o) => o.state.started);
    if (bnum < 2) { return 1; }
    const prevObj = arrStartTime[0];
    let periods = [];
    for (let i = 1; i < arrStartTime.length; i++) {
      periods = [...periods, (arrStartTime[i] - prevObj) * 0.001];
    }
    const meanSec = _.reduce(periods, (sum, n) => sum + n, 0) / (bnum - 1);
    const squreOfDiff = _.map(periods, (o) => Math.pow(o - meanSec, 2));
    const diff = _.reduce(squreOfDiff, (sum, n) => sum + n, 0) / (bnum - 1);
    // 40 % 편차 0점..  >> 0.16
    const stand = 0.16;
    if (diff > stand) { return 0; } // 0 점
    if (diff < 0.0001) { return 1; } // 만점..
    return 1 - (diff / stand);
  }

  // 호흡 시간 편차  // 여기서 pass, over, below 값도 산출하자..
  getBrthDiff(arrBrth) {
    const bnum = arrBrth.length, arrTotalSec = _.map(arrBrth, (o) => o.getTotalSec());
    const { min, max } = this.general.breathPeriod.range; //
    const passNum = _.filter(arrTotalSec, (o) => (o > min) && (o < max)).length;
    const overNum = _.filter(arrTotalSec, (o) => (o > max)).length;
    const belowNum = _.filter(arrTotalSec, (o) => (o < min)).length;
    const meanSec = _.reduce(arrTotalSec, (sum, n) => sum + n, 0) / bnum;
    const squreOfDiff = _.map(arrTotalSec, (o) => Math.pow(o - meanSec, 2));
    const diff = _.reduce(squreOfDiff, (sum, n) => sum + n, 0) / bnum;
    console.log(`  호흡 시간 편차 : ${diff}  `);
    console.log(`  prd P | O | B : ${passNum} | ${overNum} | ${belowNum}`);
    // 40 % 편차 0점..  >> 0.16
    const stand = 0.16;
    const numbers = { passNum, overNum, belowNum };
    if (diff > stand) { return { ratio: 0, ...numbers }; } // 0 점
    if (diff < 0.0001) { return { ratio: 1, ...numbers }; } // 만점..  //return 1 - (diff / stand);
    return { ratio: 1 - (diff / stand), ...numbers };
  }

  // 호기 시간 정확도
  getBrthExhale(arrBrth) {
    const { min, max } = this.general.breathExhl.range;
    console.log(`  ...   ${min}  ${max}  ${arrBrth.length}  ${arrBrth[0].getExhaleSec()} `);
    const arrEx = _.map(arrBrth, (o) => o.getExhaleSec());
    const passNum = _.filter(arrEx, (o) => (o < max) && (o > min)).length;
    const overNum = _.filter(arrEx, (o) => max < o).length;
    const belowNum = _.filter(arrEx, (o) => min > o).length;
    const avrExhale = _.reduce(arrEx, (sum, n) => sum + n, 0) / arrEx.length;
    console.log(`  호기 시간 합격 갯수 : ${passNum} / ${arrBrth.length}`);
    console.log(`  ex P | O | B : ${passNum} | ${overNum} | ${belowNum}   avr : ${avrExhale}`);
    return { ratio: passNum / arrBrth.length, passNum, overNum, belowNum, avrExhale };
  }

  // 호흡 량..  breathRange 사이에 든 호흡 갯수로 점수..
  getBrthVolumnRatio(arrVol) {
    const { min, max } = this.general.breathRange.range;
    const passNum = _.filter(arrVol, (o) => (o > min) && (o < max)).length;
    const overNum = _.filter(arrVol, (o) => (o > max)).length;
    const belowNum = _.filter(arrVol, (o) => (o < min)).length;
    console.log(`  호흡량 합격 갯수 : ${passNum} / ${arrVol.length}  :: ${passNum} | ${overNum} | ${belowNum}`);
    return { ratio: passNum / arrVol.length, passNum, overNum, belowNum };
  }

  // 호흡 횟수 정확도  brthCount
  getBrthCountScoreFactor(num) {
    const ratio = (num / this.general.breathNum.value);
    if (ratio > 1) {
      return 1 / ratio;
    }
    return ratio;
  }

  // 목 각도 판별
  getNeckStr(nckAng, nckDst) {
    const { min, max } = this.general.snippingPositionCm.range;
    let neck = 'F';
    if (nckAng > this.general.neckAngleInDegree.value) {
      neck = ((min < nckDst) && (nckDst < max)) ? 'P' : 'A';
    }
    return neck;
  }

  getAutoCheckItem(key) {
    return this.set.autoCheckItem[key];
  }

  getItemString(key) {
    console.log(langAutoChk);
    console.log(langAutoChk[this.langKey]);
    return langAutoChk[this.langKey][key];
  }

  // 삽관 판별.
  getInsertStr(thr) {
    if (!(thr[0] || thr[1] || thr[2])) {
      return 'NONE';
    }
    // thr[0] 부족, 정상, 과다..
    let insert = 'NORM'; // NORM, UNDR, OVER
    insert = thr[0] ? 'UNDR' : insert;
    insert = thr[2] ? 'OVER' : insert;
    return insert;
  }

  // 식도 삽관..
  gullet(glcnbl) {
    return glcnbl[0];
  }

  // 이빨 압력 ..  세팅의 % 값..  100 초과 가능...
  getTeethPercent(tth) {
    return (tth / this.general.teethBreakPointN.value) * 100;
  }

  // 복부 팽만 (백밸브 마스크)
  getStomachMask(glcnbl) {
    return !glcnbl[0] && glcnbl[2];
  }
  // 복부 팽만 (식도 삽관)
  getStomachGullet(glcnbl) {
    return glcnbl[0] && glcnbl[2];
  }
}

export { EttAutoCheck };
