/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / model / HtLogicalRandom.js

  this.packLogic = new HtLogicalRandom({
    max: 100, min: 0,
    countPosition: 'MAX', // 'MIN'
    variationLimit: 20,
    doNotPassMax: true, doNotPassMin: false,
    tolerance: 5,
    countPosition: 'MAX'


 * Created by Jongwoo Moon on 2017. 2. 5..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

class HtLogicalRandom {
  constructor(props) {
    this.p = props; // constants 이하는 변수들...
    this.isIncreasing = true;
    this.curValue = this.p.min;
    this.cnt = 0;
    // finishing action..
    this.finished = false;
  }

  checkMax() {
    if (this.p.max < this.curValue) {
      this.isIncreasing = false;

      if (this.p.countPosition === 'MAX') { this.cnt += 1; }

      if (this.p.doNotPassMax) { // 넘지 말아야 하므로...
        this.curValue = this.p.max - (this.p.tolerance * Math.random());
      }
    }
  }

  checkMin() {
    if (this.curValue < this.p.min) {
      this.isIncreasing = true;

      if (this.p.countPosition === 'MIN') { this.cnt += 1; }

      if (this.p.doNotPassMin) { // 넘지 말아야 하므로...
        this.curValue = this.p.min + (this.p.tolerance * Math.random());
      }
    }
  }

  getNewValue() {
    const newVar = Math.random() * this.p.variationLimit;

    if (this.isIncreasing) {
      this.curValue += newVar;
      this.checkMax();
    } else {
      this.curValue -= newVar;
      this.checkMin();
    }
    return this.curValue;
  }

}

export { HtLogicalRandom };
