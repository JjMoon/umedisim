/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / model / RlmEttSchemes.js
 *
 * Created by Jongwoo Moon on 2017. 5. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import Realm from 'realm';
import _ from 'lodash';

import { ExamRlmObj } from './Examination';

class RmHistory extends Realm.Object {
  showMyself() {
    console.log(`   ___ RmHistory  name : ${this.shr} auto : ${this.isAuto}  score : ${this.score}`);
    console.log(`       ${this.ts} `);
  }
}

RmHistory.schema = {
  name: 'RmHistory',
  properties: {
    ts: 'string',
    isAuto: 'bool', score: 'int',
    shr: { type: 'string', default: '-' },
    long: { type: 'string', default: '-' },
    contentJson: { type: 'string', default: '{}' }
  }
};

class RmRsltBs extends Realm.Object {

  changeEmail(newMail) {
    ExamRlmObj.write(() => {
      this.email = newMail;
    });
  }

  showMyself() {
    console.log(`       ${this.arrImgJson} `);
    console.log(this.arrPacket);
  }
}

RmRsltBs.schema = {
  name: 'RmRsltBs',
  properties: {
    arrPacket: 'string',
    mode: 'string', stdntInfo: 'string', email: 'string',
    examName: 'string', startTime: 'string', endTime: 'string',
    totalScore: 'int',
    arrImgJson: 'string' // [ 'folder/img1', 'folder/img2' ] 이런식으로 처리해야 함... 몇개 안되니..
  }
};


///-------------------------   -------------------------     ### ETT Result ###
class RmEttResult extends Realm.Object {
  // packets : RmPacket 객체.

  static newEttResult(histrs, arrPckt, rest) {
    const nowTs = new Date();
    const initName = `M${nowTs.getMonth() + 1}/${nowTs.getDate()} || ${nowTs.getHours()}:${nowTs.getMinutes()} `;

    ExamRlmObj.write(() => {
      const bsObj = ExamRlmObj.create('RmRsltBs', {
        ...rest, arrPacket: JSON.stringify(arrPckt),
        examName: initName, startTime: nowTs.toString(),
        endTime: nowTs.toString()
      }, true);

      const newRslt = ExamRlmObj.create('RmEttResult', {
        base: bsObj
      }, true);

      // let ttlScr = 0;
      //
      for (const his of histrs) {
        const ts = his.ts.toString(), isAuto = his.auto,
          score = isAuto ? his.score : his.scoreMarked;
        //ttlScr += score;
        newRslt.arrHistory.push({
          ts, isAuto, score,
          shr: his.shr
        });
      }
      // newRslt.totalScore = ttlScr;
      newRslt.showMyself();
    });
  }

  addHistory(his) {
    console.log('  add history');
    ExamRlmObj.write(() => {
      const newObj = ExamRlmObj.create('RmHistory', {
        ts: '',
        isAuto: his.auto, score: 0
      }, true);
      this.arrHistory.push(newObj);
    });
  }

  static removeAll() {
    ExamRlmObj.write(() => {
      const allObj = ExamRlmObj.objects('RmEttResult');
      ExamRlmObj.delete(allObj);
    });
  }

  deleteMyself() {
    ExamRlmObj.write(() => {
      ExamRlmObj.delete(this);
    });
  }

  showMyself() {
    //const startTS = new Date(this.startTime);
    const { mode, stdntInfo, examName, startTime, endTime, totalScore } = this.base;
    const sep = ' \t =====  ===== ===== ===== ===== =====';
    console.log(`\n \t${sep} \t ${sep}  RmEttResult ${stdntInfo}  ^^^  ${mode}`);
    console.log(`RmEttResult Object << name : ${examName}`);
    console.log(`RmEttResult    started at ${startTime}  to ${endTime}`);
    console.log(`   Total Score : ${totalScore}  History : ${this.arrHistory.length}`);
    _.map(this.arrHistory, (o) => o.showMyself());
    this.base.showMyself();
    console.log(`\n \t${sep} \t ${sep}  RmEttResult showMyself ____`);
  }
}

// Realm Scheme
RmEttResult.schema = {
  name: 'RmEttResult',
  properties: {
    base: { type: 'RmRsltBs' },
    arrHistory: { type: 'list', objectType: 'RmHistory' }
  }
};


///-------------------------   -------------------------     ### ETT Result ###
class RmBpResult extends Realm.Object {

  static newBpResult(rest, arrPckt, restStr) {
    const nowTs = new Date();
    const initName = `M${nowTs.getMonth() + 1}/${nowTs.getDate()} || ${nowTs.getHours()}:${nowTs.getMinutes()} `;

    ExamRlmObj.write(() => {
      console.log(rest);
      const bsObj = ExamRlmObj.create('RmRsltBs', {
        ...rest, arrPacket: JSON.stringify(arrPckt),
        examName: initName, startTime: nowTs.toString(),
        endTime: nowTs.toString()
      }, true);

      const newRslt = ExamRlmObj.create('RmBpResult', {
        base: bsObj,
        restStr
      }, true);

      newRslt.showMyself();
    });
  }

  static removeAll() {
    ExamRlmObj.write(() => {
      const allObj = ExamRlmObj.objects('RmBpResult');
      ExamRlmObj.delete(allObj);
    });
  }

  deleteMyself() {
    ExamRlmObj.write(() => {
      ExamRlmObj.delete(this);
    });
  }

  showMyself() {
    //const startTS = new Date(this.startTime);
    const { mode, stdntInfo, examName, startTime, endTime, totalScore } = this.base;
    const sep = ' \t =====  ===== ===== ===== ===== =====';
    console.log(`\n \t${sep} \t ${sep}  RmBpResult ${stdntInfo}  ^^^  ${mode}`);
    console.log(`RmBpResult Object << name : ${examName}`);
    console.log(`RmBpResult    started at ${startTime}  to ${endTime}`);
    console.log(`   Total Score : ${totalScore}  `);
    this.base.showMyself();
    console.log(`\n \t${sep} \t ${sep}  RmBpResult showMyself ____`);
  }
}

// Realm Scheme
RmBpResult.schema = {
  name: 'RmBpResult',
  properties: {
    base: { type: 'RmRsltBs' },
    restStr: { type: 'string', default: '-' },
  }
};

export { RmRsltBs, RmHistory, RmEttResult, RmBpResult };
