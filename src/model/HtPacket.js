/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / model / HtPacket.js
 *
 * Created by Jongwoo Moon on 2017. 1. 24..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';

function parse(bytesFromDevice) {
  // From : [2, 192, 46, 28, 5, 5, 5, 3]
  // To : { "thr" : [ false, false, true ],
  //        "glcnbl": [ false, true, false ],
  //        "nckAng": 45, "nckDst": 4, "blly": 0, "tth": 2 }
  let rObj = { thr: [], glcnbl: [] };
  console.log(`\n\n HtPacket.js :: parse ${bytesFromDevice} \n\n`);

  const [no, bit, bb, cc, d1, d2, ee] = bytesFromDevice;
  const bitArray = showBitValues(bit, 8);  // 0, 1로 분해
  const boolArray = _.map(bitArray, numToBool); // true, false 로 변환..
  rObj.thr = _.slice(boolArray, 2, 5); // 3 ~ 5 번째 ..
  rObj.glcnbl = _.slice(boolArray, 5, 8);
  rObj = { ...rObj, nckAng: bb - 5, nckDst: cc - 5,
    blly: ((d1 - 5) * 250) + (d2 - 5), tth: ee - 5 };
  return rObj;
}

function parseBp(bytesFromDevice) {
  // From : 0x02 0xAA 0xBB 0xCC 0xD1 0xD2 0x03
  // To : { "pls" : 90, "prsr": 190, "plsItnst": 1,
  //   "sound": 4, "sndItnst": 5, "cff": true, "tch": [false, false], "gap": false},
  const [, aa, bb, cc, d1, d2] = bytesFromDevice;
  const pls = aa; // 맥박수.

  const arrBB = showBitValues(bb, 8);
  const plsItnst = numberFromBinary(arrBB, 0, 1);
  const sndItnst = numberFromBinary(arrBB, 2, 4);
  const sound = numberFromBinary(arrBB, 5, 7);

  const arrCC = showBitValues(cc, 8); // 0xCC 값 분해...
  const boolCC = _.map(arrCC, numToBool);
  const cff = boolCC[1], tch = [boolCC[2], boolCC[3]], gap = boolCC[4];
  const prsr = ((d1 - 5) * 250) + (d2 - 5); // 혈압..

  console.log(`\n\n HtPacket.js :: parse ${bytesFromDevice} 혈압 : ${prsr} \n\n`);
  return { pls, prsr, plsItnst, sound, sndItnst, cff, tch, gap };
}

function numberFromBinary(arr, sIdx, eIdx) { // [ 1, 0, 1, 1], 2, 3  ==> 3..
  const expo = eIdx - sIdx;
  let rVal = 0, k = 0;
  for (let i = sIdx; i <= eIdx; i++) {
    const curBit = arr[i];
    rVal += Math.pow(2, expo - k++) * curBit;
  }
  return rVal;
}

// _.map(bitArray, numToBool); [true, false, false, true] 이렇게 분해.
function numToBool(n) {
  if (n === 1) {
    return true;
  }
  return false;
}

function showBitValues(num, max) { // [0, 1, 1, 0] 이런 어레이 리턴..
  const rArr = [];
  for (let i = max - 1; i >= 0; i--) {
    rArr.push((num >> i) % 2);
  }
  //console.log(`  ${num} >>  Result ::  ${rArr}`);
  return rArr;
}

export { parse, parseBp, numToBool, showBitValues, numberFromBinary };
