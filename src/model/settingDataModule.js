/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / model / settingDataModule.js
 *
 * Created by Jongwoo Moon on 2017. 3. 23..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';
import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import _ from 'lodash';

/**
  앱 Setting 데이터 저장
*/



//  BP 관련 세팅...

class SettingModule extends Component {
  ////////////////////////////////////////////////////   _//////////////////_   초기 상태
  constructor(props) {
    super(props);

    this.init_bpRealTestSetting = {
      bpLevel: 0, method: 0, pulseSense: 1, soundSense: 1, period: 1, // picker related...
      bloodShrink: 200, bloodRelax: 100, pulseNum: 80,
      examTimeMin: 3, pressStandard: 30,
      arrScore: [10, 10, 10, 10, 10, 10, 10, 10, 10]
    };
    this.init_bpTrainSetting = {
      method: 0, pulseSense: 1, soundSense: 1, period: 1,
      examTime: 3, pressStandard: 30,
    };
    // this.init_sensorCaliValue = {
    //   neckAngleInDegree: { val: 30 },
    //   snippingPositionCm: { min: 5, max: 15 },
    //   teethBreakPointN: { val: 40 },
    //   breathNum: { val: 50 },
    //   breathExhl: { min: 0.8, max: 1.2 },
    //   breathRange: { min: 500, max: 600 },
    //   breathPeriod: { min: 5, max: 6 },
    //   timeOutLimit: { val: 1000 }
    // };
    this.init_ettGeneralSet = require('./json/init_ettGeneralSet.json');
    this.init_ettAutoCheckValues = require('./json/init_ettAutoCheckValues.json');
    this.init_ettManualItems = require('./json/init_ettManualItems.json');
    this.init_system = require('./json/init_system.json');
  }

  ////////////////////////////////////////////////////   _//////////////////_   기본값 쓰기
  // 기본값 쓰기, 키값에 해당하는 객체 불러오기, 리듀싱 하는 콜백 호출..
  // 기본 값은 'init' + key 스트링으로 하자..
  async checkInit(key, callBackFunc) {
    const value = await AsyncStorage.getItem(key);  // 읽어오기
    if (typeof value === 'string') {
      const sObj = JSON.parse(value);
      if (_.isObject(sObj)) {
        console.log(`\n\n  checkAndReduce ::  ${key}   ${_.isArray(sObj)}`);
        console.log(` 읽기 성공 ${key}`);

        if (_.isArray(sObj)) { // 어레이 일 경우 idx 키 추가..
          for (let i = 0; i < sObj.length; i++) {
            sObj[i].idx = i; //console.log(sObj[i]);
          }
        }
        if (callBackFunc !== null) {
          callBackFunc(sObj);
        }
      }
    } else {
      console.log(`\n\n  checkAndReduce ::  ${key}`);
      console.log(`\n\n 기존 기록 없슴. ..  {{ 초기에 기본값 쓰기 }} ${key}`);
      const initObj = this[`init_${key}`];
      const str = JSON.stringify(initObj);
      console.log(str);
      this.saveData(key, str);
      if (callBackFunc !== null) {
        callBackFunc(initObj);
      }
    }
  }

  ////////////////////////////////////////////////////   _//////////////////_   SAVE
  // 수정 후 여기서 쓰기 작업 수행..
  saveObject(ky, obj) {
    const str = JSON.stringify(obj);
    this.saveData(ky, str);
  }

  async saveData(ky, content) {
    console.log(`\n\n Save Data ::  ${ky} : ${content}`);
    try {
      await AsyncStorage.setItem(ky, content);
    } catch (error) {
      console.log('save error');
    }
  }


  // async checkValueCallBack(key, callBackFunc) {
  //   const value = await AsyncStorage.getItem(key);  // 읽어오기
  //   if (typeof value === 'string') {
  //     const sObj = JSON.parse(value);
  //     if (_.isObject(sObj)) {
  //       console.log(` 읽기 성공 ${key}`);
  //       callBackFunc(sObj);
  //     }
  //   } else {
  //     console.log('\n\n 기존 기록 없슴    .....  \t\t\t\t\t .. Error. ..');
  //   }
  // }


  settingDataProcess(callBackFunc) {
    console.log('\n\n\n _____  SettingModule  _____  _____  _____ ');
    //this.setCaliValues(callBackFunc);
  }



  // checkEttManualItem() {
  //   this.checkSavedItem('ettManualItems', defaultItemsOri);
  // }

  // checkExistanceOfBpSetting() {
  //   console.log('\n class SettingModule ::  _____  _____  _____  BP 초기 세팅 유무 검토 ');
  //   this.checkSavedItem('bpRealTestSetting', initBpRealTestSetting);
  //   this.checkSavedItem('bpTrainSetting', initBpTrainSetting);
  // }

  async setCaliValues(callBackFunc) {
    try {
      const value = await AsyncStorage.getItem('calisetting');  // 읽어오기
      if (value !== null) {
        console.log('\n\n 데이터 읽어 오기 :: setCaliValues ');
        console.log(`Retrieve Data :: ${value}`);
        this.calisetting = JSON.parse(value);
      } else { // null case ..  기본값 저장..
        console.log('\n\n setCaliValues :: defaultSensorCaliValue 저장 ');
        const str = JSON.stringify(defaultSensorCaliValue);
        this.calisetting = defaultSensorCaliValue;
        this.saveData('calisetting', str);
      }
      callBackFunc();
    } catch (error) {
      console.log('value not found');
    }
  }

  async checkSavedItem(key, initObj) {
    const value = await AsyncStorage.getItem(key);  // 읽어오기
    if (typeof value === 'string') {
      const sObj = JSON.parse(value);
      if (_.isObject(sObj)) {
        console.log(` 읽기 성공 ${key}`);
      }
    } else {
      console.log('\n\n 기존 기록 없슴. ..');
      const str = JSON.stringify(initObj);
      console.log(str);
      this.saveData(key, str);
    }
  }

  async retrieveBpSetting(key, callBackFunc) {
    const value = await AsyncStorage.getItem(key);  // 읽어오기
    if (typeof value === 'string') {
      const sObj = JSON.parse(value);
      if (_.isObject(sObj)) {
        console.log(` 읽기 성공 ${key}`);
        callBackFunc(sObj);
      }
    } else {
      console.log('\n\n bpTrainSetting :: 기존 기록 없슴. ..');
    }
  }

  ////////////////////////////////////////////////////   _//////////////////_   RETRIEVE
  async retriveData(ky, reposi) {
    try {
      const value = await AsyncStorage.getItem(ky);  // 읽어오기
      console.log(`\n\n Retrieve Data ::  ${ky} : ${value}`);
      reposi = value;
    } catch (error) {
      console.log('value not found');
    }
  }



}





export { SettingModule };

//
// class Person {
//   get ageSeconds() {
//     return Math.floor((Date.now() - this.birthday.getTime()));
//   }
//   get age() {
//     return ageSeconds() / 31557600000;
//   }
// }
//
// Person.schema = PersonSchema;
//
// // Initialize a Realm with Car and Person models
// const realm = new Realm({ schema: [CarSchema, PersonSchema] });
