/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / model / HtPacketGenerator.js
 *
 * Created by Jongwoo Moon on 2017. 1. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import { HtLogicalRandom } from './index';

const packets = require('./packetSampleA.json');

/** Packet Generator for Test */
class HtPacketGenerator {
  constructor(props) {
    // super(props);
    this.p = props; // constants 이하는 변수들...
    this.idx = 0;
  }

  genNewPacket() {
    const rObj = packets.packet[this.idx++];
    if (rObj === undefined) {
      this.idx = 0;
      return packets.packet[this.idx++];
    }
    // console.log(`HtPacketGenerator:: genNewPacket   ${rObj.sen1}`);
    return rObj;
  }

  setGenerators() {
    this.sen1o = new HtLogicalRandom({
      max: 100, min: 0, variationLimit: 20,
      countPosition: 'MAX', finishingCount: 1, // count and finish condition
      doNotPassMax: true, doNotPassMin: false,
      tolerance: 5,
    });
    this.sen2o = new HtLogicalRandom({
      max: 100, min: 0, variationLimit: 20,
      countPosition: 'MAX', finishingCount: 1, // count and finish condition
      doNotPassMax: true, doNotPassMin: false,
      tolerance: 5,
    });
  }
}

export { HtPacketGenerator };


/* Readme:Packet:ETT:Sample

#define		START_CODE			'S'
#define		END_CODE			'E'

예제) START_CODE,buf[0],buf[1],,,,buf[9],END_CODE

기도1 홀센서 전압(mV) 	= atof(buf[0]);
기도2 홀센서 전압(mV) 	= atof(buf[1]);
기도3 홀센서 전압(mV)	= atof(buf[2]);
식도 홀센서 전압(mV)	= atof(buf[3]);
목각도 가변저항 전압(mV)= atof(buf[4]);
목거리 센서 전압(mV) 	= atof(buf[5]);
복부 거리센서 전압(mV)	= atof(buf[6]);
턱 홀센서 전압(mV)	= atof(buf[7]);
배 홀센서 전압(mV)	= atof(buf[8]);
이빨 면압센서 전압(mV)	= atoi(buf[9]);


  genNewPacketOld() {
    const sen1 = this.sen1o.getNewValue(), sen2 = this.sen2o.getNewValue();
    const rObj = { sen1, sen2, sen3: 4, thrHole: 32, neckAng: 33, neckDist: 34, belly: 12,
      chin: 22, bellyHole: 23, teeth: 12 };
    console.log(rObj);
    return rObj;
  }


*/
