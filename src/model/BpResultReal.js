 /**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / model / BpResultReal.js
 *
 * Created by Jongwoo Moon on 2017. 4. 6..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';

function getBigger(va, vb) {
  if (va > vb) {
    return va;
  }
  return vb;
}

function getCorPercent(standard, diff) {
  return ((standard - diff) / standard) * 100;
}

class BpResultReal {
  getResultScore(state) {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ BpResultReal :: getResultScore  ...\n');
    const { uxOnce, curBpVal, curAnswr, playObj, setting, prsrLogic } = state;
    const { gapExist } = playObj; // 설정 혈압 값.
    const { arrScore, pressStandard, examTimeMin } = setting.real;
    const { high, low, bpm, periodGap } = curAnswr; // 설정 혈압 값.

    // 최종 점수 어레이
    const finScrs = [];

    // 수축기 혈압 산정  [0]
    let dif = Math.abs(curBpVal.high - high), bigger = getBigger(curBpVal.high, high),
      cor = getCorPercent(bigger, dif), scr = Math.round(arrScore[0] * cor * 0.01);
    const maxRslt = { set: curBpVal.high, ans: high, dif, cor, scr };
    // { pressMax: 설정 set, 측정 ans, 오차 dif, 정확도 cor, 점수 scr  }
    finScrs.push(scr);

    // 이완기 혈압 산정  [1]
    dif = Math.abs(curBpVal.low - low); bigger = getBigger(curBpVal.low, low);
    cor = getCorPercent(bigger, dif); scr = Math.round(arrScore[1] * cor * 0.01);
    const minRslt = { set: curBpVal.low, ans: low, dif, cor, scr };
    finScrs.push(scr);

    // 맥박수 산정  [2]
    dif = Math.abs(curBpVal.bpm - bpm); bigger = getBigger(curBpVal.bpm, bpm);
    cor = getCorPercent(bigger, dif); scr = Math.round(arrScore[2] * cor * 0.01);
    const bpmRslt = { set: curBpVal.bpm, ans: bpm, dif, cor, scr };
    finScrs.push(scr);

    const { touched, cuffed, bpDepressFail, maxPres } = uxOnce;
    // 커프 설치 [3]
    scr = cuffed ? arrScore[3] : 0;
    finScrs.push(scr);

    // 상완 촉진 [4]
    scr = touched ? arrScore[4] : 0;
    finScrs.push(scr);

    // 청진 간극 [5]
    scr = (gapExist === periodGap) ? arrScore[5] : 0;
    finScrs.push(scr);

    // 적정 가압 [6]
    const pLimit = curBpVal.high + Number(pressStandard); // num, string
    console.log(`  가압 한계 : ${pLimit}  가압 최대값 : ${maxPres}`);
    scr = (pLimit > maxPres) ? arrScore[6] : 0;
    finScrs.push(scr);

    // 적정 감압 속도 [7]  2 ~ 5 사이면 합격..
    scr = (prsrLogic.dimSpeed > 2 && prsrLogic.dimSpeed < 5) ? arrScore[7] : 0;
    console.log(`\n\n   감압 속도 점수 ${scr}      dimSpeed  ${prsrLogic.dimSpeed} 2, 5 사이 ?...\n\n `);
    finScrs.push(scr);

    // 실습시간 준수 [8]
    const { startedAt, finAt } = playObj;
    const tTime = (finAt.getTime() - startedAt.getTime()) / 1000;
    const tLimit = examTimeMin * 60;
    console.log(`  시간 제한 : ${tLimit},   실습 시간 : ${tTime}`);
    scr = (tTime < tLimit) ? arrScore[8] : 0;
    finScrs.push(scr);

    const ttlScr = _.reduce(finScrs, (sum, n) => { return sum + n; }, 0);
    const newResult = { maxRslt, minRslt, bpmRslt, finScrs, ttlScr };
    console.log(newResult);

    return { ...state, newResult };
  }
}

export default BpResultReal;
