/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneGlobalSetting / GlobalSettingMainView.js
 *
 * Created by Jongwoo Moon on 2017. 4. 23..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import MsTitleBar from '../simulations/MsTitleBar';
import { MsTitleInputUnitView } from '../simulations';
import { HtText } from '../uicompo';
import * as M from '../model';

const mdl = new M.SettingModule();
const arrFlag = [require('../resource/imageCommon/flag_kr.png'),
  require('../resource/imageCommon/flag_en.png')];


class GlobalSettingMainView extends Component {
  constructor(props) {
    super(props);
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ GlobalSettingMainView :: constructor ]]  .... \n');
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ BPOperMain :: unmount ]] ...\n');
    const { langIdx, ettDevNum, bpDevNum } = this.props.uiState;
    const { stdName, stdEmail } = this.props.main;
    mdl.saveObject('system', {
      languageKey: langIdx, ettDevNum, bpDevNum, stdName, stdEmail
    });
  }

  ////////////////////////////////////////////////////   _//////////////////_   언어 선택
  renderLanguageSelectBttns() {
    const { uicommon, langKey, langIdx } = this.props.uiState;
    const { languageSetting } = uicommon[langKey];
    const vwSty = { alignItems: 'stretch', justifyContent: 'center' },
    txtSty = { color: '#115', fontWeight: '400', fontSize: 25, };
    const koActive = (langIdx === 0), enActive = (langIdx === 1);

    return (
      <View style={{ flex: 20 }} >
        <HtText
          mandator={{ flex: 10, rad: 5, text: languageSetting, margin: { }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: txtSty }}
        />
        <View style={{ flex: 10, flexDirection: 'row' }} >
          <View style={{ flex: 10 }} />
          {this.getLanguageFlag(koActive, 0)}
          <View style={{ flex: 5 }} />
          {this.getLanguageFlag(enActive, 1)}
          <View style={{ flex: 10 }} />
        </View>
      </View>
    );
  }

  getLanguageFlag(isActive, idx) {
    const scl = isActive ? 1.2 : 1.0, opacity = isActive ? 1 : 0.5;
    const w = scl * 70, h = scl * 40;
    return (
      <TouchableOpacity
        style={[{ width: w, height: h, margin: 15, marginTop: 20 }]}
        onPress={
          () => { this.props.languageChange(idx); }
        }
      >
        <Image
          style={{ width: w, height: h, opacity }}
          source={arrFlag[idx]}
        />
      </TouchableOpacity>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   기기 번호 세팅..
  renderDeviceNumberInputView() {
    const { uicommon, langKey, ettDevNum, bpDevNum } = this.props.uiState;
    const { ettDevNum: ettTtl, bpDevNum: bpTtl } = uicommon[langKey];
    return (
      <View style={{ flex: 10, flexDirection: 'row' }} >
        {this.getDeviceUnitView(ettTtl, '', `${ettDevNum}`, (txt) => {
          if (!isNaN(Number(txt))) { // 숫자로 파싱 ..
            this.props.setBleDevNum({ ettDevNum: Number(txt) });
          }
        })}
        {this.getDeviceUnitView(bpTtl, '', `${bpDevNum}`, (txt) => {
          if (!isNaN(Number(txt))) { // 숫자로 파싱 ..
            this.props.setBleDevNum({ bpDevNum: Number(txt) });
          }
        })}
      </View>
    );
  }

  getDeviceUnitView(title, unit, value, onChange) {
    const vwSty = { margin: 10, marginVertical: 10 }, flexCont = [5, 3, 1];
    return (
      <MsTitleInputUnitView
      mandator={{
        keyboardType: 'numeric', unit, title, inputFontSize: 25
      }}
      flexContent={flexCont}
      vwSty={vwSty}
      value={value}
      onChange={onChange}
      />
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   학생 이름, 이메일 세팅..
  renderStudentInfoView() {
    const { uicommon, langKey } = this.props.uiState;
    const { stdName, stdEmail } = this.props.main;
    const { name, email } = uicommon[langKey];

    console.log('  ok ??  111');
    return (

      <View style={{ flex: 10, flexDirection: 'row' }} >
        {this.getTextInputView(name, 'default', `${stdName}`, (txt) => {
          this.props.editStdName(txt);
        })}
        {this.getTextInputView(email, 'email-address', `${stdEmail}`, (txt) => {
          this.props.editStdEmail(txt);
        })}
      </View>
    );
  }

  getTextInputView(title, keyboardType, value, onChange) {
    const vwSty = { margin: 10, marginVertical: 10 }, flexCont = [3, 5, 1];
    return (
      <MsTitleInputUnitView
      mandator={{
        keyboardType, unit: '', title, inputFontSize: 25
      }}
      flexContent={flexCont}
      vwSty={vwSty}
      value={value}
      onChange={onChange}
      />
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState;
    const { globalSetting } = uicommon[langKey];

    //return (<View />);
    return (
      <View style={sty.mainContainer}>
        <MsTitleBar
          titleA={globalSetting}
          back={'Back'}
        />

        {this.renderDeviceNumberInputView()}

        {/* -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />

        {this.renderStudentInfoView()}

        {/* -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />

        {this.renderLanguageSelectBttns()}

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}

        <View style={{ flex: 40 }} />

      </View>
    );
  }
}

const sty = StyleSheet.create({
  mainContainer: {
    marginTop: 1, flex: 100
  },
  seperateLine: { height: 2, backgroundColor: '#AAAAAA', marginHorizontal: 20 },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    OperBP: state.OperBP,
    theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(GlobalSettingMainView);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
