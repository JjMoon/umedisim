/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / actions / MainActions.js
 *
 * Created by Jongwoo Moon on 2017. 2. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

const changeMainState = (stt) => {
 return {
   type: 'mainState', payload: stt
 };
};

const setEttMode = (mode) => { return { type: 'setEttMode', mode }; };
const setBpMode = (mode) => { return { type: 'setBpMode', mode }; };

const setPlayPause = (truthy) => {
  return {
    type: 'setPlayPause', payload: truthy
  };
};

// 세팅 뷰 관련.  데이터 입출력
const enterSetting = (payload) => { return { type: 'enterSetting', payload }; };
//const exitSetting = (payload) => { return { type: 'exitSetting', payload }; };
const editCaliSetting = (payload) => { return { type: 'editCaliSetting', payload }; };

// Report | PDF ETC
const setCurReport = (payload) => { return { type: 'setCurReport', payload }; };

// Exam 삭제
const deleteExam = (payload) => { return { type: 'deleteExam', payload }; };

// 체크 아이템 관련..
const clearCheckList = () => { return { type: 'clearCheckList', payload: null }; };

const addCheckList = (item) => { return { type: 'addCheckList', payload: item }; };

const addHistory = (item) => { return { type: 'addHistory', payload: item }; };

const expandChkList = (short) => { return { type: 'expandChkList', payload: short }; };

const checkProcess = (newPack) => { // 삭제 ??
  return {
    type: 'checkProcess', payload: newPack
  };
};

// 패킷 값에 따른 상태 변화 액션들..
const changeNeckAngle = (angleInDegree) => {
  return {
    type: 'neckAngle', payload: angleInDegree
  };
};

const changeChinPush = (chin) => {
  return {
    type: 'chinPush', payload: chin
  };
};

const changeNeckHeight = (hght) => {
  return {
    type: 'neckHeight', payload: hght
  };
};

const checkTeeth = (press) => {
  return {
    type: 'teethPress', payload: press
  };
};

const checkBronchus = (onoff) => {
  return {
    type: 'bronchus', payload: onoff
  };
};

// 복부 팽만(백벨브 마스크) :  AA 6  (삽관 없이 이 값 검출.  백 밸브 마스크로 과다하게 불었을 때)
// 복부 팽만(식도 삽관) : AA 6 (관 검출 AA 4)
const checkBelly = (val) => {
  return {
    type: 'checkBelly', payload: val
  };
};

const languageChange = (idx) => {
  return { type: 'languageChange', payload: idx };
};

const saveEttSetting = (payload) => { return { type: 'saveEttSetting', payload }; };
const setEttCheckItemValues = (payload) => { return { type: 'setEttCheckItemValues', payload }; };
const editManualScoreActive = (payload) => { return { type: 'editManualScoreActive', payload }; };

// BLE Actions ..
const setBleObject = (nativeObj, isIOS) => { return { type: 'setBleObject', isIOS, nativeObj }; };

const connectETT = () => { return { type: 'connectETT' }; };
const connectBP = () => { return { type: 'connectBP' }; };


export {
  deleteExam,
  setEttMode, setBpMode,
  enterSetting, editCaliSetting,
  setCurReport,
  changeMainState, setPlayPause, checkProcess,
  clearCheckList, addCheckList, expandChkList,
  addHistory,
  changeNeckAngle, changeChinPush, changeNeckHeight,
  checkTeeth, checkBronchus, checkBelly,
  languageChange,
  saveEttSetting, setEttCheckItemValues, editManualScoreActive,
  setBleObject, connectETT, connectBP
};
