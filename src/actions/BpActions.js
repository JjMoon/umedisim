/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / actions / BpActions.js
 *
 * Created by Jongwoo Moon on 2017. 2. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

// 세팅 뷰 관련.  데이터 입출력
const setBpSetting = (payload) => { return { type: 'setBpSetting', payload }; };

//const setBpUxInit = () => { return { type: 'setBpUxInit' }; };

const BpPacket = (payload) => { return { type: 'BpPacket', payload }; };

const bpPlayOrStop = (start) => { return { type: 'bpPlayOrStop', start }; };
const bpPlayTick = () => { return { type: 'bpPlayTick' }; };

const bpFinishExam = () => { return { type: 'bpFinishExam' }; }; // 실습 끝내는 액션

// Test
const bpStartTest = () => { return { type: 'bpStartTest' }; }; // 실습 끝내는 액션

// Operate Related.
const propagateBP = (payload) => { return { type: 'propagateBP', payload }; };
const setBpValues = (payload) => { return { type: 'setBpValues', payload }; };
const setBpAnswr = (payload) => { return { type: 'setBpAnswr', payload }; };



export {
  setBpSetting,
  BpPacket,
  bpPlayOrStop, bpPlayTick,
  bpFinishExam,
  bpStartTest,
  propagateBP, setBpValues, setBpAnswr
};
