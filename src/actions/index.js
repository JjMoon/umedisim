/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / actions / index.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

export * from './MainActions';
export * from './UiActions';
export * from './BpActions';

export const addCounter = step =>
  // console.log('Action :: addCounter');
  ({
    type: 'increaseCounter',
    payload: step,
  })
;

// BLE 관련 액션..
export const setBleCompo = payload => ({ type: 'setBleCompo', payload });
export const connected = payload => ({ type: 'connected', payload });

export const startScan = (iIOS, iETT, ntvObj, dN) => ({ type: 'startScan', iIOS, iETT, ntvObj, dN });

export const setBleState = stt => ({ type: 'setBleState', stt });

export const addBlePeripheral = bpName => ({
  type: 'newBlePeripheral',
  payload: bpName,
});

export const setBleDevNum = payload => ({ type: 'setBleDevNum', payload });


export const appStarted = () => ({
  type: 'init',
  payload: null,
});

export const newBlePacket = pck => // 패킷 들어올 때 ....
  ({
    type: 'newBlePacket',
    payload: pck,
  })
;


export const packetExam = pck => // 리플레이 관련 패킷
  ({
    type: 'packetExam',
    payload: pck,
  })
;

export const examListViewMode = enter => // exam list view 로 들어갈 때 리셋.
  ({
    type: 'examListViewMode',
    payload: enter,
  })
;

export const examReplay = exm => // 리플레이 시작..
  ({
    type: 'examReplay',
    payload: exm,
  })
;

export const examReplayStop = () => // 리플레이 Stop
  ({ type: 'examReplayStop' })
;

export const examReplayNext = () => // 리플레이 리스트의 인덱스
  ({
    type: 'examReplayNext',
  })
;

export const replayTick = () => // 리플레이 타이머 틱.. 0.1초 간격.
  ({ type: 'replayTick' })
;
