/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / actions / UiActions.js
 *
 * Created by Jongwoo Moon on 2017. 2. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

const setViewLayout = (stt) => { // Swap 뷰 ..
 return {
   type: 'viewLayoutKind', payload: stt
 };
};

const startStopExamin = (start) => { return { type: 'startStopExamin', payload: start }; };
const resetEttExam = () => { return { type: 'resetEttExam' }; }; // play 관련. Oper 뷰 들어가면 리셋.

const resetCurExam = () => { return { type: 'resetCurExam' }; }; // replay 관련.

// 공통
const editStdName = (payload) => { return { type: 'editStdName', payload }; };
const editStdEmail = (payload) => { return { type: 'editStdEmail', payload }; };

// Ett Setting ...
const editAutoScoreActive = (payload) => { return { type: 'editAutoScoreActive', payload }; };
const editEttManualItem = (payload) => { return { type: 'editEttManualItem', payload }; };
const propagateSetting = (payload, ettLogic) => { return { type: 'propagateSetting', payload, ettLogic }; };
const makeBrthHistory = (payload) => { return { type: 'makeBrthHistory', payload }; };

export { setViewLayout, startStopExamin, resetEttExam, resetCurExam,
  editStdName, editStdEmail,
  editAutoScoreActive, editEttManualItem, propagateSetting, makeBrthHistory
};
