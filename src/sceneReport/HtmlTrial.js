

import React, { Component } from 'react';


class HtmlTrial extends Component {
  render() {
    return (
      <div className="container">Hello {this.props.name}</div>
    );
  }

}

React.render(<HtmlTrial />, document.getElementById('container'));
