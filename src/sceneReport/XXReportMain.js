/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / mainView.js
 *
 * Created by Jongwoo Moon on 2017. 2. 14..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage, WebView } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtText, HtButton, HtColor } from '../uicompo';

//import HtmlTrial from './HtmlTrial';

/*
    로컬 저장소에서 읽고, 나갈 때 쓰자.
    데이터 객체가 리듀서에 있어야 함.
    enterSetting
    getoutofSetting
*/

class ReportMain extends Component {
  constructor(props) {
    super(props);

    this.readCaliValues();
  }

  componentWillMount() {
    this.props.changeMainState('init');
  }

  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ SceneMain :: unmount ]] ...\n');
  }

  async readCaliValues() {
    try {
      const value = await AsyncStorage.getItem('calisetting');  // 읽어오기
      const caliObj = JSON.parse(value);
      this.props.enterSetting({ calisetting: caliObj });
    } catch (error) {
      console.log('value not found');
    }
  }

  // source={{ uri: 'https://github.com/facebook/react-native' }}

    // renderWebview() {
    //   const webapp = require('./Report.html');
    //
    //   return (
    //     <WebView
    //       source={webapp}
    //       style={{ flex: 1, marginTop: 20 }}
    //     />
    //   );
    // }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState;
    const { setting } = uicommon[langKey];

    return (
      <View style={{ marginTop: 1, flex: 100, backgroundColor: '#ABC' }} >
        {this.renderWebview()}
      </View>
    );
  }
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { backgroundColor: HtColor.blueGray, margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    chkItems: state.chkItems,
    theCounter: state.timerCount,
    uiState: state.uiState
    //arrPacket: state.arrPacket,
  };
};

export default connect(mapStateToProps, actions)(ReportMain);
