/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneReport / MainReport.js
 *
 * Created by Jongwoo Moon on 2017. 4. 17..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import { captureRef, captureScreen } from 'react-native-view-shot';

import * as actions from '../actions';
import * as M from '../model';
import { HtText, HtColor } from '../uicompo';
import { HsControlBar } from '../simulations';
import MsTitleBar from '../simulations/MsTitleBar';
import HsStudentExamVw from '../simulations/HsStudentExamVw';
import GraphBrth from '../sceneOper/GraphBrth';

/**
 * Class
 */
class MainReport extends Component {
  constructor(props) {
    super(props);

    const gen = this.props.EttSetting.general;
    this.general = {};
    for (let i = 0; i < gen.length; i++) {
      const obj = gen[i]; // console.log(obj);
      this.general = { ...this.general, [obj.key]: obj };
    }

    console.log(this.props.arrPacket);

    this.state = {
      error: null, res: null,
      value: {
        format: 'png', quality: 0.9, result: 'file',
        snapshotContentContainer: false,
      },
      report: { overall: null, graph: null },
    };
  }

  // //////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
  }

  componentWillUnmount() {
    this.props.resetEttExam();
  }

  onSavePress() {
    console.log('\n\n\n\n\n  Main Report    onSavePress  데이터 저장..  ===== =====');
    this.snapshot('content').call();


    captureScreen({
      format: 'jpg',
      quality: 0.8,
    })
      .then(
        uri => console.log('Image saved to', uri),
        error => console.error('Oops, snapshot failed', error),
      );
    this.graphTimer = setInterval(this.graphScreenShot.bind(this), 500);
  }

  graphScreenShot() {
    console.log('   tick function ');
    this.snapshot('graph').call();
    clearInterval(this.graphTimer);
  }

  saveResultDB(graphImg) {
    const { report } = this.state;
    const { uxhistory, main, arrPacket } = this.props;
    const arrImgJson = `[ "${report.overall}", "${graphImg}" ]`;
    const restData = { mode: main.ettMode, arrImgJson,
      stdntInfo: main.stdName, email: main.stdEmail, totalScore: Number(this.scoreMarked) };
    console.log(`  ${main.stdEmail}  ${main.stdEmail}  점수획득 : ${this.scoreMarked}`);

    M.RmEttResult.newEttResult(uxhistory.history, arrPacket, restData);
  }

  snapshot = refname => () =>
    // takeSnapshot(this.refs[refname], this.state.value)
    captureRef(this.refs[refname], this.state.value)
      .then(res =>
        (this.state.value.result !== 'file' ?
          res :
          new Promise((success, failure) =>
            // just a test to ensure res can be used in Image.getSize
            Image.getSize(
              res,
              (width, height) => (
                console.log(res, width, height),
                success(res)
              ),
              failure),
          )),
      )
      .then((res) => {
        const { report } = this.state;
        console.log('\n\n\n\n\n      takeSnapshot   then ....');
        console.log(res);
        this.setState({
          error: null,
          res,
          previewSource: {
            uri: this.state.value.result === 'base64' ?
              `data:image/${this.state.value.format};base64,${res}` : res,
          },
        });
        // report: { overall: null, graph: null }
        if (report.overall === null) {
          this.setState({
            report: { ...report, overall: res },
          });
        } else {
          this.saveResultDB(res);
          Actions.popTo('sceneMain');
        }
      })
      .catch(error => (console.warn(error), this.setState({ error, res: null, previewSource: null })));

  renderColumnUnit(ttl, value, flex) {
    const { unit } = this.props.main.scr;
    const valueSty = { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 3 * unit };
    return (
      <View
        style={[sty.unitContainer, { flex }]}
      >
        <HtText
          mandator={{ flex: 10, rad: 5, text: ttl, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: sty.titleText }}
        />
        <HtText
          mandator={{ flex: 8, rad: 5, text: value, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: valueSty }}
        />
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   하단 큰 박스.
  renderBreathInfo(ttl, main, highV, normV, lowV) {
    const { uicommon, langKey } = this.props.uiState;
    const { high, normal, low } = uicommon[langKey];
    const { unit } = this.props.main.scr;
    const mainTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 3.5 * unit,
      textAlign: 'right' };
    const smlTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 1.7 * unit,
      textAlign: 'right', paddingRight: 8 };

    return (
      <View style={[sty.unitContainer, { flex: 1 }]}>
        <HtText
          mandator={{ flex: 1, rad: 5, text: ttl, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: sty.titleText }}
        />
        <View style={{ flex: 3, flexDirection: 'row' }}>
          <HtText
            mandator={{ flex: 13, rad: 5, text: main, margin: { }, radIgnore: true }}
            style={{ viewStyle: sty.vw, textStyle: mainTxt }}
          />
          <View style={{ flex: 10 }}>
            <View style={{ flex: 3 }} />
            <HtText
              mandator={{ flex: 5, rad: 5, text: `${highV} : ${high}`, margin: { }, radIgnore: true }}
              style={{ viewStyle: sty.vw, textStyle: smlTxt }}
            />
            <HtText
              mandator={{ flex: 5, rad: 5, text: `${normV} : ${normal}`, margin: { }, radIgnore: true }}
              style={{ viewStyle: sty.vw, textStyle: smlTxt }}
            />
            <HtText
              mandator={{ flex: 5, rad: 5, text: `${lowV} : ${low}`, margin: { }, radIgnore: true }}
              style={{ viewStyle: sty.vw, textStyle: smlTxt }}
            />
            <View style={{ flex: 3 }} />
          </View>
        </View>
      </View>
    );
  }

  renderGraph() {
    // console.log(this.state.previewSource);  { uri: '/sdldl/dldl.png' }
    const { scr, startedAt, endedAt } = this.props.main;

    console.log(`  startedAt :: ${startedAt}   ${new Date()}`);

    return (
      <View
        style={{ flex: 70, justifyContent: 'center', backgroundColor: '#FFF' }}
        ref="graph"
      >

        <GraphBrth
          parent={'MainReport'}
          time={{ s: startedAt, e: endedAt }}
          size={{ w: scr.width * 0.9, h: scr.height * 0.5 }}
        />

      </View>
    );
  }

  renderContent() {
    const { breathNum } = this.general;
    const { unit } = this.props.main.scr;
    const { history } = this.props.uxhistory;
    const { uicommon, langKey } = this.props.uiState;
    const { brthCnt, avrBrthVol, avrExhale, vpm, extBrthVol, extBrthExhale,
      extBrthPeriod, chkRightBron, chkBellyMask, chkBellyStom, totalScore,
    } = uicommon[langKey];
    const scoreTxtSty = { color: HtColor.bgDarkGray, paddingRight: 20,
      fontWeight: '500', fontSize: 3.5 * unit, textAlign: 'right' };

    const { count, avrVol, avrExhale: avrEx, respPerMin,
      passNum, overNum, belowNum, exPass, exOver, exBelow,
      prdPass, prdOver, prdBelow,
    } = this.props.uxhistory.brthInfo;
    const volumnExactPercent = (count === 0) ? 0 : _.round((passNum * (100 / count)), 1);
    const exhaleExactPercent = (count === 0) ? 0 : _.round((exPass * (100 / count)), 1);
    const periodExactPercent = (count === 0) ? 0 : _.round((prdPass * (100 / count)), 1);
    const ttlScore = _.chain(history).map(o => (o.auto ? o.score : o.scoreMarked)) // 획득한 총점 구하기...
      .reduce((sum, n) => sum + n, 0);

    this.scoreMarked = ttlScore;

    return (
      <View style={{ flex: 70, backgroundColor: '#FFF' }} ref="content">

        {/* -------------------------  -------------------------  학생 정보 */}
        <View style={[sty.reportContainer, { flex: 10 }]} >
          <HsStudentExamVw modeCase={this.props.main.ettMode} />
        </View>

        {/* -------------------------  -------------------------  작은 박스  */}
        <View style={{ flex: 20, flexDirection: 'row' }} >
          {/* -------------------------  -------------------------  Check 박스  */}
          <View style={{ flex: 1, flexDirection: 'row' }} >
            {this.renderColumnUnit(chkRightBron, 'PASS', 2)}
            {this.renderColumnUnit(chkBellyMask, 'NO', 2)}
            {this.renderColumnUnit(chkBellyStom, 'PASS', 2)}
          </View>
          {/* -------------------------  -------------------------  중간 4개 박스  */}
          <View style={{ flex: 2, flexDirection: 'row' }} >
            {this.renderColumnUnit(brthCnt, `${count} / ${breathNum.value}`, 3)}
            {this.renderColumnUnit(avrBrthVol, `${_.round(avrVol, 0)} ml`, 3)}
            {this.renderColumnUnit(avrExhale, `${_.round(avrEx, 1)} sec`, 3)}
            {this.renderColumnUnit(vpm, `${_.round(respPerMin, 1)} vpm`, 3)}
          </View>
        </View>

        {/* -------------------------  -------------------------  호흡 관련 하단 박스 3   */}
        <View style={{ flex: 30, flexDirection: 'row' }} >
          {this.renderBreathInfo(extBrthVol, `${volumnExactPercent} %`, overNum, passNum, belowNum)}
          {this.renderBreathInfo(extBrthExhale, `${exhaleExactPercent} %`, exOver, exPass, exBelow)}
          {this.renderBreathInfo(extBrthPeriod, `${periodExactPercent} %`, prdOver, prdPass, prdBelow)}
        </View>

        {/* -------------------------  -------------------------  총점 표현 부분..   */}
        <View style={{ flex: 10 }} >
          <HtText
            mandator={{ flex: 10, rad: 5, margin: { }, radIgnore: true,
              text: `${totalScore} : ${ttlScore} / ${this.getTotalScore()}` }}
            style={{ viewStyle: sty.vw, textStyle: scoreTxtSty }}
          />
        </View>
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { report } = this.state;
    const { uicommon, langKey } = this.props.uiState;
    const { resultTitle } = uicommon[langKey];

    let mainView = this.renderGraph();
    if (report.overall === null) {
      mainView = this.renderContent();
    }

    return (
      <View style={{ marginTop: 1, flex: 100 }}>
        <MsTitleBar
          swapBttnOn={false}
          titleA={resultTitle}
          back={'Back'}
          backCallback={() => Actions.popTo('sceneMain')}
          flex={10}
        />

        {mainView}

        <View style={{ flex: 8, backgroundColor: '#22F' }} >
          <HsControlBar
            flex={10}
            scrH={this.props.main.scr.height}
            onSavePress={this.onSavePress.bind(this)}
          />
        </View>
      </View>
    );
  }

  getTotalScore() {
    const { autoCheckItem, manCheckItem } = this.props.EttSetting;
    const autoArray = _.map(autoCheckItem, o => o);
    const theList = _.concat(autoArray, manCheckItem);
    const ttlScore = _.chain(theList)
      .filter(o => o.active)
      .map(o => o.score)
      .reduce((sum, n) => sum + n, 0);
    return ttlScore;
  }
}

const borderColor = '#AAA';

const sty = StyleSheet.create({
  unitContainer: {
    alignItems: 'stretch', justifyContent: 'center',
    borderWidth: 1,
    margin: 5,
    borderColor, // : 'gainsboro', //(#dcdcdc)
    borderRadius: 3,
  },
  vw: {
    alignItems: 'stretch', justifyContent: 'center',
  },
  reportContainer: {
    alignItems: 'stretch', marginVertical: 20, marginHorizontal: 10,
    borderBottomWidth: 1, borderTopWidth: 1,
    borderColor,
    justifyContent: 'center',
  },
  titleText: {
    color: '#115', fontWeight: '400', fontSize: 18,
  },
  seperateLine: {
    height: 2, backgroundColor: '#AAAAAA',
  },
  grayBorder: {
    margin: 2,
    borderWidth: 1,
    borderColor: '#888', // (#dcdcdc)
  },
});

const mapStateToProps = state => ({
  main: state.main,
  breath: state.breath,
  uxhistory: state.uxhistory,
  theCounter: state.timerCount,
  uiState: state.uiState,
  EttSetting: state.EttSetting,
  arrPacket: state.arrPacket,
});

export default connect(mapStateToProps, actions)(MainReport);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
