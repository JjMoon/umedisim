/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / simulations / HistoryList.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import React, { Component } from 'react';
import { StyleSheet, ListView, View, Text } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { HtColor } from '../uicompo';

const signs = require('./HistorySigns.json');

/** History List  */
class HistoryList extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    this.time = new Date();
    setTimeout(() => {
      console.log('set state....  ');

      this.setState({ dataSource: ds.cloneWithRows(this.props.uxhistory.history) });

      // this.state = {
      //   dataSource: ds.cloneWithRows(this.props.uxhistory.history), // main.actions),
      // };
    }, 0);
  }

  getDataSource(datum) {
    return this.state.dataSource.cloneWithRows(datum);
  }

  renderRow(dt) {
    // { "shr": "기록 명령", "long": "수행한 술기를 의말하는가?", "score": 10, "auto": false }
    // return { ...chks[idx], pass: value, scoreMarked: chks[idx].score, ts: new Date() };
    let bgColor = dt.auto ? '#EEEAF5' : '#E0F5DA'; // 오토|매뉴얼에 따라서 다른 색.
    bgColor = dt.pass ? bgColor : '#A25555'; // firebrick B22222 보다 엷게..  에러는 빨간색 배경
    const txtColor = dt.pass ? { color: HtColor.txtDarkGray } : { color: 'white' }; // 글자색..
    const titleText = dt.pass ? dt.shr : `${dt.shr} * CHK *`; // 에러일 때 글자 추가..

    const tt = _.round((dt.ts - this.props.main.startedAt) * 0.001, 1);

    // console.log(`  renderRow ::  ${tt}  ${titleText} `);

    const scoreSection = dt.pass ?
      (<Text style={[sty.score, txtColor]}>점수 : {dt.score}</Text>) :
      null;

    return (
      <View style={[sty.cellContainer, { backgroundColor: bgColor }]}>
        <Text style={[sty.title, txtColor]}>{titleText}</Text>
        <View style={sty.rowcontainer}>
          {scoreSection}
          <Text style={[sty.normal, txtColor]}>{tt} sec</Text>
        </View>
      </View>
    );
  }

  renderRowOld(dt) {
    // { "shr": "기록 명령", "long": "수행한 술기를 의말하는가?", "score": 10, "auto": false }
    // return { ...chks[idx], pass: value, scoreMarked: chks[idx].score, ts: new Date() };
    let bgColor = dt.auto ? '#EEEAF5' : '#E0F5DA'; // 오토|매뉴얼에 따라서 다른 색.
    bgColor = dt.pass ? bgColor : '#A25555'; // firebrick B22222 보다 엷게..  에러는 빨간색 배경
    const txtColor = dt.pass ? { color: HtColor.txtDarkGray } : { color: 'white' }; // 글자색..
    const titleText = dt.pass ? dt.shr : `${dt.shr} * CHK *`; // 에러일 때 글자 추가..

    const tt = _.round((dt.ts - this.props.main.startedAt) * 0.001, 1);

    const scoreSection = dt.pass ?
      (<Text style={[sty.score, txtColor]}>점수 : {dt.scoreMarked}</Text>) :
      null;

    return (
      <View style={[sty.cellContainer, { backgroundColor: bgColor }]}>
        <Text style={[sty.title, txtColor]}>{titleText}</Text>
        <View style={sty.rowcontainer}>
          {scoreSection}
          <Text style={[sty.normal, txtColor]}>{tt} sec</Text>
        </View>
      </View>
    );
  }

  render() {
    this.time = new Date();

    console.log('  history list   ', this.state);

    if (_.isNil(this.state)) return null;
    // enableEmptySections
    return (
      <ListView
        style={sty.container}
        enableEmptySections
        dataSource={this.getDataSource(this.props.uxhistory.history)}
        renderRow={this.renderRow.bind(this)}
      />
    );
  }
}

// /-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    backgroundColor: '#FAFFFF',
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  cellContainer: {
    flex: 1,
    borderRadius: 20,
    margin: 3,
  },
  rowcontainer: {
    flex: 5,
    flexDirection: 'row',
  },
  title: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: '900',
    color: HtColor.txtDarkGray,
    margin: 5,
  },
  score: {
    flex: 5,
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '400',
    color: HtColor.txtScoreRed,
    margin: 2,
  },
  normal: {
    flex: 4,
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '100',
    color: HtColor.txtDarkGray,
    margin: 1,
  },
});

const mapStateToProps = state => ({
  main: state.main,
  uxhistory: state.uxhistory,
});

export default connect(mapStateToProps)(HistoryList);
