/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / simulations / BleManView.js
 *
 * Created by Jongwoo Moon on 2017. 4. 24..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Platform, TouchableOpacity, Image,
  DeviceEventEmitter, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtText, HtColor } from '../uicompo';
import * as M from '../model';

// iOS BLE Connection
const BleManIOS = NativeModules.BleConLnkcn;
const BleLinkconModuleAndroid = NativeModules.BleLinkconModuleAndroid;

class BleManView extends Component {
  constructor(props) {
    super(props);
    this.state = { cnt: 0, fetchOn: true };
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    console.log('\n ====== ====== ====== ======  [[ BleManView :: componentDidMount ]]');
    if (Platform.OS === 'ios') {
      BleManIOS.initBle();
    } else {
      BleLinkconModuleAndroid.initialJob();
    }
  }

  componentWillMount() {
    console.log('\n ====== ====== ====== ======  [[ BleManView :: componentWillMount ]]');
    //this.props.changeMainState('init');

    DeviceEventEmitter.addListener('received', this.receivedPacket.bind(this));

    DeviceEventEmitter.addListener('receivedBP', this.receivedBpPacket.bind(this));
  }

  ////////////////////////////////////////////////////   _//////////////////_   BP 패킷 송신
  //  0x02   0xAA   0xB1, 0xB2  0xC1, 0xC2    0xDD  0xEE    0x03
  //  AA : 맥박수 설정 값.
  sendBpPacket(pulse, bpMax, bpMin, d, e) {
    console.log(`\n\n  sendBpPacket  :::   ${pulse}`);
    const b1 = _.round((bpMax / 250) + 5, 0), b2 = (bpMax % 250) + 5;
    const c1 = _.round((bpMin / 250) + 5, 0), c2 = (bpMin % 250) + 5;

    const { pulseIntense, soundIntense } = d;
    const dd = (pulseIntense << 6) + (soundIntense << 3)

    console.log(`  pulseIntense : ${pulseIntense},   soundIntense : ${soundIntense}`);

    const { operate, speaker, isTouch, gab } = e; // 4 값 모두 boolean
    const operateV = operate ? (1 << 6) : 0;
    const speakerV = speaker ? (1 << 5) : 0;
    const methodV = isTouch ? (1 << 4) : 0;
    const gabV = gab ? (1 << 3) : 0;

    const ee = (1 << 7) + operateV + speakerV + methodV + gabV;
    const pack = [0x02, pulse, b1, b2, c1, c2, dd, ee, 0x03];

    console.log(pack);

    if (Platform.OS === 'ios') {
      BleManIOS.sendBpPacket(pack);
    } else {
      BleLinkconModuleAndroid.sendBpPacket(pack);
    }
  }

  ////////////////////////////////////////////////////   _//////////////////_   패킷 수신 이벤트 처리
  receivedBpPacket(numArr) { // from Android
    console.log(` received from listener >>>>>   ${numArr}`); // 203,5,5,5,5,5,3

    const packObj = M.parseBp(numArr);
    this.props.BpPacket(packObj);
    //this.monitorState(packObj);
    //console.log(` *** Packet >> >> ${numArr} ${packObj.thr}  ${packObj.blly} `);
    this.setState({ cnt: this.state.cnt + 1 });
  }

  receivedPacket(numArr) { // from Android
    console.log(` received from listener >>>>>   ${numArr}`); // 203,5,5,5,5,5,3

    const packObj = M.parse(numArr);
    this.props.newBlePacket(packObj);
    //this.monitorState(packObj);
    console.log(` *** Packet >> >> ${numArr} ${packObj.thr}  ${packObj.blly} `);
  }


  ////////////////////////////////////////////////////   _//////////////////_   UX 이벤트 처리..
  cutConnection() {
    console.log(' connectETT ');
    //const { ettDevNum } = this.props.uiState;
    if (Platform.OS === 'ios') {
      BleManIOS.finishConnection();
    } else {
      BleLinkconModuleAndroid.finishConnection();
    }

    this.props.setBleState('None');
    this.setState({ fetchOn: false });
  }

  connectETT() {
    console.log(' connectETT ');
    const { ettDevNum } = this.props.uiState;
    if (Platform.OS === 'ios') {
      console.log('ios');
      this.props.startScan(true, true, BleManIOS, ettDevNum); // isIOS, iETT, ntvObj
      this.interval = setInterval(this.connectCheckIOS.bind(this), 1000);
    } else {
      console.log('  Its Android case    ');
      if (BleLinkconModuleAndroid !== undefined) {
        this.props.startScan(false, true, null, ettDevNum); // isIOS, iETT, ntvObj
        BleLinkconModuleAndroid.connect('ETT', ettDevNum,
          (errMsg) => {
            console.log(errMsg);
          },
          () => {
              console.log('  연결 완료');
              this.props.connected({ connETT: true });
              this.props.setBleState('Connected');
                // Timer 재설정....
              clearInterval(this.interval);
          });
      }
    }
  }

  connectBP() {
    console.log(' connectBP ');
    const { bpDevNum } = this.props.uiState;
    if (Platform.OS === 'ios') {
      this.props.startScan(true, false, BleManIOS, bpDevNum); // isIOS, iETT, ntvObj
      this.interval = setInterval(this.connectCheckIOS.bind(this), 1000);
    } else {
      console.log(' connectBP  Its Android case    ');
      if (BleLinkconModuleAndroid !== undefined) {
        this.props.startScan(false, false, null, bpDevNum); // isIOS, iETT, ntvObj
        BleLinkconModuleAndroid.connect('BP', bpDevNum,
          (errMsg) => {
            console.log(errMsg);
          },
          () => {
              console.log('  연결 완료');
              this.props.connected({ connBP: true });
              this.props.setBleState('Connected');

                // Timer 재설정....
              clearInterval(this.interval);
          });
      }
    }
  }

  ////////////////////////////////////////////////////   _//////////////////_   Android 로직..

  ////////////////////////////////////////////////////   _//////////////////_   iOS 로직..
  connectCheckIOS() { // ETT, BP 공통..
    const { cnt } = this.state;
    console.log(`\n\n  connectCheck  ${cnt}  \n\n`);
    this.setState({ cnt: cnt + 1 });

    if (cnt > 8) {
      clearInterval(this.interval);
      BleManIOS.stopScan();
      this.setState({ cnt: 0 });
      this.props.setBleState('None');
    }
    BleManIOS.connectCheck(() => {
      // 연결 성공하면...
      this.props.setBleState('Connected');
      this.setState({ fetchOn: true });

      // Timer 재설정....
      clearInterval(this.interval);

      // this.interval = setInterval(this.fetch.bind(this), 250);  타이머 대신
      this.fetch(); // 재귀 함수로..
    });
  }

  fetch() { // ETT, BP 공통..
    //console.log('\n\n  fetch from JS  \n\n  ');
    const { isETT, fetchOn } = this.state;
    BleManIOS.fetchPacket((error, numArr) => { // 콜백을 저장했다가 부르는 형태...
      if (error) {
        console.error(error);
      } else {
        //BleMan.log(' Received Javascript... ', 55);
        if (isETT) {
          if (numArr.length !== 8 || numArr[0] !== 2) { // 에러 확인..
            return;
          }
          const ettPacket = M.parse(numArr);
          this.props.newBlePacket(ettPacket);
        } else {
          const bpPack = M.parseBp(numArr);
          this.props.BpPacket(bpPack);
        }
        if (fetchOn) {
          this.fetch();
        }
      }
    });
  }



  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    return (
      <View style={[sty.container, { flex: 9 }]}>
        <View style={[sty.rowContainer, { justifyContent: 'flex-end' }]}>

          {this.renderBleInfo()}

          {this.renderMedisimConnectButtons()}

        </View>
      </View>
    );
  }

  renderMedisimConnectButtons() {
    const { conState } = this.props.bleRdcr;
    const { height } = this.props.main.scr;
    const spaceRight = 20, medisimScale = height / 1700,
      w = 206 * medisimScale, h = 130 * medisimScale;

    if (conState !== 'None') {
      return (
        <TouchableOpacity
          style={[{ width: w, height: h, margin: 15, marginTop: 20 }]}
          onPress={this.cutConnection.bind(this)}
        >
          <Image
            style={{ width: h, height: h, marginRight: spaceRight }}
            source={require('../resource/imageCommon/ble_icon.png')}
          />
        </TouchableOpacity>
      );
    }

    return (
      <View style={[sty.rowContainer, { justifyContent: 'flex-end' }]}>
        <TouchableOpacity
          style={[{ width: w, height: h, margin: 15, marginTop: 20 }]}
          onPress={this.connectBP.bind(this)}
        >
          <Image
            style={{ width: w, height: h, marginRight: spaceRight }}
            source={require('../resource/imageCommon/medisim_bp.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[{ width: w, height: h, margin: 15, marginTop: 20 }]}
          onPress={this.connectETT.bind(this)}
        >
          <Image
            style={{ width: w, height: h, marginRight: spaceRight }}
            source={require('../resource/imageCommon/medisim_ett.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   renderBleInfo
  renderBleInfo() {
    const valVw = { backgroundColor: 'white', borderWidth: 3, borderColor: '#9A9', opacity: 0 }; // 투명도로 테스트 숨기기
    const valTxt = { color: '#353', fontWeight: '500', fontSize: 14 };

    // save 버튼 크기 111 87
    //const scl = 0.001 * scrH, saveX = 111 * scl, saveY = 87 * scl;
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>

        <HtText
          mandator={{ flex: 5, rad: 10, text: this.props.bleRdcr.conState, margin: { all: 10 } }}
          style={{ viewStyle: valVw, textStyle: valTxt }}
        />

        <View style={{ flex: 10 }} />
      </View>
    );
  }
}

const sty = StyleSheet.create({
  rowContainer: { flexDirection: 'row', alignSelf: 'stretch' },
  title: {
    fontSize: 30,
    fontWeight: '600',
    textAlign: 'left',
    margin: 20,
    marginTop: 35,
    alignSelf: 'stretch'
  },
});


const mapStateToProps = (state) => {
  return {
    main: state.main,
    bleRdcr: state.bleRdcr,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions, null, { withRef: true })(BleManView);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
