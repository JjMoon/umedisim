/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / simulations / HsStudentExamVw.js
 *
 * Created by Jongwoo Moon on 2017. 4. 19..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtInput, HtColor, HtText } from '../uicompo';

const dateFormat = require('dateformat');

class HsStudentExamVw extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ HsStudentExamVw :: constructor ]] ');
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex = 1, modeCase } = this.props;
    const { uicommon, langKey } = this.props.uiState;
    const { name, email, mode, examTime } = uicommon[langKey];
    const { stdName, stdEmail, startedAt: sat } = this.props.main;
    const time = dateFormat(sat);

    return (
      <View style={{ flex, flexDirection: 'row' }} >
        {this.renderNameInput(name, stdName, 5, 12, (text) => this.props.editStdName(text))}
        {this.renderNameInput(email, stdEmail, 8, 15, (text) => this.props.editStdEmail(text))}
        {this.renderResultUnit(mode, modeCase, 3, 10)}
        {this.renderResultUnit(examTime, time, 8, 25)}
      </View>
    );
  }

  renderNameInput(ttl, value, flex, flexValue, txtChangeCB) {
    const { unit } = this.props.main.scr;
    const mainTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 2 * unit };
    const fontSize = 2.3 * unit, tb = 1.5 * unit;
    const vwSty = { alignItems: 'stretch', justifyContent: 'center', paddingVertical: 10 };

    // onPressCallback={this._bttnActOtherView.bind(this)}
    return (
      <View
        style={{ // backgroundColor: '#99E',
          flexDirection: 'row', flex, margin: 3,
          //borderWidth: 3, borderColor: '#CCC', borderRightWidth: 0, borderLeftWidth: 0
        }}
      >
        <HtText
          mandator={{ flex: 5, rad: 5, text: `${ttl} : `, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: mainTxt }}
        />
        <HtInput
          mandator={{
            flex: flexValue, rad: 4, value, margin: { tb },
            onChange: txtChangeCB
          }}
          multiline={false}
          style={{ input: { fontSize }, border: sty.grayBorder }}
        />
      </View>
    );
  }

  renderResultUnit(ttl, value, flex, flexValue) {
    const { unit } = this.props.main.scr;
    const mainTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 2 * unit };
    const valueSty = { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 2.5 * unit };

    return (
      <View
        style={{ // backgroundColor: '#CCF',
          flexDirection: 'row', flex, margin: 3,
        }}
      >
        <HtText
          mandator={{ flex: 10, rad: 5, text: `${ttl} : `, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: mainTxt }}
        />
        <HtText
          mandator={{ flex: flexValue, rad: 5, text: value, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: valueSty }}
        />
      </View>
    );
  }
}
const borderColor = '#AAA';

const sty = StyleSheet.create({
  unitContainer: {
    alignItems: 'stretch', justifyContent: 'center',
    borderWidth: 1,
    margin: 5,
    borderColor, //: 'gainsboro', //(#dcdcdc)
    borderRadius: 3
  },
  vw: {
    alignItems: 'stretch', justifyContent: 'center'
  },
  reportContainer: {
    alignItems: 'stretch', marginVertical: 20, marginHorizontal: 10,
    borderBottomWidth: 1, borderTopWidth: 1,
    borderColor,
    justifyContent: 'center',
  },
  titleText: {
    color: '#115', fontWeight: '400', fontSize: 18,
  },
  seperateLine: {
    height: 2, backgroundColor: '#AAAAAA'
  },
  grayBorder: {
    margin: 2,
    borderWidth: 1,
    borderColor: '#888', //(#dcdcdc)
  },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    breath: state.breath,
    uxhistory: state.uxhistory,
    theCounter: state.timerCount,
    uiState: state.uiState,
    EttSetting: state.EttSetting,
  };
};

export default connect(mapStateToProps, actions)(HsStudentExamVw);
