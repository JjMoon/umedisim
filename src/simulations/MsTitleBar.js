/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneBpResult / MainMeasureInput.js
 *
 * Created by Jongwoo Moon on 2017. 4. 6..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtButton, HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen } = HtColor;

class MsTitleBar extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ TitleBarOperate :: constructor ]] ');
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { height } = this.props.main.scr;
    const { hRatio = 0.1, titleA, titleB, flex, back, callback, backCallback = Actions.pop } = this.props;
    const smallButtonFlex = 5;
    // () => Actions.popTo('sceneMain')

    return (
        <View style={{ flex, backgroundColor: 'dodgerblue', height: height * hRatio }}>
          <View style={[styl.rowContainer, { flex: 1, padding: 12 }]}>
            <TouchableOpacity
              style={{
                flex: smallButtonFlex,
                flexDirection: 'row', alignSelf: 'flex-end' // 글자 밑으로 정렬.
              }}
              onPress={backCallback}
            >
              <Text style={[sty.miniTitle, { textAlign: 'left', flex: 1, marginLeft: 20 }]}>
                {back}
              </Text>
            </TouchableOpacity>

            <Text style={[sty.title, { flex: 10 }]}>
              {titleA}
            </Text>

            <TouchableOpacity
              style={{
                flex: smallButtonFlex,
                flexDirection: 'row', alignSelf: 'flex-end' // 글자 밑으로 정렬.
              }}
              onPress={callback}
            >
              <Text style={[sty.miniTitle, { textAlign: 'right', flex: 1, marginRight: 10 }]}>
                {titleB}
              </Text>
            </TouchableOpacity>
          </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    alignSelf: 'flex-end',
    color: bttnBrightTextGreen,
    fontSize: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  miniTitle: {
    alignSelf: 'center', textAlign: 'center',
    color: bttnBrightTextGreen,
    fontSize: 18,
    fontWeight: '800',
    marginBottom: 5
  }
});


const mapStateToProps = (state) => {
  return {
    main: state.main,
    //theCounter: state.timerCount,
    //uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(MsTitleBar);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
