/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / simulations / MainMeasureInput.js
 *
 * Created by Jongwoo Moon on 2017. 4. 6..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtText, HtInput, HtButton, HtColor } from '../uicompo';
/* Usage

const vwSty = { margin: 10, marginVertical: 10 }, flexCont = [5, 3, 1];
return (
  <MsTitleInputUnitView
    mandator={{
      keyboardType: 'numeric', unit, title, inputFontSize: 25
    }}
    flexContent={flexCont}
    vwSty={vwSty}
    value={value}
    onChange={onChange}
  />
);
*/

class MsTitleInputUnitView extends Component {

  render() {
    const { mandator, vwSty, flexContent = [4, 3, 2], onChange, value } = this.props;
    const { flex = 1, title, unit = '', keyboardType, inputFontSize = 17 } = mandator;

    return (
      <View style={[sty.container, vwSty, { flex }]}>
        <HtText
          mandator={{ flex: flexContent[0], rad: 5, text: title, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: sty.titleSty }}
        />

        <HtInput
          mandator={{
            flex: flexContent[1], rad: 5, value, margin: { },
            onChange
          }}
          multiline={false}
          keyboardType={keyboardType}
          style={{ input: { fontSize: inputFontSize }, border: sty.grayBoarder }}
        />

        <HtText
          mandator={{ flex: flexContent[2], rad: 5, text: unit, margin: { r: 10 }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: sty.txt }}
        />
      </View>
    );
  }
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flexDirection: 'row', alignSelf: 'stretch', borderColor: '#EAEFEF'
  },
  vw: { backgroundColor: 'white', margin: 3 },
  titleSty: { color: HtColor.bgDarkGray, fontWeight: '500', fontSize: 25 },
  txt: { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 18 },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },

});

//export default connect(mapStateToProps, actions)(MsTitleInputUnitView);
export { MsTitleInputUnitView };
