/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / simulations / HsControlBar.js
 *
 * Created by Jongwoo Moon on 2017. 4. 19..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, LayoutAnimation } from 'react-native';
// import { connect } from 'react-redux';
// import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen, bgBlue } = HtColor;

class HsControlBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: false,
    };
    console.log('\n ====== ====== ====== ======  [[ ControlBar :: constructor ]] ');
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    console.log('\n ====== ====== ====== ======  [[ ControlBar :: componentDidMount ]]');
  }

  playAndPause() {
    //console.log(`playAndPause() send >>>>>>>  ${this.state.isPlaying}  ${this.state.modalVisible}`);
    if (this.state.isPlaying) { // mainView 로 보내는 콜백..
      this.props.stopCallback();
    } else {
      this.props.playCallback();
    }
  }

  bttnActSave() {

  }

  getPlayButton() {
    // const { height } = this.props.main.scr, size = height * 0.11;
    // const { isPlaying } = this.props.OperBP;
    //
    // if (isPlaying) {
    //   return (
    //     <Image
    //       style={{ width: size, height: size }}
    //       source={require('../resource/imageCommon/pausebutton.png')}
    //     />
    //   );
    // }
    // return (
    //   <Image
    //     style={{ width: size, height: size }}
    //     source={require('../resource/imageCommon/playbutton.png')}
    //   />
    // );
  }

  // <TouchableOpacity onPress={this.playAndPause.bind(this)}>
  //   {this.getPlayButton()}
  // </TouchableOpacity>

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex, scrH, onSavePress } = this.props;
    // save 버튼 크기 111 87
    const scl = 0.001 * scrH, saveX = 111 * scl, saveY = 87 * scl;
    return (
      <View style={{ flex, backgroundColor: 'dodgerblue' }}>

        <View style={[styl.rowContainer, { flex: 100 }]}>

          <View style={{ flex: 8 }} />

          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={onSavePress}>
            <Image
              style={{ width: saveX, height: saveY }}
              source={require('../resource/imageCommon/save.png')}
            />
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    color: bttnBrightTextGreen,
    fontSize: 30,
    fontWeight: '600',
    textAlign: 'left',
    margin: 20,
    marginTop: 35,
    alignSelf: 'stretch'
  },
});


export { HsControlBar };
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
