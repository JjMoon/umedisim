/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / simulations / ResultUnitView.js
 *
 * Created by Jongwoo Moon on 2017. 4. 6..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtText, HtInput, HtButton, HtColor } from '../uicompo';

/*
<MsTitleInputUnitView
  mandator={{ flex: 3 }}
/>

*/

class ResultUnitView extends Component {

  render() {
    const { mandator, details, flexContent = [3, 2, 2, 2, 2] } = this.props;
    const { flex = 1, title } = mandator;

    return (
      <View style={[sty.container, { flex }]}>
        <HtText
          mandator={{ flex: flexContent[0], rad: 5, text: title, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: sty.titleSty }}
        />

        {this.renderDetail(details[0], flexContent[1])}
        {this.renderDetail(details[1], flexContent[2])}
        {this.renderDetail(details[2], flexContent[3])}
        {this.renderDetail(details[3], flexContent[4], true)}

      </View>
    );
  }

  renderDetail(dtl, flex, boldCase = false) {
    const { num, unit, expl } = dtl;
    const fntSiz = 10, bldSiz = 12;
    const untSty = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: fntSiz * 1.3 };
    const numSty = { color: HtColor.bgDarkGray, fontWeight: '400', fontSize: fntSiz * 2.2 };
    const explain = { color: HtColor.bgDarkGray, fontWeight: '400', fontSize: fntSiz * 1.8 };
    const untStyBld = { color: HtColor.bgDarkGray, fontWeight: '400', fontSize: bldSiz * 1.5 };
    const numStyBld = { color: HtColor.bgDarkGray, fontWeight: '800', fontSize: bldSiz * 2.2 };
    const explainBld = { color: HtColor.bgDarkGray, fontWeight: '800', fontSize: bldSiz * 1.8 };

    const uSty = boldCase ? untStyBld : untSty;
    const nmSty = boldCase ? numStyBld : numSty;
    const exSty = boldCase ? explainBld : explain;

    return (
      <View style={{ flex, alignItems: 'stretch', paddingVertical: 10 }}>
        <View style={{ flex: 5, flexDirection: 'row', alignItems: 'stretch' }}>

          <HtText
            mandator={{ flex: 3, rad: 5, text: `${num}`, margin: { all: 0 }, radIgnore: true }}
            style={{ viewStyle: sty.vw, textStyle: nmSty }}
          />
          <HtText
            mandator={{ flex: 2, rad: 5, text: unit, margin: { }, radIgnore: true }}
            style={{ viewStyle: sty.vw, textStyle: uSty }}
          />

        </View>

        <HtText
          mandator={{ flex: 3, rad: 5, text: expl, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: exSty }}
        />

      </View>
    );
  }
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flexDirection: 'row', alignSelf: 'stretch', // borderColor: '#EAEFEF'
  },
  vw: { backgroundColor: 'white', margin: 3 },
  titleSty: { color: HtColor.bgDarkGray, fontWeight: '500', fontSize: 25 },
  txt: { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 18 },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },

});

//export default connect(mapStateToProps, actions)(MsTitleInputUnitView);
export { ResultUnitView };
