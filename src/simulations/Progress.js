/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneOper / Progress.js
 *
 * Created by Jongwoo Moon on 2017. 2. 5..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, View, Text, Animated, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
//import _ from 'lodash'; //import Animation from 'lottie-react-native'; // 나중에 시도..
import { HtText, HtColor } from '../uicompo';

class UnitProgress extends Component {
  constructor(props) {
    super(props);
    this.state = props.initState;
  }

  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut(); //spring();
  }

  render() {
    let txt = '', bgCol = HtColor.bgDarkGray, txtCol = '#599', flexVal = 10;
    const { pass, isErrorCase, pFlex, playMode } = this.props;
    txt = this.props.initState.short;
    if (pass) {
      bgCol = HtColor.bgBlue;
      txtCol = 'white';
    }
    if (isErrorCase && pass) {
      bgCol = HtColor.firebrickRed;
    }
    if (pFlex > 0) {
      //console.log(` ${txt} {pFlex}  ${pFlex}`);
      flexVal = pFlex;
    }

    if (!playMode) {
      bgCol = '#8AC';
      txtCol = '#9CF';
    }

    return (
      <View style={[styU.unitProgress, { flex: flexVal }]}>
        <HtText
          mandator={{ flex: 3, rad: 5, text: txt, margin: { l: 3, r: 3 } }}
          style={{
            viewStyle: [styU.vw, { backgroundColor: bgCol }],
            textStyle: [styU.txt, { color: txtCol }]
          }}
        />
      </View>
    );
  }
}

////////////////////////////////////////////////////   _//////////////////_     style
const styU = StyleSheet.create({
  unitProgress: {
     justifyContent: 'center', alignItems: 'stretch', alignSelf: 'stretch',
     flex: 10, padding: 5
  },
  vw: { backgroundColor: HtColor.bgBlue, margin: 20 },
  txt: { color: 'white', fontWeight: '900', fontSize: 17 },
});

class Progress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //thrStr: this.props.uxhistory.thrStr, // 삽관 부족, 정상, 과다.
      //progress: new Animated.Value(0),
    };
  }
  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut(); //spring();
  }

  render() {
    const { langAutoChk, langKey, img, uicommon } = this.props.uiState;
    const { ux, playMode, onceChk } = this.props.uxhistory;
    const { insert } = img;
    const { insInfo, insShort, insNorm, insOver } = uicommon[langKey];
    const opSt = this.props.main.operatingState;

    let insrtStr = (insert === 'OVER') ? insOver : insNorm;
    insrtStr = (insert === 'UNDR') ? insShort : insrtStr;
    insrtStr = (insert === 'NONE') ? insInfo : insrtStr;

    const { secureRespTrack, pushChin, snippingPosition, brokenTooth, bronchus,
      stomachMask, stomachGullet, justGullet
    } = langAutoChk[langKey];

    //console.log(`${onceChk.brokenTooth}   ${secureRespTrack}`);

    return (
      <View style={sty.progressContainer}>
        <UnitProgress
          curState={opSt} isErrorCase={false} playMode={playMode}
          pass={onceChk.secureRespTrack}
          initState={{ uid: 0, short: secureRespTrack.shr, long: secureRespTrack.long }}
        />
        <UnitProgress
          curState={opSt} isErrorCase={false} playMode={playMode}
          pass={onceChk.pushChin}
          initState={{ uid: 1, short: pushChin.shr, long: pushChin.long }}
        />
        <UnitProgress
          curState={opSt} isErrorCase={false} playMode={playMode}
          pass={onceChk.snippingPosition}
          initState={{ uid: 2, short: snippingPosition.shr, long: snippingPosition.long }}
        />
        <UnitProgress
          curState={opSt} isErrorCase={false} playMode={playMode}
          pass={insert !== 'NONE'} pFlex={13}
          initState={{ uid: 2, short: insrtStr, long: '삽관 부족' }}
        />

        <UnitProgress
          curState={opSt} isErrorCase playMode={playMode}
          pass={onceChk.brokenTooth}
          initState={{ uid: 2, short: brokenTooth.shr, long: '이빨 부러짐.' }}
        />

        <UnitProgress
          curState={opSt} isErrorCase playMode={playMode}
          pass={onceChk.stomachMask}
          initState={{ uid: 2, short: stomachMask.shr, long: 'Bronchus Error' }}
        />
        <UnitProgress
          curState={opSt} isErrorCase playMode={playMode}
          pass={onceChk.stomachGullet}
          initState={{ uid: 2, short: stomachGullet.shr, long: 'Bronchus Error' }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    main: state.main,
    uxhistory: state.uxhistory,
    uiState: state.uiState,
    arrPacket: state.arrPacket,
  };
};


////////////////////////////////////////////////////   _//////////////////_     style
const sty = StyleSheet.create({
  progressContainer: {
    flexDirection: 'row', flex: 1,
    justifyContent: 'flex-start', backgroundColor: '#CDE',
    alignItems: 'stretch', alignSelf: 'stretch',
  },
});

export default connect(mapStateToProps)(Progress);

// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
