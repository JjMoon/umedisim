import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import Meteor from 'react-native-meteor';

import * as actions from '../actions';
import * as M from '../model';
import { HtRectBttn, HtText, HtColor, HtInput } from '../uicompo';
import MsTitleBar from '../simulations/MsTitleBar';
import HsStudentExamVw from '../simulations/HsStudentExamVw';

/**
 * Content BP
 */
class ContentBp extends Component {
  constructor(props) {
    super(props);

    const { BP } = this.props.main.curReport;
    const mail = BP === null ? null : BP.base.email;

    this.state = {
      mail,
    };
    this.debugMsg = 'init';
  }

  sendReport() {
    const { base, restStr } = this.props.main.curReport.BP;
    // const { mode, startTime, stdntInfo, email, arrImgJson, totalScore, arrPacket } = base;
    const obj = { ...base, restStr };

    console.log(obj);
    console.log(this.props.main.curReport.BP);

    Meteor.call('Repo.addOne', obj, (err, res) => {
      console.log('Items.addOne', err, res);
    });
  }

  setEmail(txt) {
    const { BP } = this.props.main.curReport;
    BP.base.changeEmail(txt);
    this.forceUpdate();
  }

  renderDebug(msg) {
    this.debugMsg = msg;
    this.forceUpdate();
  }

  sendRnEmail(addr, path) {
    console.log(` Send Mail to ${addr}  with ${path}`);
    this.renderDebug('Mailer');

    Mailer.mail(
      {
        subject: 'ETT 결과지',
        recipients: [addr],
        body: '<h1>Report</h1>',
        isHTML: true, // iOS only, exclude if false
        attachment: {
          path, // The absolute path of the file from which to read data.
          type: 'pdf', // Mime Type: jpg, png, doc, ppt, html, pdf
          name: 'report.pdf', // Optional: Custom filename for attachment
        },
      },
      (error, event) => {
        if (error) {
          console.log(error);
          this.renderDebug('error');
        }
      },
    );
  }

  render() {
    // const { mail } = this.state;
    const { flex, main } = this.props;
    const { scr } = main;
    const { BP } = main.curReport;

    if (BP === null) {
      return <View style={{ flex, backgroundColor: '#EEE' }} />;
    }

    const { base } = main.curReport.BP;
    const { mode, stdntInfo, email, arrImgJson, arrPacket } = base;
    const arrImg = JSON.parse(arrImgJson),
      width = scr.width * 0.6;
    const imgStyle = {
      width,
      margin: 5,
      alignSelf: 'stretch',
      resizeMode: 'stretch',
    };
    const imgViews = [];

    console.log(imgStyle, arrPacket); // retrieve

    for (let i = 0; i < arrImg.length; i++) {
      console.log(arrImg[i]);

      const newH = i === 0 ? scr.height * 0.45 : scr.height * 0.3;

      imgViews.push(
        <Image
          style={[imgStyle, { height: newH }]}
          source={{ uri: arrImg[i] }}
          key={i}
        />,
      );
    }

    return (
      // <Text>{this.debugMsg}</Text> 디버깅 텍스트..
      <View style={{ flex, backgroundColor: '#EEE' }}>
        <ScrollView>
          <View style={sty.titleContainer}>
            <Text style={sty.mode}>
              {' '}
              Mode : {mode} Student : {stdntInfo}{' '}
            </Text>
            <View style={sty.rowContainer}>
              <HtInput
                mandator={{
                  flex: 5,
                  rad: 4,
                  value: email,
                  margin: { a: 5 },
                  onChange: (txt) => {
                    this.setEmail(txt);
                  },
                }}
                multiline={false}
                style={{ input: { fontSize: 15 }, border: sty.grayBorder }}
              />

              <HtRectBttn
                len={50}
                ratio={3}
                borderR={3}
                txtRatio={0.4}
                ptext={'Send PDF'}
                txtCol={'#FFF'}
                bgCol={'#623'}
                onPressCallback={() => {
                  this.sendReport(); // this.createPDF();
                }}
              />
            </View>
          </View>

          {imgViews}

          <View style={sty.seperateLine} />
        </ScrollView>
      </View>
    );
  }
}

const textBase = {
  textAlign: 'left',
  alignSelf: 'center',
  fontWeight: '300',
  fontSize: 20,
};

const sty = StyleSheet.create({
  titleContainer: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },
  rowContainer: {
    flex: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mode: {
    ...textBase,
    flex: 5,
    fontWeight: '500',
    fontSize: 25,
    marginLeft: 30,
  },
  item: { ...textBase, fontSize: 19, flex: 5, marginLeft: 60 },
  score: { ...textBase, fontSize: 20, flex: 1 },
  img: {
    alignItems: 'stretch',
    justifyContent: 'center',
    margin: 3,
  },
  historyView: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    margin: 3,
  },
  reportContainer: {
    alignItems: 'stretch',
    marginVertical: 20,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    justifyContent: 'center',
  },
  seperateLine: {
    height: 2,
    backgroundColor: '#AAAAAA',
    marginHorizontal: 40,
  },
});

const mapStateToProps = state => ({
  main: state.main,
});

export default connect(mapStateToProps, actions)(ContentBp);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
