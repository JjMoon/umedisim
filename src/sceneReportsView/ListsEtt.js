/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneReportsView / ListsEtt.js
 *
 * Created by Jongwoo Moon on 2017. 5. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, ListView, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import * as M from '../model';
import { HtRectBttn, HtText, HtColor, HtInput } from '../uicompo';
import MsTitleBar from '../simulations/MsTitleBar';
import HsStudentExamVw from '../simulations/HsStudentExamVw';

class ListsEtt extends Component {
  componentWillMount() {
    const objects = M.ExamRlmObj.objects('RmEttResult');
    //console.log(this.props.EttSetting.manCheckItem);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.time = new Date();
    this.state = { // 기본 값과 함께 사용자가 추가한 항목도 가져와야 함.
        //dataSource: ds.cloneWithRows(this.props.chkItems),
        dataSource: ds.cloneWithRows(objects),
        objects
    };
  }

  getDataSource(datum) {
     return this.state.dataSource.cloneWithRows(datum);
  }

  renderRow(dt) {
    const { base } = dt;
    return (
      <TouchableOpacity
        style={sty.cell}
        onPress={() => this.props.setCurReport({ ETT: dt, BP: null })}
      >
        <Text style={{ color: '#115', fontWeight: '400', fontSize: 17 }}>
          {base.mode} : {base.stdntInfo}
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    //const objects = M.ExamRlmObj.objects('RmEttResult');
    //console.log(`  ListsEtt.js    obj length : ${objects.length}`);
    return (
      <View style={{ flex: 100, backgroundColor: '#EEE' }} >
      <ListView
        style={{ flex: 100, alignSelf: 'stretch', }}
        enableEmptySections
        dataSource={this.getDataSource(this.state.objects)}
        renderRow={this.renderRow.bind(this)}
      />
      </View>
    );
  }

}

const sty = StyleSheet.create({
  cell: {
    flex: 1, height: 40, justifyContent: 'center', borderRadius: 3,
    borderColor: '#234', borderWidth: 1,
    alignItems: 'stretch', marginVertical: 3, marginHorizontal: 5,
    paddingLeft: 10
  },
});


const mapStateToProps = (state) => {
  return {
    main: state.main,
    breath: state.breath,
    uxhistory: state.uxhistory,
    theCounter: state.timerCount,
    uiState: state.uiState,
    EttSetting: state.EttSetting,
  };
};

export default connect(mapStateToProps, actions)(ListsEtt);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
