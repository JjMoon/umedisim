import React, { Component } from 'react';
import { View, StyleSheet, Text, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import * as M from '../model';
import ListsEtt from './ListsEtt';
import ListsBp from './ListsBp';
import ContentEtt from './ContentEtt';
import ContentBp from './ContentBp';
import { HtRectBttn, HtText, HtColor, HtInput } from '../uicompo';
import MsTitleBar from '../simulations/MsTitleBar';
import HsStudentExamVw from '../simulations/HsStudentExamVw';

// const dateFormat = require('dateformat');

/**
 * Report View Main.
 */
class ReportsMainView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ettCase: true,
    };
  }

  // //////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {}

  componentWillUnmount() {
    this.props.resetEttExam();
  }

  renderButtons() {
    const { ettCase } = this.state;
    const bgCol = '#184',
      txtCol = '#FDF',
      graycol = '#888';
    const [ettBgcol, bpBgcol, ettStr, bpStr] = ettCase
      ? [graycol, bgCol, 'ETT', 'Select BP']
      : [bgCol, graycol, 'Select ETT', 'BP'];
    return (
      <View style={{ flex: 10, flexDirection: 'row' }}>
        <HtRectBttn
          len={50}
          ratio={2.5}
          borderR={3}
          txtRatio={0.4}
          ptext={ettStr}
          txtCol={txtCol}
          bgCol={ettBgcol}
          onPressCallback={() => {
            this.props.setCurReport({ ETT: null, BP: null });
            this.setState({ ettCase: true });
          }}
        />
        <HtRectBttn
          len={50}
          ratio={2.5}
          borderR={3}
          txtRatio={0.4}
          ptext={bpStr}
          txtCol={txtCol}
          bgCol={bpBgcol}
          onPressCallback={() => {
            this.props.setCurReport({ ETT: null, BP: null });
            this.setState({ ettCase: false });
          }}
        />
      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState; // 언어팩..
    const { resultTitle } = uicommon[langKey];
    const { ettCase } = this.state;

    const contentEtt = <ContentEtt flex={60} />,
      contentBp = <ContentBp flex={60} />;
    const [listView, contentView] = ettCase
      ? [<ListsEtt />, contentEtt]
      : [<ListsBp />, contentBp];

    return (
      <View style={{ flex: 100 }}>
        <MsTitleBar titleA={resultTitle} back={'Back'} />

        <View style={{ flexDirection: 'row', flex: 90 }}>
          <View style={{ flex: 40 }}>
            {this.renderButtons()}

            {listView}
          </View>

          <View style={sty.seperateLineVer} />

          {contentView}
        </View>

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}

        <View style={{ flex: 5, backgroundColor: '#89a' }} />
      </View>
    );
  }
}

const borderColor = '#AAA';

const sty = StyleSheet.create({
  unitContainer: {
    alignItems: 'stretch',
    justifyContent: 'center',
    borderWidth: 1,
    margin: 5,
    borderColor, // : 'gainsboro', //(#dcdcdc)
    borderRadius: 3,
  },
  vw: { alignItems: 'stretch', justifyContent: 'center' },
  reportContainer: { alignItems: 'stretch', marginVertical: 20,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor,
    justifyContent: 'center',
  },
  titleText: { color: '#115', fontWeight: '400', fontSize: 18  },
  seperateLine: { height: 2, backgroundColor: '#AAAAAA' },
  seperateLineVer: { width: 2, margin: 2, backgroundColor: '#999' },
  grayBorder: {
    margin: 2,
    borderWidth: 1,
    borderColor: '#888', // (#dcdcdc)
  },
});

const mapStateToProps = state => ({
  main: state.main,
  breath: state.breath,
  uxhistory: state.uxhistory,
  theCounter: state.timerCount,
  uiState: state.uiState,
  EttSetting: state.EttSetting,
});

export default connect(mapStateToProps, actions)(ReportsMainView);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
