import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  NativeModules,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Meteor from 'react-native-meteor';

import * as actions from '../actions';
import * as M from '../model';
import { HtRectBttn, HtText, HtColor, HtInput } from '../uicompo';
import MsTitleBar from '../simulations/MsTitleBar';
import HsStudentExamVw from '../simulations/HsStudentExamVw';

const Mailer = require('NativeModules').RNMail;

/**
 * Content ETT
 */
class ContentEtt extends Component {
  constructor(props) {
    super(props);

    const { ETT } = this.props.main.curReport;
    const mail = ETT === null ? null : ETT.base.email;

    this.state = {
      mail,
    };
    this.debugMsg = 'init';
  }

  sendReport() {
    const { arrHistory, base } = this.props.main.curReport.ETT;
    // const { mode, startTime, stdntInfo, email, arrImgJson, totalScore, arrPacket } = base;
    const obj = { ...base, arrHistory };

    console.log(obj);

    console.log(arrHistory);

    console.log(this.props.main.curReport.ETT);

    Meteor.call('Repo.addOne', obj, (err, res) => {
      console.log('Items.addOne', err, res);
    });
  }

  createPDF() {
    this.renderDebug('createPDF');

    const { arrHistory, base } = this.props.main.curReport.ETT;
    const { mode, stdntInfo, arrImgJson, totalScore } = base;
    const arrImg = JSON.parse(arrImgJson);

    const paperSC = 4,
      sc = 0.5,
      imgW = 2048 * sc,
      imgH = 1220 * sc;

    // <img src="pic_mountain.jpg" alt="Mountain View" style="width:304px;height:228px;">
    const sz = `style="width:${imgW}px;height:${imgH}px;"`; // 2048 1220
    const title = `<h2> 모드 : ${mode},    실습생 : ${stdntInfo} </h2>`,
      img1 = `<p><img src="${arrImg[0]}" ${sz}></p>\n`,
      img2 = `<p><img src="${arrImg[1]}" ${sz} ></p>\n`;
    let scores = '<p>'; // 점수 HTML
    for (let i = 0; i < arrHistory.length; i++) {
      const { shr, score } = arrHistory[i];
      scores += ` ${shr} : ${score} <br> \n`;
    }
    scores += `</p> <p> Total : ${totalScore} </p> \n`;

    // const theBody = `${title} ${img1} ${img2} ${scores}`; // HTML String
    const theBody = `${title} `; // HTML String
    const htmlBase = `<!DOCTYPE html><html><head><meta charset="UTF-8" /><title>결과지</title></head><body>${theBody}</body></html>`;
    const options = {
      html: htmlBase, // HTML String

      // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************
      fileName: 'test2img',
      directory: 'docs',
      base64: true,
      height: 297 * paperSC,
      width: 210 * paperSC,
      padding: 24,
    };

    console.log('  PDF 변환 단계 ....');

    this.renderDebug(options.html);
    RNHTMLtoPDF.convert(options).then((data) => {
      console.log(data.filePath);
      // console.log(data.base64);
      this.file = { path: data.filePath };
      this.renderDebug('convert inside');

      this.sendRnEmail(this.props.main.curReport.ETT.base.email, data.filePath);
    });
  }

  setEmail(txt) {
    const { ETT } = this.props.main.curReport;
    ETT.base.changeEmail(txt);
    this.forceUpdate();
  }

  renderDebug(msg) {
    this.debugMsg = msg;
    this.forceUpdate();
  }

  sendRnEmail(addr, path) {
    console.log(` Send Mail to ${addr}  with ${path}`);
    this.renderDebug('Mailer');

    Mailer.mail(
      {
        subject: 'ETT 결과지',
        recipients: [addr],
        body: '<h1>Report</h1>',
        isHTML: true, // iOS only, exclude if false
        attachment: {
          path, // The absolute path of the file from which to read data.
          type: 'pdf', // Mime Type: jpg, png, doc, ppt, html, pdf
          name: 'report.pdf', // Optional: Custom filename for attachment
        },
      },
      (error, event) => {
        if (error) {
          console.log(error);
          this.renderDebug('error');
        }
      },
    );
  }

  render() {
    // const { mail } = this.state;
    const { flex, main } = this.props;
    const { scr } = main;
    const { ETT } = main.curReport;

    if (ETT === null) {
      return <View style={{ flex, backgroundColor: '#EEE' }} />;
    }

    const { arrHistory, base } = main.curReport.ETT;
    const { mode, stdntInfo, email, arrImgJson, totalScore, arrPacket } = base;

    console.log(arrPacket); // retrieve

    console.log(arrImgJson);

    const arrImg = JSON.parse(arrImgJson);
    const width = scr.width * 0.6,
      hght = scr.height * 0.4;
    const imgStyle = {
      width,
      height: hght,
      margin: 4,
      alignSelf: 'stretch',
      resizeMode: 'contain',
    };
    const imgViews = [],
      histrs = [];

    for (let i = 0; i < arrImg.length; i++) {
      console.log(arrImg[i]);
      imgViews.push(
        <Image style={imgStyle} source={{ uri: arrImg[i] }} key={i} />,
      );
    }
    for (let i = 0; i < arrHistory.length; i++) {
      const { shr, score } = arrHistory[i];
      histrs.push(
        <View style={sty.historyView} key={i}>
          <Text style={sty.item}>{shr}</Text>
          <Text style={sty.score}>{score}</Text>
        </View>,
      );
    }

    return (
      // this.createPDF.bind(this)
      <View style={{ flex, backgroundColor: '#EEE' }}>
        <Text>{this.debugMsg}</Text>
        <ScrollView>
          <View style={sty.titleContainer}>
            <Text style={sty.mode}>
              {' '}
              Mode : {mode} Student : {stdntInfo}{' '}
            </Text>
            <View style={sty.rowContainer}>
              <HtInput
                mandator={{
                  flex: 5,
                  rad: 4,
                  value: email,
                  margin: { a: 5 },
                  onChange: (txt) => {
                    this.setEmail(txt);
                  },
                }}
                multiline={false}
                style={{ input: { fontSize: 15 }, border: sty.grayBorder }}
              />

              <HtRectBttn
                len={50}
                ratio={3}
                borderR={3}
                txtRatio={0.4}
                ptext={'Send PDF'}
                txtCol={'#FFF'}
                bgCol={'#623'}
                onPressCallback={() => {
                  this.sendReport(); // this.createPDF();
                }}
              />
            </View>
          </View>

          {imgViews}
          {histrs}

          <View style={sty.seperateLine} />

          <View style={sty.historyView}>
            <Text style={sty.item}>Total</Text>
            <Text style={sty.score}>{totalScore}</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const textBase = {
  textAlign: 'left',
  alignSelf: 'center',
  fontWeight: '300',
  fontSize: 20,
};

const sty = StyleSheet.create({
  titleContainer: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },
  rowContainer: {
    flex: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mode: {
    ...textBase,
    flex: 5,
    fontWeight: '500',
    fontSize: 25,
    marginLeft: 30,
  },
  item: { ...textBase, fontSize: 19, flex: 5, marginLeft: 60 },
  score: { ...textBase, fontSize: 20, flex: 1 },
  img: {
    alignItems: 'stretch',
    justifyContent: 'center',
    margin: 3,
  },
  historyView: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    margin: 3,
  },
  reportContainer: {
    alignItems: 'stretch',
    marginVertical: 20,
    marginHorizontal: 10,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    justifyContent: 'center',
  },
  seperateLine: {
    height: 2,
    backgroundColor: '#AAAAAA',
    marginHorizontal: 40,
  },
});

const mapStateToProps = state => ({
  main: state.main,
});

export default connect(mapStateToProps, actions)(ContentEtt);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
