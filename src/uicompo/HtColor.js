/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtColor.js
 *
 * Created by Jongwoo Moon on 2017. 1. 1..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

class HtColor {
  // https://facebook.github.io/react-native/docs/colors.html
  static statusBarColor = 'lemonchiffon'; // lemonchiffon (#fffacd)
  static bttnTouchableHighlightUnderlayColor = '#888';
  static bttnDarkBG = '#15E';
  static bttnBrightTextGreen = '#9DE';
  static firebrickRed = 'firebrick'; // B2 22 22

  /// Back Ground Color
  static blueGray = '#5A5E7F';
  static bgBlue = 'deepskyblue'; // deepskyblue (#00bfff)  dodgerblue (#1e90ff)
  static bgCyan = '#9DF3';
  static bgBrightGray = '#EDE';
  static bgDarkGray = '#345';
  static almostWhite = '#F8F8FF';
  /// Text Color
  static textWhite = '#DDEEFF';
  static txtDarkGray = '#115';
  static txtScoreRed = '#931';
}

export { HtColor };
