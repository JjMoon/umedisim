/**
 */
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { HtColor } from './HtColor';

/*
사용 example =========================================================================================
<HtButton width={100} fWidth={1} ptext={'Other'}
  onPressCallback={this._bttnActOtherView.bind(this)}>
사용 example =========================================================================================
*/
//const styl = require('./HtStyles');
///-------------------------  UI Components  -------------------------  버튼..
class HtButton extends Component {
   //  {/* View -------------------------  버튼 Location  ------------------------- View */}
  render() {
    const { txtStyle, vwStyle, ptext, active = true, onPressCallback, f, w, normal = null } = this.props;
    const onPress = active ? onPressCallback : null;

    const vwSty = (normal === null) ? sty.baseView : sty.normalView;
    const txtSty = (normal === null) ? sty.buttonLabel : sty.normalText;

    return (
      <TouchableOpacity
        style={[vwSty, { width: w, flex: f }, vwStyle]}
        underlayColor={'#CCDDEE'}
        onPress={onPress}
      >
        <Text style={[txtSty, txtStyle]}>{ptext}</Text>
      </TouchableOpacity>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  baseView: {
    flex: 1, flexDirection: 'row',
    justifyContent: 'center', alignItems: 'stretch', alignSelf: 'stretch',
  },
  buttonLabel: {
    flex: 1, alignSelf: 'center', textAlign: 'center',
    fontWeight: '400', color: '#0000FF', fontSize: 11
  },
  normalView: {
    flex: 1, flexDirection: 'row',
    justifyContent: 'center', alignItems: 'stretch', alignSelf: 'stretch',
    borderRadius: 5, backgroundColor: HtColor.bttnDarkBG
  },
  normalText: {
    flex: 1, alignSelf: 'center', textAlign: 'center',
    fontWeight: '400', color: HtColor.almostWhite, fontSize: 11
  }
});

export { HtButton };

/*
parameter ::
  ptext : 버튼 라벨 텍스트
  onPressCallback : 콜백 함수
  fWidth : flex 값.
  width : 절대값 폭.
  example ________________________________________________________________________________
  <HtButton width={100} fWidth={1} ptext={'Other'}
    onPressCallback={this._bttnActOtherView.bind(this)}>
  example ________________________________________________________________________________
*/
