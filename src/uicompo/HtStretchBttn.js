/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtStretchBttn.js
 *
 * Created by Jongwoo Moon on 2017. 2. 13. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, LayoutAnimation, TouchableHighlight } from 'react-native';

const styl = require('../uicompo/HtStyles');

class HtStretchBttn extends Component {
  constructor(props) {
    super(props);
    this.state = { // 변하거나 default 값을 줄 때는 상태변수에 재 할당 함..
      flex: this.props.flex || 1
    };
  }
  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut(); //spring();
  }

  render() {
    return (
      <TouchableHighlight
        style={[styl.buttonBase, {
          flex: this.state.flex,
          backgroundColor: this.props.bgCol,
          borderRadius: this.props.borderR,
          }]}
        underlayColor={'#CCDDEE'}
        onPress={this.props.onPressCallback}
      >
        <Text
          style={[sty.buttonLabel, {
          color: this.props.txtCol,
          fontSize: this.props.len * this.props.txtRatio }]}
        >
          {this.props.ptext}
        </Text>
      </TouchableHighlight>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  buttonLabel: {
    fontWeight: '400',
    textAlign: 'center',
  },

});


export { HtStretchBttn };
