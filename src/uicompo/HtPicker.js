/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtPicker.js
 *
 * Created by Jongwoo Moon on 2017. 2. 28..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet } from 'react-native';
import { HtText, HtColor } from './';

/*
사용 example =========================================================================================
<HtButton width={100} fWidth={1} ptext={'Other'}
  onPressCallback={this._bttnActOtherView.bind(this)}>
사용 example =========================================================================================
*/
//const styl = require('./HtStyles');
///-------------------------  UI Components  -------------------------  버튼..
class HtPicker extends Component {

  // 안드로이드 에서 드롭다운..

   //  {/* View -------------------------  버튼 Location  ------------------------- View */}
  render() {
    const { flex, title, ttlFontSize, selected, onValueChange, pickerItems, ttlSty } = this.props.mandator;
    const ttlDefault = { // HtText  텍스트, 뷰 스타일
      color: HtColor.bgDarkGray, fontWeight: '300', fontSize: ttlFontSize
    }, vwSty = {
      margin: 3, justifyContent: 'flex-end', alignItems: 'flex-end'
    };
    // console.log(pickerItems[0].props);
    return (
      <View style={{ flex, alignItems: 'stretch' }}>
        <HtText
          mandator={{ flex: 4, rad: 5, text: title, margin: { all: 1 }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: [ttlDefault, ttlSty] }}
        />
        <Picker
          style={{ flex: 4 }}
          selectedValue={selected}
          onValueChange={onValueChange}
          mode={'dropdown'}
        >
          {pickerItems}
        </Picker>
      </View>
    );
  }
} // mode={'dropdown'}  'dialog'

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  buttonBase: {
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    //borderRadius: 5,    backgroundColor: '#EDEFED',
    padding: 3,
    margin: 5,
  },
  buttonLabel: {
    alignSelf: 'stretch',
    textAlign: 'center',
    flex: 1,
    fontWeight: '400',
    color: '#0000FF',
    fontSize: 11
  },
});

export { HtPicker };
