/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtText.js
 *
 * Created by Jongwoo Moon on 2017. 3. 9..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

/*
  << props >>
    flex : 전체 뷰의 flex 값.
    rad  : 코너 Radius Text margin 도 같이 주어야 함.

  << 사용 예 >>
    <HtText
      mandator={{ flex: 3, rad: 5, text: autoManTxt, margin: { r: 10 }, radIgnore }}
      style={{ viewStyle: sty.vw, textStyle: sty.txt }}
    />

    vw: { backgroundColor: 'white', margin: 3 },
    txt: { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 18 },

*/

///-------------------------  UI Components  -------------------------  버튼..
class HtText extends Component {
  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex, rad, text, margin, radIgnore = false } = this.props.mandator;
    let { t = 0, b = 0, l = 0, r = 0, lr = 0, tb = 0, all = 0 } = margin;
    if (lr > 0) { l = r = lr; }
    if (tb > 0) { t = b = tb; }
    if (all > 0) { l = r = b = t = all; tb = lr = all = 0; }
    const txtMargin = radIgnore ? 0 : rad;

    const { viewStyle, textStyle } = this.props.style;
    return (
      <View
        style={[sty.container, viewStyle,
          {
            flex,
            borderRadius: rad,
            marginTop: t, marginBottom: b, marginLeft: l, marginRight: r
          }]}
      >
        <Text
          allowFontScaling
          style={[sty.textBase, textStyle,  // 텍스트
            {
              marginLeft: txtMargin, marginRight: txtMargin // 텍스트 폭이 작으면 이 값을 조정해야..
            }]}
        >
          {text}
        </Text>
      </View>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    justifyContent: 'center', alignItems: 'center',
    flexDirection: 'row' // 상하 방향의 중앙에 넣기 위해서.
  },
  textBase: {
    flex: 100,
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'center',
    // backgroundColor: '#5A5', // debugging
    color: '#0000FF',
    fontSize: 14
  },
});

export { HtText };
