/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtStyles.js
 *
 * Created by Jongwoo Moon on 2017. 1. 1..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import { StyleSheet } from 'react-native';
import { HtColor }  from './HtColor';

///-------------------------  UI Components  -------------------------  버튼..
const fontSizeBase = 12, fontSizeTitle = 15, fontSizeButton = 12, fontSizeInput = 15;

///-------------------------  UI Components  -------------------------  버튼..
module.exports = StyleSheet.create(
{
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    margin: 5
    //backgroundColor: '#F5FCFF',
  },
  containerTop: {
    flex: 1,
    marginTop: 55, marginLeft: 10, marginRight: 10,
    alignItems: 'center',  // 'flex-start', //'center',
    justifyContent: 'flex-start', //  'center', // 화면의 위에 배치.. 센터로 하면 가운데로 내려옴..
    //alignSelf: 'flex-start'
    //backgroundColor: '#F5FCFF',
  },
  containerLeftAlgin: {
    alignItems: 'flex-start'
  },
  rowContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'stretch',
  },
  /// -------------------------  -------------------------  // <Text>
  title: {
    fontSize: fontSizeTitle,
    textAlign: 'center',
    margin: 5,
    alignSelf: 'stretch'
  },
  ttlLeft3: { // 제목 3 .. 작은 제목.
    fontWeight: '500',
    fontSize: fontSizeBase,
    textAlign: 'left',
    margin: 4,
  },
  content: { // Text
    textAlign: 'left',
    fontSize: fontSizeBase,
    color: '#051111',
    margin: 3,
  },
  buttonLabel: { // Text
    textAlign: 'center',
    fontWeight: '400',
    alignSelf: 'center',
    alignItems: 'stretch',
    color: HtColor.bttnBrightTextGreen, //'#0000FF',
    fontSize: fontSizeButton
  },
  /// -------------------------  -------------------------  // <TextInput>
  inputCom: {  // 입력 상자
    justifyContent: 'center',
    alignSelf: 'center',
    margin: 2,
    marginLeft: 5, marginRight: 5,
    fontSize: fontSizeInput,
    backgroundColor: '#DDD',
    borderWidth: 0, borderRadius: 4,
  },
  /// -------------------------  -------------------------  // <Button, TouchableHighlight, TouchableOpacity>
  buttonRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'stretch',
    alignSelf: 'center',
  },
  buttonBase: {
    justifyContent: 'center',
    alignSelf: 'stretch', //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    borderRadius: 5,
    padding: 3,
    margin: 5,
    backgroundColor: HtColor.bttnDarkBG, //'#3AE',
    alignItems: 'stretch'
  },
});
