/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / uicompo / HtMimaxView.js
 *
 * Created by Jongwoo Moon on 2017. 1. 20..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';
import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Text, TouchableHighlight } from 'react-native';
import { HtColor } from './HtColor';

const styl = require('./HtStyles');

class HtMimaxView extends Component {
  render() {
    const { obj } = this.props;
    const { uiVal, normVal } = obj;
    const flxTitle = 4, flxInput = 6, flxTilde = 2, viewPadding = 20; // 간격 조절.

    return (
      <View style={[styl.container, { paddingLeft: viewPadding, paddingRight: viewPadding }]}>

        {/* -------------------------  -------------------------  -------------------------    설 명 .. ... */}
        <View style={[styl.container, { flex: 5 }]}>
          <Text style={[styl.title, { flex: 2 }]}> {obj.title} </Text>
          <Text style={[styl.content, { flex: 2 }]}>센서 값 : 11234 </Text>
        </View>

        {/* -------------------------  -------------------------  -------------------------    화면 표시 최소/최대 .. ... */}
        <View style={[styl.rowContainer, { flex: 3 }]}>
          <Text style={{ flex: flxTitle, textAlign: 'right' }} >화면 표시 : </Text>
          <TextInput
            style={[styl.inputCom, sty.txtInput, { flex: flxInput }]}
            value={uiVal.min.toString()}
          />
          <Text style={[sty.txtGeneral, { flex: flxTilde }]} >~</Text>
          <TextInput
            style={[styl.inputCom, sty.txtInput, { flex: flxInput }]}
            value={uiVal.max.toString()}
          />
        </View>

        {/* -------------------------  -------------------------  -------------------------    Sensor 최소/최대 .. ... */}
        <View style={[styl.rowContainer, { flex: 3 }]}>
          <Text style={{ flex: flxTitle, textAlign: 'right' }} >센서 값 : </Text>
          <TextInput
            style={[styl.inputCom, sty.txtInput, { flex: flxInput }]}
            value={normVal.min.toString()}
          />
          <Text style={[sty.txtGeneral, { flex: flxTilde }]} >~</Text>
          <TextInput
            style={[styl.inputCom, sty.txtInput, { flex: flxInput }]}
            value={normVal.max.toString()}
          />
        </View>

        {/* -------------------------  -------------------------  -------------------------    적용 버튼 .. ... */}
        <View style={[styl.rowContainer, { flex: 5 }]}>
          <Text style={{ flex: flxTitle, textAlign: 'right' }} >센서 값 : </Text>
          <TouchableHighlight
            style={[styl.buttonBase, sty.bttnExtra, { flex: flxInput }]}
            underlayColor={HtColor.bttnTouchableHighlightUnderlayColor}
            onPress={this.props.onPressMinCallback}
          >
            <Text style={[styl.buttonLabel]} >센서값을 {'\n'} 최소값으로 적용</Text>
          </TouchableHighlight>
          <Text style={[sty.txtGeneral, { flex: flxTilde }]} >~</Text>
          <TouchableHighlight
            style={[styl.buttonBase, sty.bttnExtra, { flex: flxInput }]}
            underlayColor={HtColor.bttnTouchableHighlightUnderlayColor}
            onPress={this.props.onPressMaxCallback}
          >
            <Text style={[styl.buttonLabel]} >센서값을 {'\n'} 최대값으로 적용</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
// underlayColor={'#CCDDEE'}
}


const sty = StyleSheet.create({
  txtInput: {
    alignSelf: 'stretch',
    textAlign: 'center',
  },
  txtGeneral: {
    textAlign: 'center',
    fontSize: 12,
    //alignItems: 'stretch',alignSelf: 'center',justifyContent: 'center',backgroundColor: '#34E',
  },
  bttnExtra: {
    margin: 5,
    marginLeft: 15,
    marginRight: 15,
  }

});


export { HtMimaxView };
