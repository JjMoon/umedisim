/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtRectBttn.js
 *
 * Created by Jongwoo Moon on 2017. 1. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

class HtRectBttn extends Component {
  constructor(props) {
    super(props);
    this.state = { // 변하거나 default 값을 줄 때는 상태변수에 재 할당 함..
      flex: this.props.flex || 1,
      padding: this.props.padding || 3,
      margin: this.props.margin || 5,
    };
  }

  render() {
    const { ratio = 1, len } = this.props;
    const width = len * ratio, height = len;
    return (
      <TouchableOpacity
        style={[sty.buttonBase, {
          flex: this.props.flex,
          padding: this.state.padding,
          margin: this.state.margin,
          backgroundColor: this.props.bgCol,
          width, height,
          borderRadius: this.props.borderR }]}
        underlayColor={'#CCDDEE'}
        onPress={this.props.onPressCallback}
      >
        <Text
          style={[sty.buttonLabel, {
            color: this.props.txtCol,
            textAlign: 'center',
            fontSize: this.props.len * this.props.txtRatio
          }]}
        >
          {this.props.ptext}
        </Text>
      </TouchableOpacity>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  buttonBase: {
    justifyContent: 'center',
    padding: 3,
    margin: 5,
    borderRadius: 10,
  },
  buttonLabel: {
    fontWeight: '400',
    textAlign: 'center',
    //alignSelf: 'center',
    //alignItems: 'center',
  },
});


export { HtRectBttn };
