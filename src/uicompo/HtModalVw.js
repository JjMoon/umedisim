/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtModalVw.js
 *
 * Created by Jongwoo Moon on 2017. 3. 21..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity } from 'react-native';
import _ from 'lodash';

const lang = require('../data/uicommon.json');

/*
  << props >>
    flex : 전체 뷰의 flex 값.
    rad  : 코너 Radius Text margin 도 같이 주어야 함.
    value, onChange

  << 사용 예 >>
    <HtInput
      mandator={{
        flex: 5, rad: 5, value: shr,
        onChange: (text) => this.setState({ shr: text })
      }}
      style={{ input: { fontSize: 17 }, border: sty.grayBoarder }}
    />
*/

///-------------------------  UI Components  -------------------------  버튼..
class HtModalVw extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: props.modalVisible,
      offAction: false
    };
  }

  renderContent() {
    const { content, flx } = this.props;

    return (
      <View style={[sty.contentContainer, { flex: flx.fWid }]} >

        {this.renderTitle()}

        <Text style={[sty.textBase, { flex: flx.vContent }]}>
        {content}
        </Text>

        <View style={sty.seperateLine} />

        {this.renderButtons()}
      </View>
    );
  }

  renderTitle() {
    const { title, flx } = this.props;
    if (title === undefined) {
      return (null);
    }
    return (
      <Text style={[sty.title, { flex: flx.vTitle }]}>
        {title}
      </Text>
    );
  }

  renderButtons() {
    const { arrBttn, flx } = this.props;

    const langObj = lang.kr;

    if (arrBttn === undefined) {
      return (
        <View style={[sty.bttnContainer, { flex: flx.vBttn }]}>
          <TouchableOpacity
            style={sty.touchable}
            onPress={() => { this.setState({ offAction: true }); }} // 끄기..
          >
            <Text style={{ flex: 1 }} >{langObj.ok}</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <View style={[sty.bttnContainer, { flex: flx.vBttn }]}>
        {arrBttn.map(this.renderButton.bind(this))
        }
      </View>
    );
  }

  renderButton(param, idx) {
    const { title, action } = param;
    return (
      <TouchableOpacity
        key={`${param.id}_${idx}`}
        style={sty.touchable}
        onPress={() => {
          this.setState({ offAction: true });
          action();
        }}
      >
        <Text style={sty.bttnTxt} >{title}</Text>
      </TouchableOpacity>
    );
  }

  // onPress={function() {
  //   this.setState({ modalVisible: false });
  //   action();
  // }.bind(this) }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { content, flx, modalVisible } = this.props;
    // const { flex, rad, value, onChange, margin } = this.props.mandator;
    // const { t = 0, b = 0, l = 0, r = 0 } = margin;
    // const { input, border } = this.props.style;


    const visible = modalVisible && !this.state.offAction;


    return (
      <Modal
          animationType={'slide'}
          transparent
          visible={visible}
      >
        <View style={{ backgroundColor: '#3334', flex: 10 }} >
          <View style={{ flex: 10 }} />

          <View style={[sty.container, { flex: flx.fHgt }]}>
            <View style={[sty.rowContainer, { flex: 3 }]}>
              <View style={{ flex: 10 }} />

              {this.renderContent()}

              <View style={{ flex: 10 }} />
            </View>
          </View>

          <View style={{ flex: 10 }} />
        </View>
      </Modal>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    justifyContent: 'center', alignItems: 'center',
  },
  rowContainer: { flexDirection: 'row', alignSelf: 'stretch' },
  contentContainer: {
    justifyContent: 'center', alignItems: 'center',
    backgroundColor: '#F4FfFf',
    borderRadius: 10,
  },
  bttnContainer: {
    justifyContent: 'center', alignItems: 'center',
    flexDirection: 'row'

  },
  textBase: {
    flex: 1,
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'stretch',
    margin: 3,
    color: '#345',
    fontSize: 20
  },
  title: {
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'stretch',
    margin: 5, marginTop: 20,
    color: '#123',
    fontSize: 23
  },
  seperateLine: { alignSelf: 'stretch', height: 1, backgroundColor: '#AAA', marginLeft: 10, marginRight: 10 },
  touchable: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20
    //alignItems: 'center',
    //borderWidth: 1, borderColor: '#EEAAEE', // 경계선.
    //borderRadius: 5,    backgroundColor: '#EDEFED',
    //padding: 3, //marginTop: 10, margin: 5,

  },
  bttnTxt: {
    flex: 1,
    fontWeight: '400',
    alignSelf: 'center',
    alignItems: 'stretch',
    color: '#0000FF',
    fontSize: 18
  },
});

export { HtModalVw };
