/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / uicompo / HtInput.js
 *
 * Created by Jongwoo Moon on 2017. 3. 9..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';
import React, { Component } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { HtColor } from './HtColor';

/*
  << props >>
    flex : 전체 뷰의 flex 값.
    rad  : 코너 Radius Text margin 도 같이 주어야 함.
    value, onChange

  << 사용 예 >>
    <HtInput
      mandator={{
        flex: 5, rad: 5, value: shr, keyboardType
        onChange: (text) => this.setState({ shr: text })
      }}
      style={{ input: { fontSize: 17 }, border: sty.grayBoarder }}
    />
*/

///-------------------------  UI Components  -------------------------  버튼..
class HtInput extends Component {
  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { keyboardType, multiline = true, mandator } = this.props;
    const { flex, rad, value, onChange, margin, editable } = mandator;

    let { t = 0, b = 0, l = 0, r = 0, lr = 0, tb = 0, all = 0 } = margin;
    if (lr > 0) { l = r = lr; }
    if (tb > 0) { t = b = tb; }
    if (all > 0) { l = r = b = t = all; tb = lr = all = 0; }
    const { input, border } = this.props.style;

    return (
      <TextInput
        style={[sty.textBase, input, border,
          {
            flex, borderRadius: rad,
            marginTop: t, marginBottom: b, marginLeft: l, marginRight: r
          }]}
        editable={editable}
        multiline={multiline}
        keyboardType={keyboardType}
        onChangeText={onChange}
        blurOnSubmit={false}
        value={value}
      />
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  // container: {
  //   justifyContent: 'center', alignItems: 'center',
  //   flexDirection: 'row' // 상하 방향의 중앙에 넣기 위해서.
  // },
  textBase: {
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'stretch',
    //margin: 3,
    backgroundColor: '#FFF',
    color: '#345',
    fontSize: 14
  },
});

export { HtInput };
