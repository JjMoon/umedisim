/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/bleumen.git
 *
 * src / uicompo / HtSpinner.js
 *
 * Created by Jongwoo Moon on 2017. 1. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const HtSpinner = ({ size }) => {
  return (
    <View style={styles.spinnerStyle}>
      <ActivityIndicator size={size || 'large'} />
    </View>
  );
};

const styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export { HtSpinner };
