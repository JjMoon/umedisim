/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpSetting / TrainingSetting.js
 *
 * Created by Jongwoo Moon on 2017. 2. 14..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, Picker, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtText, HtButton, HtPicker, HtColor, HtInput } from '../uicompo';
import BPSettingTitleBar from './TitleBar';

const langBpSetting = require('../data/langBpSetting.json');
const Item = Picker.Item;

class TrainingSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      method: 0, pulseSense: 1, soundSense: 1, period: 1,
      examTime: 3, pressStandard: 100,
    };

    this.retriveData();
  }

  componentWillUnmount() {
    this.saveCurrentState();
    this.props.setBpSetting({ train: this.state });
  }

  ////////////////////////////////////////////////////   _//////////////////_   SAVE
  saveCurrentState() {
    const str = JSON.stringify(this.state);
    console.log(` TrainSetting :: save :: ${str}`);
    this.saveData('bpTrainSetting', str);
  }

  async saveData(ky, content) {
    console.log(`\n\n Save Data ::  ${ky} : ${content}`);
    try {
      await AsyncStorage.setItem(ky, content);
    } catch (error) {
      console.log('save error');
    }
  }

  async retriveData() {
    try {
      const value = await AsyncStorage.getItem('bpTrainSetting');  // 읽어오기
      if (typeof value === 'string') {
        const sObj = JSON.parse(value);
        if (_.isObject(sObj)) {
          this.setState(sObj);
        }
      } else {
        console.log('\n\n retriveData :: 기존 기록 없슴.  새로 쓰기..');
        this.saveCurrentState();
      }
    } catch (error) {
      console.log('value not found');
    }
  }

  renderInputbox(title, unitStr) {
    const { uicommon, langKey } = this.props.uiState;
    const { addManualItem } = uicommon[langKey];

    return (
      <View style={{ flex: 1 }}>
        <Text>{title}</Text>
        <HtButton
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: '#208'
          }}
          width={100} fWidth={1} ptext={addManualItem}
          onPressCallback={() => {
            this.setState({ expanded: false });
          }}
        />
      </View>
    );
  }


  // 실습시간, 가압 기준 입력 유닛뷰..
  renderHorColumn(title, content, textChanged, margin = 10) {
    const { unit } = this.props.main.scr;
    const vwSty = { margin: 3 };
    const txtSty = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 2.5 * unit };

    return (
      <View style={{ flex: 5, flexDirection: 'row', margin, padding: 5 }} key={Math.random()} >
        <HtText
          mandator={{ flex: 3, rad: 5, text: title, margin: { r: 10 }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: txtSty }}
        />
        <HtInput
          mandator={{
            flex: 3, rad: 5, value: content, margin: { },
            onChange: textChanged
          }}
          keyboardType={'numeric'} // 'decimal-pad'
          multiline={false}
          style={{ input: { fontSize: 3 * unit }, border: sty.grayBoarder }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState;
    const { selectMethod, touch, hearing, combine, pulseSensitivity, examTime: examTimeStr, pressStandard: pressStandardStr,
      soundSensitivity, checkperiod, on, random, off } = langBpSetting[langKey];
    const { languageSetting } = uicommon[langKey];
    const { method, pulseSense, soundSense, period, langIdx, examTime, pressStandard } = this.state;
    const ttlFntSz = 25;

    return (
      <View style={{ marginTop: 1, flex: 80, backgroundColor: '#FFEFFF' }}>

        <View style={{ flex: 3 }} comment={' Empty View >>>>>>>>>>>>> '} />
        <View style={{ flex: 7, flexDirection: 'row' }} >

          {this.renderHorColumn(examTimeStr, `${examTime}`,
            (val) => this.setState({ examTime: Number(val) }))}
          {this.renderHorColumn(pressStandardStr, `${pressStandard}`,
            (val) => this.setState({ pressStandard: Number(val) }))}

        </View>

        <View
          style={{
            flex: 30, flexDirection: 'row', margin: 20,
            borderWidth: 1, borderRadius: 5, borderColor: '#333'
          }}
        >
          <HtPicker
            mandator={{
              flex: 10, title: selectMethod, ttlFontSize: ttlFntSz, selected: method,
              onValueChange: (key) => { this.setState({ method: key }); },
              pickerItems: [
                <Item label={touch} value={0} key={1} />,
                <Item label={hearing} value={1} key={2} />,
                <Item label={combine} value={2} key={3} />,
              ]
            }}
          />
          {/* -------------------------  -------------------------  분리선.. ... */}
          <View style={sty.seperateLine} />
          {/* -------------------------  -------------------------  분리선.. ... */}
          <HtPicker
            mandator={{
              flex: 10, title: pulseSensitivity, ttlFontSize: ttlFntSz, selected: pulseSense,
              onValueChange: (key) => { this.setState({ pulseSense: key }); },
              pickerItems: [
                <Item label={'1'} value={1} key={1} />,
                <Item label={'2'} value={2} key={2} />,
                <Item label={'3'} value={3} key={3} />,
              ]
            }}
          />
          {/* -------------------------  -------------------------  분리선.. ... */}
          <View style={sty.seperateLine} />
          {/* -------------------------  -------------------------  분리선.. ... */}
          <HtPicker
            mandator={{
              flex: 10, title: soundSensitivity, ttlFontSize: ttlFntSz, selected: soundSense,
              onValueChange: (key) => { this.setState({ soundSense: key }); },
              pickerItems: [
                <Item label={'1'} value={1} key={Math.random()} />,
                <Item label={'2'} value={2} key={Math.random()} />,
                <Item label={'3'} value={3} key={Math.random()} />,
                <Item label={'4'} value={4} key={Math.random()} />,
                <Item label={'5'} value={5} key={Math.random()} />,
              ]
            }}
          />

          {/* -------------------------  -------------------------  분리선.. ... */}
          <View style={sty.seperateLine} />
          {/* -------------------------  -------------------------  분리선.. ... */}
          <HtPicker
            mandator={{
              flex: 10, title: checkperiod, ttlFontSize: ttlFntSz, selected: period,
              onValueChange: (key) => { this.setState({ period: key }); },
              pickerItems: [
                <Item label={on} value={1} key={Math.random()} />,
                <Item label={random} value={2} key={Math.random()} />,
                <Item label={off} value={3} key={Math.random()} />
              ]
            }}
          />
        </View>

        <View style={{ flex: 10 }} comment={' Empty View >>>>>>>>>>>>> '} />

      </View>
    );
  }
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { backgroundColor: HtColor.blueGray, margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
  seperateLine: { width: 1, margin: 10, backgroundColor: '#999' },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    chkItems: state.chkItems,
    theCounter: state.timerCount,
    uiState: state.uiState
    //arrPacket: state.arrPacket,
  };
};

export default connect(mapStateToProps, actions)(TrainingSetting);
