/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpSetting / RealTestSetting.js
 *
 * Created by Jongwoo Moon on 2017. 2. 14..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, Picker, AsyncStorage } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtText, HtButton, HtInput, HtPicker, HtColor } from '../uicompo';

const langBpSetting = require('../data/langBpSetting.json');
const Item = Picker.Item;

class RealTestSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bpLevel: 0, method: 0, pulseSense: 1, soundSense: 1, period: 1, // picker related...
      bloodShrink: 10, bloodRelax: 10, pulseNum: 5,
      examTimeMin: 3, pressStandard: 5,
      arrScore: [10, 10, 10, 10, 10, 10, 10, 10, 10]
    };
    this.retriveData();
  }

  componentWillUnmount() {
    this.saveCurrentState();
    this.props.setBpSetting({ real: this.state });
  }

  ////////////////////////////////////////////////////   _//////////////////_   SAVE
  saveCurrentState() {
    const str = JSON.stringify(this.state);
    console.log(` RealTestSetting :: save :: ${str}`);
    this.saveData('bpRealTestSetting', str);
  }

  async saveData(ky, content) {
    console.log(`\n\n Save Data ::  ${ky} : ${content}`);
    try {
      await AsyncStorage.setItem(ky, content);
    } catch (error) {
      console.log('save error');
    }
  }
  async retriveData() {
    try {
      const value = await AsyncStorage.getItem('bpRealTestSetting');  // 읽어오기
      if (typeof value === 'string') {
        const sObj = JSON.parse(value);
        if (_.isObject(sObj)) {
          console.log(sObj);
          this.setState(sObj);
        }
      } else {
        console.log('\n\n retriveData :: 기존 기록 없슴.  새로 쓰기..');
        this.saveCurrentState();
      }
    } catch (error) {
      console.log('value not found');
    }
  }

  renderItemTitle() {
    const { uicommon, langKey } = this.props.uiState;
    const { addManualItem } = uicommon[langKey];

    return (
      <View style={{ flex: 1 }}>
        <Text> total score </Text>
        <HtButton
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: '#208'
          }}
          width={100} fWidth={1} ptext={addManualItem}
          onPressCallback={() => {
            this.setState({ expanded: false });
          }}
        />
      </View>
    );
  }

  renderHorColumn(title, content, textChanged, margin = 10) {
    const { unit } = this.props.main.scr;
    const vwSty = { margin: 3 };
    const txtSty = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 2.5 * unit };

    return (
      <View style={{ flex: 5, flexDirection: 'row', margin, padding: 5 }} key={Math.random()} >
        <HtText
          mandator={{ flex: 3, rad: 5, text: title, margin: { r: 10 }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: txtSty }}
        />
        <HtInput
          mandator={{
            flex: 3, rad: 5, value: content, margin: { },
            onChange: textChanged
          }}
          keyboardType={'numeric'} // 'decimal-pad'
          multiline={false}
          style={{ input: { fontSize: 3 * unit }, border: sty.grayBoarder }}
        />
      </View>
    );
  }

  renderScore() {
    const { unit } = this.props.main.scr;
    const { langKey } = this.props.uiState;
    const { scoreSetting, totalScore, scoreSetList } = langBpSetting[langKey];
    const { arrScore } = this.state;

    const vwSty = { margin: 3 }, vwMargin = 3;
    const ttlSty = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 3 * unit };

    const arrCompo = _.map(_.range(8), (n) => {
      return (this.renderHorColumn(scoreSetList[n], `${arrScore[n]}`,
        (val) => {
          console.log(isNaN(Number(val)));
          if (isNaN(Number(val))) { // 숫자로 파싱 ..
            this.setState({ arrScore: _.fill(arrScore, 10, n, n + 1) }); // 파싱 안되면..
          } else {
            this.setState({ arrScore: _.fill(arrScore, Number(val), n, n + 1) });
            return;
            //this.setState({ arrScore: _.fill(arrScore, 0, n, n + 1) });
          }
        }
        , vwMargin));
    });

    return (
      <View style={{ flex: 25 }} >

      <HtText
        mandator={{ flex: 5, rad: 5, text: scoreSetting, margin: { r: 10 }, radIgnore: true }}
        style={{ viewStyle: vwSty, textStyle: ttlSty }}
      />

      {arrCompo /*  배점...   */}

      <HtText
        mandator={{ flex: 5, rad: 5, text: totalScore, margin: { r: 10 }, radIgnore: true }}
        style={{ viewStyle: vwSty, textStyle: ttlSty }}
      />

      </View>
    );
  }

  renderPickers() {
    const { uicommon, langKey } = this.props.uiState;
    const { bpLevels, highBP, normalBP, lowBP,
      selectMethod, touch, hearing, combine, pulseSensitivity,
      soundSensitivity, checkperiod, on, random, off } = langBpSetting[langKey];
    const { bpLevel, method, pulseSense, soundSense, period } = this.state;
    const ttlFntSz = 20;

    return (
      <View
        style={{
          flex: 10, flexDirection: 'row', margin: 10,
          //borderWidth: 1, borderRadius: 5, borderColor: '#333'
        }}
      >
        <HtPicker
          mandator={{
            flex: 10, title: bpLevels, ttlFontSize: ttlFntSz, selected: bpLevel,
            onValueChange: (key) => { this.setState({ bpLevel: key }); },
            pickerItems: [
              <Item style={{ fontSize: 10 }} label={highBP} value={0} key={Math.random()} />,
              <Item label={normalBP} value={1} key={Math.random()} />,
              <Item label={lowBP} value={2} key={Math.random()} />,
            ]
          }}
        />
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLineVer} />

        <HtPicker
          mandator={{
            flex: 13, title: selectMethod, ttlFontSize: ttlFntSz, selected: method,
            onValueChange: (key) => { this.setState({ method: key }); },
            pickerItems: [
              <Item label={touch} value={0} key={Math.random()} />,
              <Item label={hearing} value={1} key={Math.random()} />,
              <Item label={combine} value={2} key={Math.random()} />,
            ]
          }}
        />
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLineVer} />

        <HtPicker
          mandator={{
            flex: 10, title: pulseSensitivity, ttlFontSize: ttlFntSz, selected: pulseSense,
            onValueChange: (key) => { this.setState({ pulseSense: key }); },
            pickerItems: [
              <Item label={'1'} value={1} key={1} />,
              <Item label={'2'} value={2} key={2} />,
              <Item label={'3'} value={3} key={3} />,
            ]
          }}
        />
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLineVer} />
        {/* -------------------------  -------------------------  분리선.. ... */}
        <HtPicker
          mandator={{
            flex: 10, title: soundSensitivity, ttlFontSize: ttlFntSz, selected: soundSense,
            onValueChange: (key) => { this.setState({ soundSense: key }); },
            pickerItems: [
              <Item label={'1'} value={1} key={Math.random()} />,
              <Item label={'2'} value={2} key={Math.random()} />,
              <Item label={'3'} value={3} key={Math.random()} />,
              <Item label={'4'} value={4} key={Math.random()} />,
              <Item label={'5'} value={5} key={Math.random()} />,
            ]
          }}
        />

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLineVer} />
        {/* -------------------------  -------------------------  분리선.. ... */}
        <HtPicker
          mandator={{
            flex: 10, title: checkperiod, ttlFontSize: ttlFntSz, selected: period,
            onValueChange: (key) => { this.setState({ period: key }); },
            pickerItems: [
              <Item label={on} value={1} key={Math.random()} />,
              <Item label={random} value={2} key={Math.random()} />,
              <Item label={off} value={3} key={Math.random()} />
            ]
          }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState;
    const { bloodShr, bloodRlx, pulseNm, examTime, pressStandard: pressStandardStr } = langBpSetting[langKey];

    const { bloodShrink, bloodRelax, pulseNum, examTimeMin, pressStandard } = this.state;

    return (
      <View style={{ flex: 80, flexDirection: 'row', backgroundColor: '#FFE' }}>

        <View style={{ flex: 60 }} >

          {this.renderPickers()}

          {/* -------------------------  -------------------------  분리선.. ... */}
          <View style={sty.seperateLine} />
          {/* -------------------------  -------------------------  분리선.. ... */}

          <View style={{ flex: 10, flexDirection: 'row' }}>

            <View style={{ flex: 5 }} >
              {this.renderHorColumn(bloodShr, `${bloodShrink}`,
                (val) => this.setState({ bloodShrink: Number(val) }))}
              {this.renderHorColumn(bloodRlx, `${bloodRelax}`,
                (val) => this.setState({ bloodRelax: Number(val) }))}
              {this.renderHorColumn(pulseNm, `${pulseNum}`,
                (val) => this.setState({ pulseNum: Number(val) }))}
            </View>

            <View style={{ flex: 5 }} >
              <View style={{ flex: 3 }} comment={' 공간 확보 용'} />
              {this.renderHorColumn(examTime, `${examTimeMin}`,
                (val) => this.setState({ examTimeMin: Number(val) }))}
              {this.renderHorColumn(pressStandardStr, `${pressStandard}`,
                (val) => this.setState({ pressStandard: Number(val) }))}
              <View style={{ flex: 3 }} comment={' 공간 확보 용'} />

            </View>

          </View>

        </View>

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLineVer} />

        {this.renderScore()}



      </View>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { backgroundColor: HtColor.blueGray, margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    borderWidth: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
  seperateLine: { height: 1, backgroundColor: '#999', margin: 10 },
  seperateLineVer: { width: 1, margin: 10, backgroundColor: '#999' },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    chkItems: state.chkItems,
    theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(RealTestSetting);
