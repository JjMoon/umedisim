/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneBpSetting / BpSettingMain.js
 *
 * Created by Jongwoo Moon on 2017. 3. 28..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtText, HtButton, HtColor } from '../uicompo';
import BPSettingTitleBar from './TitleBar';
import RealTestSetting from './RealTestSetting';
import TrainingSetting from './TrainingSetting';
import * as M from '../model';
/*
    로컬 저장소에서 읽고, 나갈 때 쓰자.
    데이터 객체가 리듀서에 있어야 함.
    enterSetting
    getoutofSetting
*/

const mdl = new M.SettingModule();

class BpSettingMain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      realTestMode: true
    };
  }

  componentWillMount() {
    this.props.changeMainState('init');
  }

  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ BpSettingMain :: unmount ]] ...\n');
  }

  renderItemTitle() {
    const { uicommon, langKey } = this.props.uiState;
    const { addManualItem } = uicommon[langKey];

    return (
      <View style={{ flex: 1 }}>
        <Text> total score </Text>
        <HtButton
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: '#208'
          }}
          width={100} fWidth={1} ptext={addManualItem}
          onPressCallback={() => {
            this.setState({ expanded: false });
          }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState;
    const { settingTraing, settingRealTest } = uicommon[langKey];

    const { realTestMode } = this.state;

    const title = realTestMode ? settingRealTest : settingTraing;
    const subTitle = realTestMode ? settingTraing : settingRealTest;

    const mainView = realTestMode ? (<RealTestSetting />) : <TrainingSetting />;

    console.log(realTestMode);

    return (
      <View style={{ marginTop: 1, flex: 100 }}>

        <BPSettingTitleBar
          flex={10}
          titleA={title}
          titleB={subTitle}
          back={'go back'}
          callback={() => { this.setState({ realTestMode: !realTestMode }); }}
        />

        {mainView}

        <View style={{ flex: 10, backgroundColor: '#5DF' }} />
      </View>
    );
  }
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { backgroundColor: HtColor.blueGray, margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    chkItems: state.chkItems,
    theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(BpSettingMain);
