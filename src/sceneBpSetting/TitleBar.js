/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpSetting / TitleBar.js
 *
 * Created by Jongwoo Moon on 2017. 3. 28..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtButton, HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen } = HtColor;

class BPSettingTitleBar extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ BPSettingTitleBar :: constructor ]] ');
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { titleA, titleB, flex, back, callback } = this.props;
    const smallButtonFlex = 5;

    return (
        <View style={{ flex, backgroundColor: 'dodgerblue' }}>
          <View style={[styl.rowContainer, { flex: 1, padding: 12 }]}>
            <TouchableOpacity
              style={{
                flex: smallButtonFlex,
                flexDirection: 'row', alignSelf: 'flex-end' // 글자 밑으로 정렬.
              }}
              onPress={Actions.pop}
            >
              <Text style={[sty.miniTitle, { textAlign: 'left', flex: 1, marginLeft: 20 }]}>
                {back}
              </Text>
            </TouchableOpacity>

            <Text style={[sty.title, { flex: 10 }]}>
              {titleA}
            </Text>

            <TouchableOpacity
              style={{
                flex: smallButtonFlex,
                flexDirection: 'row', alignSelf: 'flex-end' // 글자 밑으로 정렬.
              }}
              onPress={callback}
            >
              <Text style={[sty.miniTitle, { textAlign: 'right', flex: 1, marginRight: 10 }]}>
                {titleB}
              </Text>
            </TouchableOpacity>
          </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    alignSelf: 'flex-end',
    color: bttnBrightTextGreen,
    fontSize: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  miniTitle: {
    alignSelf: 'center', textAlign: 'center',
    color: bttnBrightTextGreen,
    fontSize: 18,
    fontWeight: '800',
    marginBottom: 5
  }
});


const mapStateToProps = (state) => {
  return {
    //main: state.main,
    //theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(BPSettingTitleBar);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
