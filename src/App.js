import React, { Component } from 'react';
// import { View, Text } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
// import firebase from 'firebase';
// import { Header, Button, Spinner } from './components/common';
// import LoginForm from './components/LoginForm';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import SceneMain from './sceneMain/mainView';
import SceneOperate from './sceneOper/mainView';
import SceneExams from './sceneExams/examView';
import SceneSetting from './sceneSetting/mainView';
import GlobalSettingMainView from './sceneGlobalSetting/GlobalSettingMainView';
import SceneBpOper from './sceneBpOper/BPOperMain';
import SceneBpSetting from './sceneBpSetting/BpSettingMain';
import SceneReport from './sceneReport/MainReport';
import SceneBpMeasureInput from './sceneBpResult/MainMeasureInput';
import SceneBpResult from './sceneBpResult/ResultView';
import SceneReports from './sceneReportsView/ReportsMainView';

/** */
class App extends Component {
  state = { loggedIn: null };

  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <Router>
          <Scene key="root">
            <Scene
              key="sceneMain" component={SceneMain} title="PageOne" initial hideNavBar
            />
            <Scene
              key="sceneOperate" component={SceneOperate} title="PageTwo"
              hideNavBar
            />
            <Scene
              key="sceneSetting" component={SceneSetting} title="Setting"
              hideNavBar
            />
            <Scene
              key="sceneExams" component={SceneExams} title="Examinations"
              hideNavBar
            />
            <Scene
              key="sceneBpOper" component={SceneBpOper} title="BP Operation"
              hideNavBar
            />
            <Scene
              key="sceneBpSetting" component={SceneBpSetting} title="BP Setting"
              hideNavBar
            />
            <Scene
              key="sceneReportView" component={SceneReport} title="Report"
              hideNavBar
            />
            <Scene
              key="sceneBpMeasureInput" component={SceneBpMeasureInput} title="Measure Input"
              hideNavBar
            />
            <Scene
              key="sceneBpResultView" component={SceneBpResult} title="Measure Input"
              hideNavBar
            />
            <Scene
              key="sceneGlobalSetting" component={GlobalSettingMainView} title="Global Setting"
              hideNavBar
            />
            <Scene
              key="sceneReports" component={SceneReports} title="Global Setting"
              hideNavBar
            />


          </Scene>
        </Router>
      </Provider>
    );
  }
}

export default App;

/*
<Scene
  onLeft={() => alert('Left button!')} leftTitle="Left"  // 왼쪽 커스텀 버튼...
  direction="vertical" // 아래에서 올라옴..
/>
*/
