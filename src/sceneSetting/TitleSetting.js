/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / TitleSetting.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen, bgBlue } = HtColor;

class TitleSetting extends Component {
  constructor(props) {
    super(props);
    //this.props.setViewLayout('imageLeft'); // default
    console.log('\n ====== ====== ====== ======  [[ TitleSetting :: constructor ]] ');
  }

  render() {
    const { height } = this.props.main.scr;
    const { hPercent, title } = this.props;
    return (
        <View style={{ backgroundColor: bgBlue, height: (height * hPercent * 0.01) }}>
          <TouchableOpacity style={{ flex: 5, marginLeft: 20 }} onPress={Actions.pop}>
            <Text style={[sty.title, { flex: 30 }]}>
              {title}
            </Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    color: bttnBrightTextGreen,
    fontSize: 25,
    fontWeight: '600',
    textAlign: 'left',
    margin: 10,
    marginTop: 35,
    alignSelf: 'stretch'
  },
  miniTitle: {
    color: bttnBrightTextGreen,
    fontSize: 18,
    fontWeight: '800',
    textAlign: 'center',
    margin: 5, marginTop: 45,
    alignSelf: 'stretch'
  }
});


const mapStateToProps = (state) => {
  return {
    main: state.main,
    //theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(TitleSetting);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
