/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / SettingGeneral.js
 *
 * Created by Jongwoo Moon on 2017. 3. 9..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { StyleSheet, ListView, View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtColor } from '../uicompo';
import SettingGenCell from './SettingGenCell';

/** // 실행 화면에서 실습 중에 체크하는 리스트 뷰.. */
class SettingGeneral extends Component {
  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    this.time = new Date();
    this.state = { // 기본 값과 함께 사용자가 추가한 항목도 가져와야 함.
      dataSource: ds.cloneWithRows(this.props.EttSetting.general),
    };
  }

  getDataSource(datum) {
    return this.state.dataSource.cloneWithRows(datum);
  }

  renderRow(dt) {
    return (
      <SettingGenCell
        item={dt}
        // {/*  redraw={this.redraw.bind(this)}    자동 닫침 안됨..  */}
      />
    );
  }

  renderRowOld(dt) {
    return (
      <View style={{ flex: 1 }}>
        <Text style={{ textAlign: 'center' }}>{dt.shr}</Text>
      </View>
    );
  }

  render() {
    this.time = new Date();
    return (
      <ListView
        style={{ flex: this.props.flex, alignSelf: 'stretch' }}
        enableEmptySections
        dataSource={this.getDataSource(this.props.EttSetting.general)}
        renderRow={this.renderRow.bind(this)}
      />
    );
  }
}


// /-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#EEEAE5',
    margin: 2,
    borderRadius: 10,
    height: 75,
  },
  rowContainer: {
    flexDirection: 'row', alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  roundVw: {
    // flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
    borderRadius: 5,
    paddingTop: 5,
  },
  genText: {
    fontWeight: '400', textAlign: 'center',
    fontSize: 15,
    padding: 10,
  },
  title: {
    fontWeight: '900',
    fontSize: 18,
    color: '#345',
    textAlign: 'center',
  },
  boxKind: {
    textAlign: 'center',
    color: '#EEC',
    fontWeight: '300',
    fontSize: 14,
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', // (#dcdcdc)
    borderRadius: 5,
  },
  styText: {
    flex: 1, alignSelf: 'stretch',
    textAlign: 'center', color: '#252',
    paddingTop: 25,
    // backgroundColor: '#DDD',
    fontWeight: '300', fontSize: 16,
  },
  inputTxt: {
    alignSelf: 'stretch',
    margin: 10, marginTop: 20, marginBottom: 20,
    textAlign: 'center', color: '#133',
    fontWeight: '300', fontSize: 18,
    backgroundColor: 'white',
    borderWidth: 2, borderColor: '#469',
    borderRadius: 8,
  },
});

const mapStateToProps = state => ({
  main: state.main,
  EttSetting: state.EttSetting,
  chkItems: state.chkItems,
});

export default connect(mapStateToProps, actions)(SettingGeneral);


// ////////////////////////////////////////////////////   _//////////////////_   render
// renderA() {
//   const { title, explain } = this.state;
//   const { range, value } = this.props.item;
//
//   const valueArea = range === undefined ? this.renderValue(value) : this.renderRange();
//   const typeStr = range === undefined ? '최소값 설정' : '범위 설정';
//   return (
//     <View style={[sty.container]}>
//       <View style={[sty.rowContainer, { flex: 5 }]}>
//         <Text style={[sty.title, { flex: 8 }]}>{title}</Text>
//         <View style={[sty.roundVw, { flex: 3 }]}>
//           <Text style={[sty.boxKind, { flex: 3 }]}>{typeStr}</Text>
//         </View>
//         {valueArea}
//       </View>
//
//       <View style={[sty.grayBoarder, { flex: 4 }]}>
//         <Text style={[sty.genText, { flex: 1 }]}>{explain}</Text>
//       </View>
//     </View>
//   );
// } // onChangeText={(text) => this.changeName(text)}
