/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneSetting / mainView.js
 *
 * Created by Jongwoo Moon on 2017. 2. 14..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
// import Popup from 'react-native-popup';
import Toast, { DURATION } from 'react-native-easy-toast';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import _ from 'lodash';
import * as actions from '../actions';
import TitleSetting from './TitleSetting';
import CheckItemSetting from './CheckItemSetting';
import SettingGeneral from './SettingGeneral';
import GlobalItems from './GlobalItems';
import { HtText, HtButton, HtColor } from '../uicompo';
import * as M from '../model';

/*
    로컬 저장소에서 읽고, 나갈 때 쓰자.
    데이터 객체가 리듀서에 있어야 함.
    enterSetting
    getoutofSetting
*/
const mdl = new M.SettingModule();

/** */
class sceneSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentWillMount() {
    this.props.changeMainState('init');
  }

  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ sceneSettimg :: unmount ]] ...\n');

    this.saveCurrentState();
  }

  // //////////////////////////////////////////////////   _//////////////////_   SAVE
  saveCurrentState() {
    // const str = JSON.stringify(this.state);
    mdl.saveObject('ettAutoCheckValues', this.props.EttSetting.autoCheckItem);
    mdl.saveObject('ettManualItems', this.props.EttSetting.manCheckItem);
    mdl.saveObject('ettGeneralSet', this.props.EttSetting.general);
  }

  showPopup() {
    this.popup.confirm({
      title: 'title',
      content: 'Are you ready?',
    });
    // this.popup.tip({ content: 'come on!', });
  }

  renderAddItemBttn() {
    const { uicommon, langKey } = this.props.uiState;
    const { addManualItem, addedManualItem } = uicommon[langKey];
    const bttnSty = { margin: 5 };
    return (
      <HtButton
        normal
        f={1} width={100} ptext={addManualItem}
        vwStyle={bttnSty}
        txtStyle={{
          fontWeight: '800', fontSize: 20, color: '#EEE',
        }}
        onPressCallback={() => {
          this.refs.toast.show(addedManualItem);
        }}
      />
    );
  }

  renderItemTitle() { // 총 점 : 99,  수동 항목 추가.
    const { uicommon, langKey } = this.props.uiState;
    const { totalScore, addManualItem, addedManualItem } = uicommon[langKey];
    const txtSty = { color: '#225', fontWeight: '800', fontSize: 20 };
    const bttnSty = { margin: 5 };


    return (
      <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
        <HtText
          mandator={{ flex: 2, rad: 5, text: `${totalScore} : ${this.getTotalScore()}`, margin: { all: 2 } }}
          style={{ viewStyle: sty.vw, textStyle: txtSty }}
        />

      </View>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey } = this.props.uiState;
    const { height } = this.props.main.scr;
    const { setting } = uicommon[langKey];
    const h90per = 0.9 * height;

    return (
      <View style={{ marginTop: 1, flex: 100 }}>
        <TitleSetting
          hPercent={10}
          title={setting}
        />

        <View style={sty.container}>

          {/* 자동 | 매뉴얼 체크 리스트 뷰  */}
          <View style={{ flex: 6, height: h90per, alignSelf: 'stretch', alignItems: 'stretch' }} >
            {this.renderItemTitle()}
            <CheckItemSetting flex={10} />
          </View>

          <View style={{ flex: 5, height: h90per, alignSelf: 'stretch', alignItems: 'stretch' }} >
            {/* 언어 변경 등 고정 뷰.. */}
            <View style={{ flex: 1, alignSelf: 'stretch', alignItems: 'stretch' }} >
              <GlobalItems />
            </View>

            {/* 일반 사항 세팅 리스트 뷰  */}
            <View style={{ flex: 8, alignSelf: 'stretch', alignItems: 'stretch' }} >
              <SettingGeneral />
            </View>
          </View>

        </View>
        <Toast ref="toast" />
      </View>
    );
  }

  getTotalScore() {
    const { autoCheckItem, manCheckItem } = this.props.EttSetting;
    const autoArray = _.map(autoCheckItem, o => o);
    const theList = _.concat(autoArray, manCheckItem);
    const ttlScore = _.chain(theList)
      .filter(o => o.active)
      .map(o => o.score)
      .reduce((sum, n) => sum + n, 0);
    return ttlScore;
  }
}


// /-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    // height: 668,
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40,
  },
  vw: { margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black',
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', // (#dcdcdc)
    borderRadius: 5,
  },
});

const mapStateToProps = state => ({
  main: state.main,
  EttSetting: state.EttSetting,
  chkItems: state.chkItems,
  theCounter: state.timerCount,
  uiState: state.uiState,
  // arrPacket: state.arrPacket,
});

export default connect(mapStateToProps, actions)(sceneSetting);
