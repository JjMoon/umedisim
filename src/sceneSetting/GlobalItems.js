/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / GlobalItems.js
 *
 * Created by Jongwoo Moon on 2017. 3. 24..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { StyleSheet, Picker, View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtText, HtInput, HtColor, HtButton } from '../uicompo';

/**
  언어,
  디바이스 관련 세팅.
  BLE 접속 이름.

*/
class GlobalItems extends Component {
  constructor(props) {
    super(props);
    // const { shr, long, score } = this.props.item;
    this.state = {
      expanded: false,
      language: 0,
    };
  }

  renderLanguagePicker() {
    return (
      <Picker
        style={{ flex: 1 }}
        selectedValue={this.state.language}
        onValueChange={
          (key) => {
            this.setState({ language: key });
            this.props.languageChange(key);
          }
        }
      >
        <Picker.Item label="한국어" value={0} />
        <Picker.Item label="English" value={1} />
      </Picker>
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    // const { shr, long, score, expanded } = this.state;
    // const { auto } = this.props.item;
    // //const autoManCol = auto ? '#FEEAEA' : '#EEEAF5';
    // const autoManCol = auto ? '#FEDADA' : '#DEDAF5';
    // const autoManTxt = auto ? '자동감지' : '수동체크';

    return (
      <View
        style={[sty.container, { height: 20, backgroundColor: '#FEDADA' }]}
      >
        <View style={sty.rowContainer}>
          <HtText
            mandator={{ flex: 3, rad: 5, text: 'Comment', margin: { r: 10 } }}
            style={{ viewStyle: sty.vw, textStyle: sty.txt }}
          />
        </View>

      </View>
    );
  }
}

// /-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    alignItems: 'stretch', justifyContent: 'center',
    borderRadius: 5,
    margin: 4, padding: 3, // padding: 0, paddingLeft: 2, paddingRight: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40,
  },
  vw: { backgroundColor: HtColor.blueGray, marginLeft: 5, marginRight: 5 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black',
  },
  grayBoarder: {
    margin: 2,
    borderWidth: 0.5,
    borderColor: '#888', // (#dcdcdc)
  },
});

const mapStateToProps = state => ({
  main: state.main,
  chkItems: state.chkItems,
});

export default connect(mapStateToProps, actions)(GlobalItems);
