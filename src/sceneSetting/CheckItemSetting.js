/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / CheckItemSetting.js
 *
 * Created by Jongwoo Moon on 2017. 3. 9..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, ListView, View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtText, HtColor } from '../uicompo';
import CheckItemCell from './CheckItemCell';

const autoItems = require('../model/json/autoCheckItems.json');

/// 실행 화면에서 실습 중에 체크하는 리스트 뷰..
class CheckItemSetting extends Component {
  constructor(props) {
    super(props);

    const { manCheckItem } = this.props.EttSetting;

    this.datum = _.concat(autoItems, manCheckItem);
  }

  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.time = new Date();
    this.state = { // 기본 값과 함께 사용자가 추가한 항목도 가져와야 함.
        dataSource: ds.cloneWithRows(this.datum),
    };
  }

  getDataSource(datum) {
     return this.state.dataSource.cloneWithRows(datum);
  }

  renderRow(dt) {
    return (
      <CheckItemCell
        item={dt}
        //{/*  redraw={this.redraw.bind(this)}    자동 닫침 안됨..  */}
      />
    );
  }

  renderRowOld(dt) {
    return (
      <View style={{ flex: 1 }}>
        <Text style={{ textAlign: 'center' }}>{dt.shr}</Text>
      </View>
    );
  }

  render() {
    this.time = new Date();
    const { flex } = this.props;
    return (
      <View style={{ flex }}>
        <ListView
          style={{ flex, alignSelf: 'stretch', }}
          enableEmptySections
          dataSource={this.getDataSource(this.datum)}
          renderRow={this.renderRow.bind(this)}
        />
      </View>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    borderRadius: 5,
    margin: 4, padding: 3, // padding: 0, paddingLeft: 2, paddingRight: 2,
    height: 80
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { backgroundColor: HtColor.blueGray, marginLeft: 5, marginRight: 5 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    margin: 2,
    borderWidth: 0.5,
    borderColor: '#888', //(#dcdcdc)
  },
});

const mapStateToProps = state => {
  return {
    main: state.main,
    EttSetting: state.EttSetting,
    chkItems: state.chkItems,

  };
};

export default connect(mapStateToProps, actions)(CheckItemSetting);


//
// <TextInput
//   style={[sty.title, sty.grayBoarder, { fontSize: 15 }]}
//   onChangeText={(text) => this.setState({ long: text })}
//   value={long}
// />
