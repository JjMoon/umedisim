/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / SettingGeneral.js
 *
 * Created by Jongwoo Moon on 2017. 3. 9..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';

class SettingGenCell extends Component {
  // { "title": "스니핑 위치", "explain": "정상으로 판결되는 바닥으로 부터의 머리의 거리",
  //  "key" : "snippingPositionCm"
  //   "range" : { "min": 5, "max": 15}, "unit": "cm" },

  constructor(props) {
    super(props);
    const { explain, value, range, key } = this.props.item;
    console.log(`====== ====== ====== ======  [[ SettingGenCell ]] ...   ${key}`);
    const { general } = this.props.EttSetting; // 저장된 범위, 최대값 등 객체..
    const valObj = _.filter(general, (o) => { return o.key === key; })[0];
    console.log(valObj);
    //const { calisetting } = this.props.main.setting;
    this.state = {
      explain, value: `${value}`, range,
      key, valObj // :  calisetting[key] // { val: 30 } or { min: 2, max: 5 }
    };
  }

  valueChanged(txt) { // 값 설정 시 액션 함수
    console.log(`value changed ${txt}`);
    const { valObj } = this.state, numVal = Number(txt);
    if (isNaN(numVal)) {
      return;
    }
    this.setState({ valObj: { ...valObj, value: numVal } }); // state 와 리듀싱을 동시에 함.
    this.props.editCaliSetting({ ...this.props.item, value: numVal });
  }

  rangeChanged(minCase, txt) {
    console.log(`range changed  minCase ? :: ${minCase}  value : ${txt}    `);
    const { valObj } = this.state, numVal = Number(txt);
    if (isNaN(numVal)) {
      return;
    }
    const { min, max } = valObj.range;
    const minVal = minCase ? numVal : min, maxVal = minCase ? max : numVal;
    const range = { min: minVal, max: maxVal };
    this.setState({ valObj: { ...valObj, range } }); // state 와 리듀싱을 동시에 함.
    this.props.editCaliSetting({ ...this.props.item, range });
  }

  renderValue() {
    // "value": 1200, "unit": "sec"
    const { valObj } = this.state;
    if (valObj === undefined) {
      return null;
    }
    return (
      <View style={[sty.rowContainer, { flex: 7 }]}>
        <TextInput
          style={[sty.inputTxt, { flex: 5 }]}
          onChangeText={this.valueChanged.bind(this)}
          value={`${valObj.value}`}
        />
        <Text
          style={[sty.styText, { flex: 4, textAlign: 'right', marginRight: 20 }]}
        > {this.props.item.unit}</Text>
      </View>
    );
  }

  // onChangeText={(text) => this.setState({ range: { ...range, min: text } })}
  renderRange() {
    const { min, max } = this.state.valObj.range;
    //const { min, max } = this.state.range;
    return (
      <View style={[sty.rowContainer, { flex: 7 }]}>
        <TextInput
          style={[sty.inputTxt, { flex: 10 }]}
          onChangeText={(text) => this.rangeChanged(true, text)}
          value={`${min}`}
        />

        <Text style={[sty.styText, { flex: 3 }]}> ~ </Text>
        <TextInput
          style={[sty.inputTxt, { flex: 10 }]}
          onChangeText={(text) => this.rangeChanged(false, text)}
          value={`${max}`}
        />
        <Text
          style={[sty.styText, { flex: 8, textAlign: 'right', marginRight: 20 }]}
        > {this.props.item.unit}</Text>
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langSetting, langKey } = this.props.uiState;
    //const { explain } = this.state;
    const { range, key } = this.props.item;
    const { title, explain } = langSetting[langKey][key];
    const valueArea = range === undefined ? this.renderValue() : this.renderRange(); // 오른쪽 뷰..
    const { setValueLF, setRangeLF } = uicommon[langKey];
    const typeStr = range === undefined ? setValueLF : setRangeLF; //'값\n설정' : '범위\n설정';
    return (
      <View style={[sty.container]}>
        <View style={[sty.rowContainer, { flex: 5 }]}>
          <View style={[sty.roundVw, { flex: 8 }]}>
            <Text style={[sty.title, { flex: 3 }]}>{title}</Text>
            <Text style={[sty.genText, { flex: 5 }]}>{explain}</Text>
          </View>
          <View
            style={[sty.roundVw, { flex: 2,
              margin: 12,
              paddingTop: 10, backgroundColor: '#689' }]}
          >
            <Text style={[sty.boxKind, { flex: 4 }]}>{typeStr}</Text>
          </View>

          {valueArea}
        </View>

      </View>
    );
  } // onChangeText={(text) => this.changeName(text)}
}


///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#EEEAE5',
    margin: 2,
    borderRadius: 10,
    height: 75
  },
  rowContainer: {
    flexDirection: 'row', alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },
  roundVw: {
    //flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
    borderRadius: 5,
    paddingTop: 5
  },
  genText: {
    fontWeight: '400', textAlign: 'center',
    fontSize: 15,
    padding: 10,
  },
  title: {
    fontWeight: '900',
    fontSize: 18,
    color: '#345',
    textAlign: 'center',
  },
  boxKind: {
    textAlign: 'center',
    color: '#EEC',
    fontWeight: '300',
    fontSize: 14,
  },
  styText: {
    flex: 1, alignSelf: 'stretch',
    textAlign: 'center', color: '#252',
    paddingTop: 25,
    //backgroundColor: '#DDD',
    fontWeight: '300', fontSize: 16,
  },
  inputTxt: {
    alignSelf: 'stretch',
    margin: 10, marginTop: 20, marginBottom: 20,
    textAlign: 'center', color: '#133',
    fontWeight: '300', fontSize: 18,
    backgroundColor: 'white',
    borderWidth: 2, borderColor: '#469',
    borderRadius: 8
  }
});

const mapStateToProps = state => {
  return {
    main: state.main,
    EttSetting: state.EttSetting,
    chkItems: state.chkItems,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(SettingGenCell);
