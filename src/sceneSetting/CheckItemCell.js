/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneSetting / CheckItemSetting.js
 *
 * Created by Jongwoo Moon on 2017. 3. 9..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, ListView, View, Text, TextInput, Switch } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as actions from '../actions';
import { HtText, HtInput, HtColor, HtButton } from '../uicompo';

class CheckItemCell extends Component {
  constructor(props) {
    super(props);
    let score = this.props.item.score;
    let active = this.props.item.active;
    const { auto, key, shr, long } = this.props.item;
    if (auto) {
      ({ score, active } = this.props.EttSetting.autoCheckItem[key]);
    } else {
      //console.log(this.props.item);
    }
    this.state = {
      expanded: false,
      shr, long, score: `${score}`, active
    };
  }

  renderShrinkedView() {
    const { langKey, langAutoChk } = this.props.uiState;
    let { shr, active } = this.state;
    const { auto, key } = this.props.item;
    if (auto) {
      shr = langAutoChk[langKey][key].shr; // auto 일때만 언어팩 적용
    }
    let autoManCol = auto ? '#FEDADA' : '#DEDAF5';
    autoManCol = active ? autoManCol : '#BBB';

    const textColor = auto ? '#227' : '#252';
    return (
      <View
        style={[sty.container, { height: 30,
          backgroundColor: autoManCol }]}
      >
        <HtButton
          f={1} width={100} ptext={shr}
          vwStyle={{ margin: 1, padding: 1 }}
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: textColor, margin: 1
          }}
          onPressCallback={() => {
            this.setState({ expanded: true });
          }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { uicommon, langKey, langAutoChk } = this.props.uiState; // 언어팩..
    const { auto, minusFactor, key } = this.props.item;
    const { expanded, score, active } = this.state;
    let { shr, long } = this.state;
    if (auto) {
      shr = langAutoChk[langKey][key].shr; // auto 일때만 언어팩 적용
      long = langAutoChk[langKey][key].long; // auto 일때만 언어팩 적용
    }
    const { autoCheck, manualChk, partScore, apply } = uicommon[langKey]; // plusFactorItem, minusFactorItem
    const textColor = auto ? '#227' : '#252',
      longBackground = auto ? 'transparent' : 'white',
      longBorder = auto ? sty.noBorder : sty.grayBorder,
      autoManTxt = auto ? autoCheck : manualChk; // '자동감지' : '수동체크';
    let autoManCol = auto ? '#FEDADA' : '#DEDAF5';
    autoManCol = active ? autoManCol : '#BBB';

    const editable = !auto;
    //const minusFactorText = minusFactor ? minusFactorItem : plusFactorItem;
    const shortColumn = auto ? null : ( // 짧은 제목
      <HtInput
        mandator={{
          flex: 5, rad: 5, value: shr, margin: { l: 10, r: 10 }, editable,
          onChange: (text) => this.setState({ shr: text })
        }}
        style={{ input: { fontSize: 17 }, border: sty.grayBoarder }}
      />
    );
    const closeStr = uicommon[langKey].close;

    if (!expanded) {
      return (
        this.renderShrinkedView()
      );
    }
    return (
      <View
        style={[sty.container, { height: 130, backgroundColor: autoManCol }]}
      >
        <HtButton
          f={8} width={100} ptext={`${shr}        ${closeStr}`}
          vwStyle={{ margin: 1 }}
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: textColor
          }}
          onPressCallback={() => {
            this.setState({ expanded: false });
          }}
        />
        <View style={sty.rowContainer}>
          <HtText
            mandator={{ flex: 3, rad: 5, text: autoManTxt, margin: { r: 10 } }}
            style={{ viewStyle: sty.vw, textStyle: sty.txt }}
          />

          {shortColumn}

          <HtText
            mandator={{ flex: 2, rad: 5, text: partScore, margin: { l: 5, r: 5 } }}
            style={{ viewStyle: sty.vw, textStyle: sty.txt }}
          />

          <HtInput
            mandator={{ // 점수는 항상 수정 가능..
              flex: 2, rad: 5, value: score, margin: { l: 5 }, editable: true,
              onChange: (text) => {
                this.setState({ score: text });
                if (auto) {
                  this.props.editAutoScoreActive({ [key]: { score: Number(text), active } });
                } else {
                  this.props.editManualScoreActive({ ...this.props.item, score: Number(text), active });
                }
              }
            }}
            keyboardType={'numeric'}
            style={{ input: { fontSize: 17 }, border: sty.grayBoarder }}
          />

          <HtText
            mandator={{ flex: 2, rad: 5, text: apply, margin: { l: 5, r: 5 } }}
            style={{ viewStyle: sty.vw, textStyle: sty.txt }}
          />

          <Switch
            disabled={false}
            style={{ flex: 1, alignSelf: 'flex-end' }}
            value={active}
            onValueChange={(value) => {
              this.setState({ active: value });
              if (auto) {
                this.props.editAutoScoreActive({ [key]: { score: Number(score), active: value } });
              } else {
                this.props.editManualScoreActive({ ...this.props.item, score: Number(score), active: value });
              }
            }}
          />

        </View>

        <HtInput
          mandator={{
            flex: 18, rad: 5, value: long, margin: { t: 5 }, editable,
            onChange: (text) => this.setState({ long: text })
          }}
          style={{ input: { fontSize: 17, backgroundColor: longBackground }, border: longBorder }}
        />

      </View>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    borderRadius: 5,
    margin: 4, padding: 3, // padding: 0, paddingLeft: 2, paddingRight: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { backgroundColor: HtColor.blueGray, marginLeft: 5, marginRight: 5 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  noBorder: { borderWidth: 0 },
  grayBorder: {
    margin: 2,
    borderWidth: 0.5,
    borderColor: '#888', //(#dcdcdc)
  },
});

const mapStateToProps = state => {
  return {
    main: state.main,
    EttSetting: state.EttSetting,
    chkItems: state.chkItems,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(CheckItemCell);


//
// <TextInput
//   style={[sty.title, sty.grayBoarder, { fontSize: 15 }]}
//   onChangeText={(text) => this.setState({ long: text })}
//   value={long}
// />
