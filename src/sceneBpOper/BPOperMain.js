/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpOper / BPOperMain.js
 *

 * Created by Jongwoo Moon on 2017. 4. 4..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Text, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import BPTitleBar from './TitleBar';
import BpGraph from './BpGraph';
import BpStringOper from './BpStringOper';
import ControlBar from './ControlBar';
import * as M from '../model';

const lang = require('../data/langOper.json');
const packetSamples = require('../model/packSmplBP.json');

const bpLogic = new M.BpLogic();
const mdl = new M.SettingModule();

class BPOperMain extends Component {
  constructor(props) {
    super(props);
    const { isPlaying, mode } = this.props.OperBP;
    console.log(`\n\n\n\n\n ====== ====== ====== ======  [[ SceneBpOper :: constructor ]]  ..... ${mode} \n`);

    this.state = { cnt: 0, timeStr: '0' };
    this.interval = null;
    bpLogic.init(this.props); //  BpLogic :: 로직 처리 객체.. 리듀서에 등록..

    this.bpData = bpLogic.getBpData();

    // 세팅 정보 읽어서 리듀서에 등록.
    mdl.retrieveBpSetting('bpTrainSetting', (obj) => {
      this.props.setBpSetting({ train: obj });
    });
    mdl.retrieveBpSetting('bpRealTestSetting', (obj) => {
      this.props.setBpSetting({ real: obj });
      this.props.setBpValues(this.bpData);
    });

    this.packetGeneratorCheck(isPlaying); // 가짜 패킷 제조 할지 말지..

    this.bleCompo = this.props.main.bleCompo; // ble 매니저.
  }

  async setBpObject() {
    try {
      await this.props.setBpValues(this.bpData);
    } catch (err) {
      console.log(err);
    }
  }

  tickPlay() {
    console.log('\n ------------------------------------------------------- >> Tick Play');

    this.props.bpPlayTick();

    // time set..
    const { isPlaying, playObj } = this.props.OperBP;
    if (isPlaying) {
      const nowTs = new Date();
      const periodInSec = (nowTs - playObj.startedAt) * 0.001;

      this.setState({ timeStr: `${periodInSec}` });
    } else {
      this.setState({ timeStr: '0' });
    }
  }

  tickFunction() {
    console.log('\n ------------------------------------------------------- >> Tick');
    this.setState({
      cnt: this.state.cnt + 1
    });
    const pack = packetSamples[this.state.cnt % packetSamples.length];
    console.log('BP 샘플 패킷 ');

    this.props.BpPacket(pack);
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    this.props.changeMainState('bpOper');
    //this.props.examListViewMode();
    //this.props.setBpUxInit();
    this.props.propagateBP(bpLogic);

    if (this.props.bleRdcr.conState === 'Connected') {
      this.sendPacket();
    }




    // const bpData = bpLogic.getBpData();
    // console.log(bpData);
    // this.props.setBpValues(bpData);
  }



  componentWillUpdate() {
    //LayoutAnimation.easeInEaseOut(); //spring(); // easeInEaseOut();
  }

  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ BPOperMain :: unmount ]] ...\n');
    clearInterval(this.interval);
    clearInterval(this.uxTimer);
  }

  sendPacket() {
    const { mode, setting } = this.props.OperBP;
    const { bpm, high, low, periodGap } = this.bpData;

    const curSetObj = (mode === 'TRAIN') ? setting.train : setting.real;

    const pulseIntense = (mode === 'TRAIN') ? curSetObj.pulseSense : 1;
    const soundIntense = (mode === 'TRAIN') ? curSetObj.soundSense : 1;
    const d = { pulseIntense, soundIntense };
    const operate = true, speaker = true; // 이건 나중에 확인해 봐야 함...
    // 촉진법, 청음, 촉진-청음.
    const isTouch = (mode === 'TRAIN' && curSetObj.method === 0);
    const gab = (periodGap !== null);
    const e = { operate, speaker, isTouch, gab };

    this.bleCompo.sendBpPacket(bpm, high, low, d, e); // pulse, bpMax, bpMin, d, e
  }

  packetGeneratorCheck(isPlaying) {
    if (this.props.bleRdcr.conState === 'Connected') {
      return;
    }
    if (!this.props.main.debugMode) {
      return;
    }
    if (isPlaying && this.interval === null) {
      this.interval = setInterval(this.tickFunction.bind(this), 1000);
    }
    if (!isPlaying && this.interval !== null) {
      clearInterval(this.interval);
      this.interval = null;
    }
  }

  startPlay() {
    if (this.props.bleRdcr.conState === 'Connected') {
      return;
    }
    this.uxTimer = setInterval(this.tickFunction.bind(this), 500);  // debugging use
  }

  stopPlay() {
    const { packets } = this.props.OperBP;
    console.log(`  Stopped ....  packets : ${packets.length} ea`);
    clearInterval(this.uxTimer);
    this.setState({ cnt: 0 });
    //this.props.makeBrthHistory(arrBrth);
    Actions.sceneBpMeasureInput();
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { mode } = this.props.OperBP;
    const { uicommon, langKey } = this.props.uiState;
    const { bpOperationTr, bpOperationRl, goback } = uicommon[langKey];
    const title = (mode === 'TRAIN') ? bpOperationTr : bpOperationRl;

    //return (<View />);
    return (
      <View style={sty.mainContainer}>
        <BPTitleBar
          flex={10}
          back={goback}
          title={title}
        />
      <BpGraph flex={50} />
      <BpStringOper flex={30} />
      <ControlBar
        flex={10}
        title={lang.Title}
        playCallback={this.startPlay.bind(this)}
        stopCallback={this.stopPlay.bind(this)}
      />
      </View>
    );
  }
}

const sty = StyleSheet.create({
  mainContainer: {
    marginTop: 1, flex: 100
  },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    OperBP: state.OperBP,
    bleRdcr: state.bleRdcr,
    theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(BPOperMain);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
