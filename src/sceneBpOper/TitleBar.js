/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpOper / BPOperMain.js
 *
 * Created by Jongwoo Moon on 2017. 3. 27..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen } = HtColor;

class BPTitleBar extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ TitleBarOperate :: constructor ]] ');
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    console.log('\n ====== ====== ====== ======  [[ BPTitleBar :: componentDidMount ]]');
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { title, flex, back } = this.props;
    const smallButtonFlex = 5;

    return (
        <View style={{ flex, backgroundColor: 'dodgerblue' }}>
          <View style={[styl.rowContainer, { flex: 1, padding: 12 }]}>
            <TouchableOpacity
              style={{
                flex: smallButtonFlex,
                flexDirection: 'row', alignSelf: 'flex-end' // 글자 밑으로 정렬.
              }}
              onPress={Actions.pop}
            >
              <Text style={[sty.miniTitle, { textAlign: 'left', flex: 1, marginLeft: 20 }]}>
                {back}
              </Text>
            </TouchableOpacity>

            <Text style={[sty.title, { flex: 30 }]}>
              {title}
            </Text>

            <View style={{ flex: smallButtonFlex }} />
          </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    flex: 30,
    alignSelf: 'flex-end',
    color: bttnBrightTextGreen,
    fontSize: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  miniTitle: {
    alignSelf: 'center', textAlign: 'center',
    color: bttnBrightTextGreen,
    fontSize: 18,
    fontWeight: '800',
    marginBottom: 5
  }
});


const mapStateToProps = (state) => {
  return {
    //main: state.main,
    //theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(BPTitleBar);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
