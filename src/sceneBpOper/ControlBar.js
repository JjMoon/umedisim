/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneBpOper / ControlBar.js
 *
 * Created by Jongwoo Moon on 2017. 4. 5..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');

const { bttnBrightTextGreen, bgBlue } = HtColor;

/** BP Control Bar at bottom */
class ControlBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: false,
      modalVisible: false,
      dueTimeInSec: 0,
    };
    console.log('\n ====== ====== ====== ======  [[ ControlBar :: constructor ]] ');
  }

  // //////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    console.log('\n ====== ====== ====== ======  [[ ControlBar :: componentDidMount ]]');
  }

  componentWillUnmount() {
    clearInterval(this.stopWatch);
  }

  playAndPause() {
    const { isPlaying } = this.props.OperBP;
    console.log(`playAndPause() send >>>>>>>  ${this.state.isPlaying}  ${this.state.modalVisible}`);

    if (this.state.isPlaying) { // mainView 로 보내는 콜백..
      this.props.stopCallback();
      clearInterval(this.stopWatch);
    } else {
      this.stopWatch = setInterval(() => {
        console.log(`\n\n\n BP Time : ${this.state.dueTimeInSec}  \n\n\n`);
        this.setState({
          dueTimeInSec: this.state.dueTimeInSec + 1,
        });
      }, 1000);
      this.props.playCallback();
    }
    this.props.bpPlayOrStop(!isPlaying);
    this.setState({ isPlaying: !isPlaying });
  }

  getPlayButton() {
    const { height } = this.props.main.scr, size = height * 0.11;
    const { isPlaying } = this.props.OperBP;

    if (isPlaying) {
      return (
        <Image
          style={{ width: size, height: size }}
          source={require('../resource/imageCommon/pausebutton.png')}
        />
      );
    }
    return (
      <Image
        style={{ width: size, height: size }}
        source={require('../resource/imageCommon/playbutton.png')}
      />
    );
  }

  // //////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex } = this.props;
    const { dueTimeInSec } = this.state;
    const min = dueTimeInSec > 59 ? `${parseInt(dueTimeInSec / 60)} : ` : null;
    const sec = `${dueTimeInSec % 60}`;
    return (
      <View style={{ flex, backgroundColor: bgBlue }}>


        <View style={[styl.rowContainer, { flex: 100 }]}>
          <TouchableOpacity onPress={this.playAndPause.bind(this)}>
            {this.getPlayButton()}
          </TouchableOpacity>

          <Text style={sty.timer}>{min}{sec}</Text>
        </View>
      </View>
    );
  }
}

const sty = StyleSheet.create({
  // / -------------------------  -------------------------  // <Text>
  title: {
    color: bttnBrightTextGreen,
    fontSize: 30,
    fontWeight: '600',
    textAlign: 'left',
    margin: 20,
    marginTop: 35,
    alignSelf: 'stretch',
  },
  timer: {
    flex: 5, fontSize: 35, fontWeight: '400',
    color: bttnBrightTextGreen,
    textAlign: 'right',
    margin: 20, marginTop: 15,
    alignSelf: 'stretch',
  },
});

const mapStateToProps = state => ({
  main: state.main,
  OperBP: state.OperBP,
  theCounter: state.timerCount,
});

export default connect(mapStateToProps, actions)(ControlBar);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
