/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpOper / BpStringOper.js
 *
 * Created by Jongwoo Moon on 2017. 3. 27..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtText, HtButton, HtColor } from '../uicompo';

class BpStringOper extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ BpStringOper :: constructor 초기값 설정. ]] ');
    const { mode, setting } = this.props.OperBP;
    const setObj = (mode === 'TRAIN') ? setting.train : setting.real;
    const { pulseSense, soundSense } = setObj;
    this.state = {
      pulseSense, soundSense
    };
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    console.log('\n ====== ====== ====== ======  [[ BpStringOper :: componentDidMount ]]');
  }

  renderBigInfos(main, unitStr, value) {
    const { unit } = this.props.main.scr;
    const mainTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 3.5 * unit };
    const unitTxt = { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 2.4 * unit };
    const valVw = { backgroundColor: 'white', borderWidth: 3, borderColor: '#9A9' };
    const valTxt = { color: '#353', fontWeight: '500', fontSize: 3 * unit };

    return (
      <View
        style={{
          flexDirection: 'row', flex: 10, margin: 20, padding: 7, // marginLeft: 30, marginRight: 30,
          borderRadius: 5, borderWidth: 1, borderColor: '#234'
        }}
      >
        <HtText
          mandator={{ flex: 6, rad: 5, text: main, margin: { } }}
          style={{ viewStyle: sty.vw, textStyle: mainTxt }}
        />
        <HtText
          mandator={{ flex: 4, rad: 5, text: unitStr, margin: { lr: 1 } }}
          style={{ viewStyle: sty.vw, textStyle: unitTxt }}
        />
        <HtText
          mandator={{ flex: 5, rad: 10, text: value, margin: { all: 10 } }}
          style={{ viewStyle: valVw, textStyle: valTxt }}
        />
      </View>
    );
  }

  renderPluMin(active, ttl, value, plCallback, mnCallback) {
    const { unit } = this.props.main.scr;
    const plTxtColor = active ? '#55F' : '#CCC';
    const mainTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 3 * unit };
    const valueSty = { color: HtColor.bgDarkGray, textAlign: 'center', fontWeight: '400', fontSize: 2.7 * unit };
    const plmnSty = { color: plTxtColor, fontWeight: '900', fontSize: 4 * unit };

    // onPressCallback={this._bttnActOtherView.bind(this)}
    return (
      <View
        style={{
          flexDirection: 'row', flex: 20, margin: 10, marginLeft: 30, marginRight: 30,
          //borderWidth: 3, borderColor: '#CCC', borderRightWidth: 0, borderLeftWidth: 0
        }}
      >
        <HtText
          mandator={{ flex: 8, rad: 5, text: ttl, margin: { } }}
          style={{ viewStyle: sty.vw, textStyle: mainTxt }}
        />

        <HtButton
          width={100} f={2} ptext={'＋'} txtStyle={plmnSty}
          onPressCallback={plCallback}
          active={active}
        />

        <HtText
          mandator={{ flex: 6, rad: 5, text: value, margin: { lr: 1 } }}
          style={{ viewStyle: sty.vw, textStyle: valueSty }}
        />

        <HtButton
          width={100} f={2} ptext={'－'} txtStyle={plmnSty}
          onPressCallback={mnCallback}
          active={active}
        />
      </View>
    );
  }

  renderPeriod(ttl, value, unitStr) {
    const { unit } = this.props.main.scr;
    const mainTxt = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 2.5 * unit };
    const valueSty = { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 3 * unit };
    //const unitSty = { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 2 * unit };

    return (
      <View
        style={{
          flexDirection: 'row', flex: 10, margin: 10, marginLeft: 30, marginRight: 30,
          //borderWidth: 3, borderColor: '#CCC', borderRightWidth: 0, borderLeftWidth: 0
        }}
      >
        <HtText
          mandator={{ flex: 10, rad: 5, text: ttl, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: mainTxt }}
        />
        <HtText
          mandator={{ flex: 8, rad: 5, text: value, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: valueSty }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex } = this.props;
    const { playObj, ux, mode, curBpVal } = this.props.OperBP;
    const { pressure, prsrDescRate } = ux;
    const { pulseSense, soundSense } = this.state;
    const periodStr = (curBpVal.periodGap === null) ? '끔' : '켬';
    const activePlusMinus = (mode === 'TRAIN');

    return (
      <View style={{ flex, backgroundColor: '#FFFAFF' }}>
        <View style={{ flexDirection: 'row', flex: 10 }} >
          {this.renderBigInfos('커프 압력', '(mmHg)', `${pressure}`)}
          {this.renderBigInfos('감압 속도', '(mmHg/s)', `${prsrDescRate}`)}
        </View>

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}

        <View style={{ flexDirection: 'row', flex: 7 }} >

          {this.renderPluMin(activePlusMinus, '맥박 강도', `${pulseSense}`, () => {
            console.log('맥박 강도 + '); // max 3
            if (pulseSense < 3) {
              this.setState({ pulseSense: pulseSense + 1 });
              // 여기서 패킷 보내자.
            }
          }, () => {
            console.log('맥박 강도 -');
            if (pulseSense > 1) {
              this.setState({ pulseSense: pulseSense - 1 });
            }
          })}
          {this.renderPluMin(activePlusMinus, '음 강도', `${soundSense}`, () => {
            console.log('음 강도 + '); // max 5
            if (soundSense < 5) {
              this.setState({ soundSense: soundSense + 1 });
            }
          }, () => {
            console.log('음 강도 -');
            if (soundSense > 1) {
              this.setState({ soundSense: soundSense - 1 });
            }
          })}

          {this.renderPeriod('청진 \n간극', periodStr)}
          {this.renderPeriod('실습 \n시간', playObj.playTmStr)}

        </View>

      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  xxtitle: {

  },
  seperateLine: { height: 1, backgroundColor: '#888' },
});


const mapStateToProps = (state) => {
  return {
    main: state.main,
    OperBP: state.OperBP,
    //theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(BpStringOper);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
