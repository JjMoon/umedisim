/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneBpOper / BPGraph.js
 *
 * Created by Jongwoo Moon on 2017. 3. 27..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import ReactNativeART, { Surface, Group, Shape } from 'ReactNativeART';
import * as scale from 'd3-scale';
import * as shape from 'd3-shape';
import _ from 'lodash';
import * as actions from '../actions';
import { HtText, HtColor } from '../uicompo';

const styl = require('../uicompo/HtStyles');
const d3 = { scale, shape }, maxBP = 300;
//const totalTimeInSec = 20; // 그래프에서 보여지는 총 시간
const { bttnBrightTextGreen } = HtColor;

class BpGraph extends Component {
  constructor(props) {
    super(props);
    console.log('\n ====== ====== ====== ======  [[ TitleBarOperate :: constructor ]] ');

    this.state = { viewWidth: 200, viewHeight: 100, offset: 10, strDist: 40 };
  }
  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    const { graphOnly = false } = this.props;
    const { scr } = this.props.main;
    const viewWidth = scr.width * (60 / 65) * (5 / 6),
      viewHeight = scr.height * 0.5;

    const newWidth = graphOnly ? scr.width : viewWidth;
    this.setState({ viewWidth: newWidth, viewHeight });
    console.log('\n ====== ====== ====== ======  [[ BpGraph :: componentDidMount ]]');
  }

  drawGraph() {
    const { graphOnly = false } = this.props;
    const { viewWidth, viewHeight, strDist, offset } = this.state;
    const { setting, playObj } = this.props.OperBP;
    const totalTimeInSec = setting.real.examTimeMin * 30; // 전체 시간의 반만...
    const timeSpanSec = totalTimeInSec * 1000;
    let endTime = new Date(),
      startTime = new Date(endTime.getTime() - timeSpanSec);
    if (graphOnly) { // 전체 시간..
      const { startedAt, finAt } = playObj;
      startTime = startedAt;
      endTime = finAt;
    }

    const
      scTime = d3.scale.scaleTime()
        .domain([startTime, endTime])
        .range([strDist, viewWidth - offset]),
      scY = d3.scale.scaleLinear()
        .domain([0, maxBP])
        .range([offset, viewHeight - offset]);
    this.scY = scY; this.scTime = scTime; // 멤버 변수로 설정하여 널리 사용...
    return d3.shape.line()
      .x((v) => { //console.log(v.tsObj);
        return scTime(v.tsObj);
      })
      .y((v) => { //console.log(maxBP - v.prsr);
        return scY(maxBP - v.prsr);
      });
  }

  renderRealCompo() {
    const { mode, curBpVal, setting } = this.props.OperBP;
    if (mode === 'TRAIN') {
      console.log('  its train ....  return empty group');
      return (<Group x={0} y={0} />);
    }    //const setObj = (mode === 'REAL') ? setting.real : setting.train;
    const { viewWidth, viewHeight, strDist, offset } = this.state;
    const { high, low, periodGap } = curBpVal;
    const ori = { x: strDist, y: viewHeight - offset },
      end = { x: viewWidth - offset, y: offset };
    const overY = this.scY(maxBP - high - setting.real.pressStandard),
      highY = this.scY(maxBP - high), lowY = this.scY(maxBP - low);
    const overLine = `M${ori.x} ${overY} ${end.x} ${overY}`;
    const area = `M${ori.x} ${highY} ${end.x} ${highY} ${end.x} ${lowY} ${ori.x} ${lowY} ${ori.x} ${highY}`;
    let areaGroup = (
      <Group x={0} y={0}>
      <Shape
        d={area}
        fill="#DDF"
      />
      </Group>
    );

    if (periodGap !== null) { // 청진 간극 표시
      const { up, down } = periodGap;
      //console.log(` 청진 간극 표시  up ~ down >> ${up}   ${down}`);
      const uy = this.scY(maxBP - up), dy = this.scY(maxBP - down);
      const aup = `M${ori.x} ${highY} ${end.x} ${highY} ${end.x} ${uy} ${ori.x} ${uy} ${ori.x} ${highY}`;
      const adn = `M${ori.x} ${dy} ${end.x} ${dy} ${end.x} ${lowY} ${ori.x} ${lowY} ${ori.x} ${uy}`;
      //console.log(aup); console.log(adn); console.log(overLine);
      areaGroup = (
        <Group x={0} y={0}>
          <Shape d={aup} fill="#DDF" />
          <Shape d={adn} fill="#DDF" />
        </Group>
      );
    }
    return (
      <Group x={0} y={0}>
        <Group x={0} y={0}>
          <Shape d={overLine} stroke="#C22" strokeWidth={1} />
        </Group>
          {areaGroup}
      </Group>
    );
  }

  renderBars() {
    const { touched, cuffed } = this.props.OperBP.uxOnce;
    let touchSign = null, cuffSign = null;

    if (touched.v) { // 촉진.. 회색..
      const xCor = this.scTime(touched.ts);
      touchSign = this.getUnitBar(xCor, '#999', 100);
    }

    if (cuffed.v) {
      const xCor = this.scTime(cuffed.ts);
      cuffSign = this.getUnitBar(xCor, '#5C5', 10);
    }
    return (
      <Group x={0} y={0}>
        <Group x={0} y={0}>
          {touchSign}
          {cuffSign}
        </Group>
      </Group>
    );
  }

  getUnitBar(xCor, color, offset) {
    const lowY = this.ori.y - 10 - offset, upY = this.ori.y - 90 - offset, wid = 10;
    // stroke={color} strokeWidth={8}
      return (
        <Shape
          d={`M${xCor} ${lowY}  ${xCor - wid} ${upY}  ${xCor + wid} ${upY} `}
          fill={color}
        />
      );
  }

  renderGraph() {
    const { graphOnly = false } = this.props;
    const { viewWidth, viewHeight, strDist, offset } = this.state;
    const { packets, setting } = this.props.OperBP;
    const totalTimeInSec = setting.real.examTimeMin * 60;
    const timeSpanSec = totalTimeInSec * 1000, endTime = new Date(),
      startTime = new Date(endTime.getTime() - timeSpanSec);
    const arrForGraph = _.filter(packets, (o) => {
      return startTime < o.tsObj.getTime();  // o.ts; // 시작 이후..
    });
    const graphShape = (<Shape
      d={this.drawGraph()(arrForGraph)}
      stroke="#599"
      strokeWidth={2}
    />);
    const realCompo = this.renderRealCompo();
    const yL = viewHeight - (2 * offset), yStp = yL / 6,
      ori = { x: strDist, y: viewHeight - offset },
      end = { x: viewWidth - offset, y: offset };
    const horLines = _.chain(_.range(1, 6))
      .map((n) => { return ori.y - (n * yStp); })
      .reduce((sum, n) => { return `${sum} M${ori.x} ${n} ${end.x} ${n} `; }, '');
    const axis = `M${ori.x} ${ori.y} ${end.x} ${ori.y}  M${ori.x} ${ori.y} ${ori.x} ${end.y} `;

    const txtH = viewHeight * 0.04, txtMarg = txtH * 1.2,  // 축의 기준값 글씨 관련...
      txt200 = (viewHeight - txtMarg) - (4 * yStp), txt100 = (viewHeight - txtMarg) - (2 * yStp);
    const txtSty = { position: 'absolute', marginLeft: 5, fontSize: txtH };

    this.ori = ori; this.end = end;

    return (
      <View style={{ flex: 50, backgroundColor: '#EEE', alignItems: 'stretch' }} >
        <Surface
          height={viewHeight}
          width={viewWidth}
          style={sty.surfaceContainer}
        >
          <Group x={0} y={0}>
          {realCompo}
          <Shape
            d={axis} stroke="#444"
            strokeWidth={1}
          />
          <Shape
            d={horLines} stroke="#688"
            strokeWidth={0.5}
          />
            {graphShape}
          </Group>
          {this.renderBars()}
        </Surface>
        <Text style={[txtSty, { marginTop: 10 }]} >300</Text>
        <Text style={[txtSty, { marginTop: txt200 }]} >200</Text>
        <Text style={[txtSty, { marginTop: txt100 }]} >100</Text>
      </View>
    );
  }

  renderSmlInfo(ttl, value, unitTxt) {
    const { unit, width } = this.props.main.scr;
    const wunit = width * 0.01;
    const vwSty = { backgroundColor: '#0000', margin: 1 };
    const ttlSty = { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 1.7 * wunit };
    const unitSty = { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 1 * wunit };
    const valSty = { color: '#353', fontWeight: '500', fontSize: 2 * unit };
    //const valVw = { backgroundColor: 'white', borderWidth: 3, borderColor: '#9A9' };

    return (
      <View
        style={{
          flexDirection: 'row', flex: 10,
          // borderRadius: 5, borderWidth: 1, borderColor: '#234'
        }}
      >
        <HtText
          mandator={{ flex: 7, rad: 1, text: ttl, margin: { lr: 1 }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: ttlSty }}
        />
        <HtText
          mandator={{ flex: 5, rad: 1, text: value, margin: { all: 1 }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: valSty }}
        />
        <HtText
          mandator={{ flex: 5, rad: 1, text: unitTxt, margin: { all: 2 }, radIgnore: true }}
          style={{ viewStyle: vwSty, textStyle: unitSty }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   촉진 커프 표시 컬럼..
  renderCuffVw(isOn, isTouch) {
    const { touched, cuffed } = this.props.OperBP.uxOnce;
    const ttl = isTouch ? '촉진' : '커프';
    const onOffCase = isTouch ? touched.v : cuffed.v;
    const txtSty = onOffCase ? sty.txt : sty.txtOff;

    return (
      <View
        style={{
          flex: 100, borderWidth: 2, borderRadius: 5, borderColor: '#555',
          justifyContent: 'center', alignItems: 'stretch', margin: 5
        }}
      >
        <HtText
          mandator={{ flex: 3, rad: 5, text: ttl, margin: { t: 4 } }}
          style={{ viewStyle: sty.vw, textStyle: txtSty }}
        />
        <HtText
          mandator={{ flex: 3, rad: 5, text: 'OK', margin: { b: 4 } }}
          style={{ viewStyle: sty.vw, textStyle: txtSty }}
        />
      </View>

    );
  }

  renderRealGuide(curBpVal) {
    const { high, low, bpm } = curBpVal;
    return (
      <View style={{ flex: 10, alignSelf: 'stretch', borderRadius: 3, borderWidth: 1, margin: 5 }} >
        <View style={{ flex: 2 }} />
        {this.renderSmlInfo('수축기', `${high}`, 'mmHG')}
        {this.renderSmlInfo('이완기', `${low}`, 'mmHG')}
        {this.renderSmlInfo('맥박수', `${bpm}`, 'bpm')}
        <View style={{ flex: 2 }} />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { flex, graphOnly = false, main, OperBP } = this.props;
    const { mode, curBpVal } = OperBP;

    if (graphOnly) {
      return (
        <View style={{ flex, flexDirection: 'row' }}>
          {this.renderGraph()}
        </View>
      );
    }

    // real : 155 140, train : 155 103
    const scl = (main.scr.width * (60 / 64) * (1 / 6)) / 165,
      dimX = 155, dimY = (mode === 'REAL') ? 140 : 103,
      signX = scl * dimX, signY = scl * dimY,
      source = (mode === 'REAL') ?
        require('../resource/imageCommon/real_sign.png') :
        require('../resource/imageCommon/train_sign.png'),
      realGuide = (mode === 'REAL') ? this.renderRealGuide(curBpVal) : null;

    return (
      <View style={{ flex, flexDirection: 'row' }}>

        <View style={{ flex: 5 }} >
          {this.renderCuffVw(true, true)}
          {this.renderCuffVw(false, false)}
        </View>
        <View style={{ flex: 60 }} >
          <View style={{ flexDirection: 'row', flex: 6 }} >

            {this.renderGraph() /* flex : 50 */}

            <View style={{ flex: 10, alignItems: 'center', justifyContent: 'center' }} >

              <Image
                style={{ width: signX, height: signY }}
                source={source}
              />
              <View style={{ flex: 5 }} />
              {realGuide}

            </View>
          </View>
        </View>

      </View>
    );
  }
}

const sty = StyleSheet.create({
  /// -------------------------  -------------------------  // <Text>
  title: {
    flex: 30,
    alignSelf: 'flex-end',
    color: bttnBrightTextGreen,
    fontSize: 25,
    fontWeight: '600',
    textAlign: 'center',
  },
  miniTitle: {
    alignSelf: 'center', textAlign: 'center',
    color: bttnBrightTextGreen,
    fontSize: 18,
    fontWeight: '800',
    marginBottom: 5
  },
  surfaceContainer: {
    flex: 70,
    alignSelf: 'stretch',
    borderWidth: 1,
    borderRadius: 5,
    margin: 1
  },
  vw: { backgroundColor: '#0000', margin: 3 },
  txtBold: { color: HtColor.bgDarkGray, fontWeight: '800', fontSize: 25 },
  txt: { color: HtColor.bgDarkGray, fontWeight: '300', fontSize: 25 },
  txtOff: { color: '#BBA', fontWeight: '700', fontSize: 25 },
});


const mapStateToProps = (state) => {
  return {
    main: state.main,
    OperBP: state.OperBP,
    //theCounter: state.timerCount,
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions, null, { withRef: true })(BpGraph);
//export default connect(mapStateToProps, actions)(BpGraph);
// connect() 에서 함수를 리턴하면 거기에 SceneConnect 를 전달함..
