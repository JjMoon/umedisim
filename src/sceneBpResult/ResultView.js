/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneBpResult / MainMeasureInput.js
 *
 * Created by Jongwoo Moon on 2017. 4. 7..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { takeSnapshot } from 'react-native-view-shot';
import * as actions from '../actions';
import { HtText, HtButton, HtColor } from '../uicompo';
import MsTitleBar from '../simulations/MsTitleBar';
import { ResultUnitView, HsControlBar } from '../simulations';
import HsStudentExamVw from '../simulations/HsStudentExamVw';
import * as M from '../model';
import BpGraph from '../sceneBpOper/BpGraph';

const langBp = require('../data/langBp.json');
const langBpSetting = require('../data/langBpSetting.json');

class ResultView extends Component {
  constructor(props) {
    super(props);

    const { langKey } = this.props.uiState;
    const { mode } = this.props.OperBP;

    this.state = {
      realTestMode: (mode === 'REAL'), lang: langBp[langKey],
      value: {
        format: 'png', quality: 0.9, result: 'file',
        snapshotContentContainer: false,
      },
      report: { overall: null, graph: null }
    };
  }

  componentWillMount() {
    this.props.changeMainState('init');
  }

  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ SceneMain :: unmount ]] ...\n');
  }

  onSavePress() {
    console.log('\n\n\n\n\n  ResultView    onSavePress  데이터 저장..  ===== =====');
    this.snapshot('content').call();
    this.graphTimer = setInterval(this.graphScreenShot.bind(this), 1000);
  }

  saveResultDB(graphImg) {
    const { report } = this.state;
    const { OperBP, main } = this.props;
    const { mode, packets, curBpVal, curAnswr, answ, setting } = OperBP;
    const arrImgJson = `[ "${report.overall}", "${graphImg}" ]`;
    const restData = { mode, arrImgJson,
      stdntInfo: main.stdName, email: main.stdEmail, totalScore: Number(0) };

    const curSetting = mode === 'REAL' ? setting.real : setting.train;
    const theRest = JSON.stringify({ curBpVal, curAnswr, answ, curSetting });

    M.RmBpResult.newBpResult(restData, packets, theRest); // DB 에 저장..
  }

  graphScreenShot() {
    console.log('   tick function ');
    this.snapshot('graph').call();
    clearInterval(this.graphTimer);
  }

  snapshot = refname => () =>
  //snapshot(refname) {
    takeSnapshot(this.refs[refname], this.state.value)
    .then(res =>
      this.state.value.result !== 'file' ?
      res :
      new Promise((success, failure) =>
      // just a test to ensure res can be used in Image.getSize
        Image.getSize(
          res,
          (width, height) => (
            console.log(res, width, height),
            success(res)
          ),
          failure)
      )
    )
    .then(res => {
      const { report } = this.state;
      console.log('\n\n\n\n\n      takeSnapshot   then ....');
      console.log(res);
      this.setState({
        error: null,
        res,
        previewSource: {
          uri: this.state.value.result === "base64" ?
          "data:image/"+this.state.value.format+";base64,"+res : res
        }
      });

      if (report.overall === null) {
        this.setState({
          report: { ...report, overall: res }
        });
      } else {
        this.saveResultDB(res);
        Actions.popTo('sceneMain');
      }
    })
    .catch(error => (console.warn(error), this.setState({ error, res: null, previewSource: null })));

  renderItemTitle() {
    const { uicommon, langKey } = this.props.uiState;
    const { addManualItem } = uicommon[langKey];

    return (
      <View style={{ flex: 1 }}>
        <Text> total score </Text>
        <HtButton
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: '#208'
          }}
          width={100} fWidth={1} ptext={addManualItem}
          onPressCallback={() => {
            this.setState({ expanded: false });
          }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   트레이닝 체크 뷰
  renderTrainingScore(n, title, rslt, setObj) {
    const ptunit = this.state.lang.scoreUnit;
    const viewSty = { flex: 1, flexDirection: 'row', borderBottomWidth: 1, margin: 2 };
    const ttlSty = { color: '#224', fontWeight: '500', fontSize: 15 };
    const scrSty = { color: '#224', fontWeight: '400', fontSize: 14 };

    const rsltText = rslt ? 'O' : 'X';

    return (
      <View style={viewSty} key={n} >
        <HtText
          mandator={{ flex: 5, rad: 5, text: title, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: ttlSty }}
        />
        <HtText
          mandator={{ flex: 4, rad: 5, text: rsltText, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: scrSty }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   리얼 테스트 점수 뷰
  renderRealScore(n, title, score, assign) {
    const ptunit = this.state.lang.scoreUnit;
    const viewSty = { flex: 1, flexDirection: 'row', borderBottomWidth: 1, margin: 2 };
    const ttlSty = { color: '#224', fontWeight: '500', fontSize: 15 };
    const scrSty = { color: '#224', fontWeight: '400', fontSize: 14 };

    return (
      <View style={viewSty} key={n} >
        <HtText
          mandator={{ flex: 5, rad: 5, text: title, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: ttlSty }}
        />

        <HtText
          mandator={{ flex: 4, rad: 5, text: `${score} / ${assign} ${ptunit}`, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: scrSty }}
        />
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   폐기 대상...
  getRealTestScore() {
    const { mode, setting, uxOnce } = this.props.OperBP;
    const { touched, cuffed } = uxOnce;
    const { real } = setting;
    const { arrScore } = real;

    return [1, 2, 3,
      cuffed ? arrScore[3] : 0, // 커프 설치 점수
      touched ? arrScore[4] : 0,
      6, 7, 8, 9];
  }

  ////////////////////////////////////////////////////   _//////////////////_   점수 / 평가 뷰 Loop
  renderRightColumn() {
    const { setting, uxOnce, newResult: r } = this.props.OperBP;
    const { real, train } = setting;
    //const { finScrs } = newResult;
    const { langKey } = this.props.uiState;
    const { bpTrainChkLst, bpRealTstChkLst } = langBp[langKey];
    // [ "커프 설치", "상완 촉진", "청진 간극", "적정 가압", "적정 감압 속도", "실습 시간 준수" ],
    const { realTestMode, realTestScore } = this.state;
    const checkListTitle = realTestMode ? bpRealTstChkLst : bpTrainChkLst;

    let rCompo = [];

    for (let k = 0; k < checkListTitle.length; k++) {
      const obj = checkListTitle[k];
      if (realTestMode) {
        rCompo = [...rCompo, this.renderRealScore(k, obj, r.finScrs[k], real.arrScore[k])];
      } else {
        rCompo = [...rCompo, this.renderTrainingScore(k, obj, r.finScrs[k], train)];
      }
    }
    return rCompo;
  }

  renderContent() {
    //const { scr } = this.props.main;
    const { curBpVal, curAnswr, bpLogic } = this.props.OperBP;
    const { highDE, lowDE, bpmDE } = bpLogic.getDifference(curBpVal, curAnswr);
    //const { maxRslt, minRslt, bpmRslt } = newResult;
    const { langKey } = this.props.uiState;
    const { resultBPShrink, resultBPRelax, pulseNum, setValue, chkValue,
      missValue, exactNess } = langBp[langKey];

    return (
      <View style={{ flex: 6, alignItems: 'stretch', justifyContent: 'center', alignSelf: 'stretch' }} >
        <ResultUnitView
          mandator={{ title: resultBPShrink }}
          details={[
            { num: curBpVal.high, unit: 'mmHg', expl: setValue },
            { num: curAnswr.high, unit: 'mmHg', expl: chkValue },
            { num: highDE.diff, unit: 'mmHg', expl: missValue },
            { num: highDE.cor, unit: '%', expl: exactNess },
          ]}
        />
        <View style={sty.seperateLine} />
        <ResultUnitView
          mandator={{ title: resultBPRelax }}
          details={[
            { num: curBpVal.low, unit: 'mmHg', expl: setValue },
            { num: curAnswr.low, unit: 'mmHg', expl: chkValue },
            { num: lowDE.diff, unit: 'mmHg', expl: missValue },
            { num: lowDE.cor, unit: '%', expl: exactNess },
          ]}
        />
        <View style={sty.seperateLine} />
        <ResultUnitView
          mandator={{ title: pulseNum }}
          details={[
            { num: curBpVal.bpm, unit: 'bpm', expl: setValue },
            { num: curAnswr.bpm, unit: 'bpm', expl: chkValue },
            { num: bpmDE.diff, unit: 'bpm', expl: missValue },
            { num: bpmDE.cor, unit: '%', expl: exactNess },
          ]}
        />
      </View>
    );
  }

  renderGraph() {
    return (
      <View style={{ flex: 59, backgroundColor: '#EEE' }} >
        <View style={{ flex: 3 }} />
        <BpGraph flex={11} graphOnly ref='graph' />
        <View style={{ flex: 3 }} />
      </View>
    );
  }

  renderContentTotal() {
    const setObj = M.getCurBpSettingObj(this.props.OperBP);
    const { langKey } = this.props.uiState;
    const { touch, hearing, combine } = langBpSetting[langKey];

    // 모드 : 촉진 청음...
    const { method } = setObj;
    let methodStr = '촉진';
    switch (method) {
      case 0: methodStr = touch; break;
      case 1: methodStr = hearing; break;
      default: methodStr = combine; break;
    }

    return (
      <View style={{ flex: 59, backgroundColor: '#FFF' }} ref='content'>
        <HsStudentExamVw flex={9} modeCase={methodStr} />
        <View style={{ flex: 50, flexDirection: 'row' }} >
          {this.renderContent()}
          <View style={{ flex: 3 }} >
            {this.renderRightColumn()}
          </View>
        </View>
      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { report } = this.state;
    const { mode } = this.props.OperBP;
    const { langKey } = this.props.uiState;
    const { resultTrainBP, resultRealTestBP } = langBp[langKey];
    const { realTestMode } = this.state;

    let mainView = this.renderGraph();
    if (report.overall === null) {
      mainView = this.renderContentTotal();
    }

    const title = (mode === 'REAL') ? resultRealTestBP : resultTrainBP;

    return (
      <View style={{ flex: 100, backgroundColor: '#FFF' }}>
        <MsTitleBar
          titleA={title}
          back={'Back'}
          callback={() => { this.setState({ realTestMode: !realTestMode }); }}
        />

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />

        {mainView}

        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />



        <View style={{ flex: 8, backgroundColor: '#22F' }} >
          <HsControlBar
            flex={10}
            scrH={this.props.main.scr.height}
            onSavePress={this.onSavePress.bind(this)}
          />
        </View>

      </View>
    );
  }
} // decimal-pad

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  titleSty: { color: HtColor.bgDarkGray, fontWeight: '500', fontSize: 25 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
  seperateLine: { height: 2, backgroundColor: '#AAAAAA' },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    OperBP: state.OperBP,
    chkItems: state.chkItems,
    theCounter: state.timerCount,
    uiState: state.uiState
    //arrPacket: state.arrPacket,
  };
};

export default connect(mapStateToProps, actions)(ResultView);
