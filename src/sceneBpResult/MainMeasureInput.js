/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / sceneBpResult / MainMeasureInput.js
 *
 * Created by Jongwoo Moon on 2017. 4. 6..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, Text, StyleSheet, Switch } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../actions';
import { HtText, HtButton, HtColor } from '../uicompo';
import MsTitleBar from '../simulations/MsTitleBar';
import { MsTitleInputUnitView, HsControlBar } from '../simulations';

const langBp = require('../data/langBp.json');
// import TrainingSetting from './TrainingSetting';

class MainMeasureInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      realTestMode: true
    };
  }

  componentWillMount() {
    this.props.changeMainState('init');
  }

  componentWillUnmount() {
    console.log('\n\n\n\n\n ====== ====== ====== ======  [[ SceneMain :: unmount ]] ...\n');
  }

  renderItemTitle() {
    const { uicommon, langKey } = this.props.uiState;
    const { addManualItem } = uicommon[langKey];

    return (
      <View style={{ flex: 1 }}>
        <Text> total score </Text>
        <HtButton
          txtStyle={{
            fontWeight: '800', fontSize: 15, color: '#208'
          }}
          width={100} fWidth={1} ptext={addManualItem}
          onPressCallback={() => {
            this.setState({ expanded: false });
          }}
        />
      </View>
    );
  }

  renderContent() {
    const { langKey } = this.props.uiState;
    const { shrinkBp, relaxBp, pulseNum, touchSlip } = langBp[langKey];
    const { mode, setting, curAnswr } = this.props.OperBP;
    const { high, low, bpm, periodGap } = curAnswr;
    const curSetObj = (mode === 'REAL') ? setting.real : setting.train;
    const isTouchCase = (curSetObj.method === 0); // 촉진법 (0)일 경우.... 아닌 경우는 모두 false.
    const relaxView = isTouchCase ? null :  // 이완 혈압 적어 넣기....
      this.renderInputView(relaxBp, 'mmHg', `${low}`,
      (txt) => {
        if (!isNaN(Number(txt))) { // 숫자로 파싱 ..
          this.props.setBpAnswr({ low: Number(txt) });
        }
      }),
      touchDistVw = isTouchCase ? null :  // 청진 간극이 있었는 지 없었는 지 선택 액션..
      this.renderTouchDistanceView(touchSlip, periodGap,
        (val) => {
          console.log(`  청진간극 값 선택 : ${val} `);
          this.props.setBpAnswr({ periodGap: val });
        }
      );

    return (
      <View
        style={{
          flex: 90,
          alignItems: 'stretch', justifyContent: 'center',
          alignSelf: 'stretch'
        }}
      >
        <View style={{ flex: 1 }} />

        {this.renderInputView(shrinkBp, 'mmHg', `${high}`, (txt) => {
          if (!isNaN(Number(txt))) { // 숫자로 파싱 ..
            this.props.setBpAnswr({ high: Number(txt) });
          }
        })}

        {relaxView}

        {this.renderInputView(pulseNum, 'bmp', `${bpm}`, (txt) => {
          if (!isNaN(Number(txt))) { // 숫자로 파싱 ..
            this.props.setBpAnswr({ bpm: Number(txt) });
          }
        })}

        {touchDistVw}

        <View style={{ flex: 3 }} />
      </View>
    );
  }

  renderInputView(title, unit, value, onChange) {
    const vwSty = { margin: 10 }, flexCont = [5, 3, 2];
    return (
      <MsTitleInputUnitView
        mandator={{
          keyboardType: 'numeric', unit, title
        }}
        flexContent={flexCont}
        vwSty={vwSty}
        value={value}
        onChange={onChange}
      />

    );
  }

  renderTouchDistanceView(touchSlip, boolVal, onValueChange) {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <HtText
          mandator={{ flex: 5, rad: 5, text: touchSlip, margin: { }, radIgnore: true }}
          style={{ viewStyle: sty.vw, textStyle: sty.titleSty }}
        />
        <Switch
          disabled={false}
          style={{ flex: 5, alignSelf: 'center' }}
          value={boolVal}
          onValueChange={onValueChange}
        />
      </View>
    );
  }

  onSavePress() {
    this.props.bpFinishExam();
    Actions.sceneBpResultView();
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { langKey } = this.props.uiState;
    const { measureTitle } = langBp[langKey];
    const { realTestMode } = this.state;

    return (
      <View style={{ flex: 100 }} >
        <MsTitleBar
          titleA={measureTitle}
          back={'Back'}
          callback={() => { this.setState({ realTestMode: !realTestMode }); }}
        />

        {this.renderContent()}

        <HsControlBar
          flex={10}
          scrH={this.props.main.scr.height}
          onSavePress={this.onSavePress.bind(this)}
        />

      </View>
    );
  }
} // decimal-pad

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: { // {  alignItems: 'stretch', flex: 100 }
    flex: 70, flexDirection: 'row',
    alignSelf: 'stretch',
    padding: 3,
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowContainer: {
    flex: 10, flexDirection: 'row', alignSelf: 'stretch',
    alignItems: 'stretch',
    height: 40
  },
  vw: { margin: 3 },
  txt: { color: '#EEE', fontWeight: '800', fontSize: 14 },
  titleSty: { color: HtColor.bgDarkGray, fontWeight: '500', fontSize: 25 },
  buttonLabel: {
    fontWeight: '400', textAlign: 'center',
  },
  title: {
    flex: 10,
    textAlign: 'center',
    color: 'black'
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    OperBP: state.OperBP,
    chkItems: state.chkItems,
    theCounter: state.timerCount,
    uiState: state.uiState
    //arrPacket: state.arrPacket,
  };
};

export default connect(mapStateToProps, actions)(MainMeasureInput);
