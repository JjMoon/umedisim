/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / TimerReducer.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

// import data from '.Lib.json'; // json 으로부터 읽기.
//export default () => [{ counter: 0 }];
export default (state = 0, action) => {
  // if (state == null) {
  //   state = { timerCount: 0 };
  // }
  //console.log("TimerReducer:: action >>" + action.payload);

  switch (action.type) {
    case 'increaseCounter':
      return state + action.payload;
    default:
      return state;
  }
};
