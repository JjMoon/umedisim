/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / BreathReducer.js
 *
 * Created by Jongwoo Moon on 2017. 2. 20..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import _ from 'lodash';
import BreathLogic from './BreathLogic';

// 500 ~ 600 정도가 정상임..
const thHold = 100, countMin = 300;

export default (state = [], action) => {
  const { type, payload } = action;
  switch (type) {
    case 'startStopExamin':
      console.log(`\n\n\n\n  BreathReducer >> startStopExamin ${action.payload}`);
      if (action.payload) { // 시작할 때 리셋.
        return [];
      }
      return state;
    case 'resetEttExam':
      return [];
    case 'newBlePacket':
      // console.log(` Volumn : ${action.payload}`);
      const vol = payload.blly;
      console.log(`\n\n  Breath Reducer .. volumn : ${vol}  [BreathLogic] :: ${state.length}`, payload);

      if (state.length === 0 && vol < 10) { // 아직 시작 전.. 스킵.
        return [];
      }
      if (state.length === 0) {
        const firstMemeber = new BreathLogic();
        firstMemeber.process(vol);
        return [firstMemeber];
      }
      const lst = _.last(state);
      // console.log(`   current state ID ::  ${lst.state.stateID}`);
      if (lst.process(thHold, countMin, vol)) {
        return [...state, new BreathLogic()];
      }
      // console.log(action.payload);
      return state;
    default:
      return state;
  }
};
