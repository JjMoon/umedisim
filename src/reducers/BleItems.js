/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / MainState.js
 *
 * Created by Jongwoo Moon on 2017. 1. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

export default (state = [], action) => {
  switch (action.type) {
    case 'newBlePeripheral':
      if (state === null) {
        return [action.payload];
      }
      return [...state, action.payload];
    default:
      return state;
  }
};
