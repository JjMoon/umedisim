/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / UiReducer.js
 *
 * Created by Jongwoo Moon on 2017. 2. 1. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import _ from 'lodash';

const langMain = require('../data/langMain.json');
const langOper = require('../data/langOper.json');
const langSetting = require('../data/langSetting.json');
const uicommon = require('../data/uicommon.json');
const langAutoChk = require('../data/langAutoCheckItems.json');

const arrLangKey = ['kr', 'en_US'];

const initState = { viewState: null,
  langKey: 'kr', langIdx: 0, ettDevNum: 12345, bpDevNum: 54321,
  langMain, langOper, langSetting, uicommon, langAutoChk,
  img: { neck: 'F', insert: 'NORM', gullet: false, teeth: 0, chinPush: false,
    bronchus: false, bellyMask: false, bellyBronchus: false,
  }, // P F A, NORM, UNDR, OVER
  set: { autoCheckItem: {}, manCheckItem: [], general: [] }, ettLogic: null,
};

// 세팅..
// autoCheckItem: {}, manCheckItem: [], general: []

export default (state = initState, action) => {
  const { ettLogic } = state;

  switch (action.type) {
    case 'propagateSetting':
      console.log('propagateSetting');
      console.log(action.payload);
      console.log(action.ettLogic);
      return { ...state, set: action.payload, ettLogic: action.ettLogic };
    case 'setBleDevNum':
      return { ...state, ...action.payload }; // { ettDevNum: 11223 } 이런 식...
    case 'languageChange':
      console.log(`\n\n\n\n\n  languageChange : ${action.payload}  ${arrLangKey[action.payload]}`);
      return { ...state, langKey: arrLangKey[action.payload], langIdx: action.payload };
      // langIdx: state.langIdx + 1 };
    case 'viewLayoutKind':
      // console.log(`PacketReducer.js || Packet Number >> ${state.length}`);
      return { ...state, viewState: action.payload };
    case 'newBlePacket':
      return graphicProcess(ettLogic, action.payload, state);
    default:
      return state;
  }
};

function graphicProcess(ettLogic, pack, state) {
  const { thr, glcnbl, nckAng, nckDst, tth, blly } = pack;

  const chinPush = glcnbl[1]; // 턱들기 성공..
  console.log(`   호흡 : ${blly}`);
  const neck = ettLogic.getNeckStr(nckAng, nckDst);
  // 삽관 정보 : 부족, 정상, 과다..
  const insert = ettLogic.getInsertStr(thr);
  // 식도 삽관.
  const gullet = ettLogic.gullet(glcnbl);
  // 이빨 압력 %로 표시.
  const teeth = ettLogic.getTeethPercent(tth);
  // 복부 팽만 (백밸브 마스크)
  const bellyMask = ettLogic.getStomachMask(glcnbl);
  const bellyBronchus = ettLogic.getStomachGullet(glcnbl);
  const bronchus = glcnbl[0];

  //
  const img = { neck, insert, gullet, teeth, chinPush, bronchus, bellyMask, bellyBronchus };
  return { ...state, img };
}
