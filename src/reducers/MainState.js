/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / MainState.js
 *
 * Created by Jongwoo Moon on 2017. 1. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.

{
  mainState: 'init' (not connected) 'cali' 'play' 'result'  ::  큰 분류의 상태.
  isplaying: boolean :: 'play' 에서 play(true) | pause 구별.
  ux : { neckAngle : 25 }
  operatingState: 0,
  result: [ 0, 0 ... ]
}
  패킷 형태 :
  "thr" : [ false, false, false ],
  "glcnbl": [ false, false, false ],
  "nckAng": 3, "nckDst": 4, "blly": 100, "tth": 2
 */
'use strict';

import { Dimensions } from 'react-native';

////////////////////////////////////////////////////   _//////////////////_     초기 State
const initState = {
  mainState: 'init', ettMode: 'NONE', connETT: false, connBP: false, // 'ETT'  'LMA'
  curReport: { ETT: null, BP: null },
  bleCompo: null,
  stdName: 'J. Doe', stdEmail: 'abc@gmail.com',
  //setting: { calisetting: null, autoItem: null, manualItem: null },
  debugMode: false,
  startedAt: new Date(), endedAt: new Date(), examListMode: false,
  operatingState: 0,
  result: [0, 0, 0, 0, 0, 0, 0],
  scr: { width: 10, height: 10, unit: 5 },
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'init':
      const { width, height } = Dimensions.get('window');
      const unit = (width > height ? height : width) * 0.01;
      console.log(`App Started  >>> reducers Screen : ${width} X ${height}`);
      return { ...initState, scr: { width, height, unit } };
    case 'mainState': // 전체 상태 표시
      // init, oper, exam, setting, bpTrain, bpReal
      return { ...state, mainState: action.payload };
    case 'setBleCompo': return { ...state, bleCompo: action.payload };
    case 'connected': return { ...state, ...action.payload }; // connETT connBP
    case 'setEttMode':
      console.log(`\n\n\nsetEttMode ::  ${action.mode}`);
      return { ...state, ettMode: action.mode }; // 'ETT'  'LMA'

    case 'setCurReport': return { ...state, curReport: action.payload }; // sceneReport 에서 보이는 객체 세팅.

    case 'editStdName': return { ...state, stdName: action.payload };
    case 'editStdEmail': return { ...state, stdEmail: action.payload };
    case 'checkProcess':
      const { operatingState, result } = state;
      const { newOperatState, resultAfter } = checkProcess(operatingState, result, action.payload);
      return { ...state, operatingState: newOperatState, result: resultAfter };
    case 'startStopExamin':
      console.log(`\n\n\n\n\n   startStopExamin   ${action.payload}`);
      if (action.payload) {
        return { ...state, startedAt: new Date() };
      }
      return { ...state, endedAt: new Date() };    // 여기서 교육 데이터 저장. 등 처리..
    case 'resetEttExam':
      return { ...state, startedAt: new Date() };

    case 'examReplay': // Exam Replay 버튼 눌림.
      return { ...state, startedAt: new Date() };
    default:
      return state;
  }
};


function checkProcess(operIdx, rslt, pck) {
  console.log(`checkProcess   operIdx : ${operIdx} `);
  console.log(pck);
  let packVal, newOperatState = operIdx;
  const resultAfter = rslt;

  console.log(`기도확보 : 0xBB >> ${pck.nckAng}`);
  switch (operIdx) {
    case 0:
      packVal = pck.sen1;
      break;
    case 1:
      packVal = pck.sen2;
      break;
    case 2:
      break;
    default:
      break;
  }

  switch (operIdx) {
    case 0:
    case 1:
    case 2:
      if (packVal > 80) {
        resultAfter[operIdx] = 1;
        newOperatState++;
        console.log(`\n\n\n\n\n ====== [[ MainState :: checkProcess ]] ... newOperatState : ${newOperatState} \n`);
      }
      break;
    default:
  }
  console.log(`newOperatState : ${newOperatState}`);
  return { newOperatState, resultAfter };
}
