/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / PacketReducer.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

function addTimeStamp(pObj) {
  const dateNow = new Date();
  return { ...pObj, tsObj: dateNow, ts: dateNow.toString() };
}

export default (state = [], action) => {
  const { type, payload } = action;

  switch (type) {
    case 'newBlePacket': // 'newBlePacket' action
      return [...state, addTimeStamp(payload)];
    case 'startStopExamin':
      if (action.payload) {
        return [];
      }
      return state;
    case 'resetEttExam':
      return [];
    case 'examReplay': // Exam Replay 버튼 눌림.
      return [];
    case 'packetExam':
      const newObj = addTimeStamp(action.payload);
      return [...state, newObj];
    default:
      return state;
  }
};
