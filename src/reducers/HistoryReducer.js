/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / MainState.js
 *
 * Created by Jongwoo Moon on 2017. 1. 16..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.

  패킷 형태 :
  "thr" : [ false, false, false ], "glcnbl": [ false, false, false ], "nckAng": 3, "nckDst": 4, "blly": 100, "tth": 2
 */


import _ from 'lodash';
import deepcopy from 'deepcopy';

const chks = require('../data/CheckItemsDefault.json');

const initState = {
  playMode: false, startedAt: null, ettLogic: null,
  ux: {
    thrStr: '삽관 정보',
    neckAngle: false, chinPush: false, neckHeight: false,
    checkTeeth: false, bronchus: false,
    bellyMask: false, bellyBronchus: false,
  },
  uxRemain: { bellyMask: false, bellyBronchus: false },
  history: [], curPacket: null,
  onceChk: { secureRespTrack: false, pushChin: false, snippingPosition: false,
    brokenTooth: false, stomachMask: false, stomachGullet: false },
  brthInfo: {
    count: 0, passNum: 0, overNum: 0, belowNum: 0, avrVol: 0, avrExhale: 0, respPerMin: 0,
    exPass: 0, exOver: 0, exBelow: 0, prdPass: 0, prdOver: 0, prdBelow: 0,
  },
  replay: {
    exam: null, idx: 0,
    examStarted: 0, replayStarted: 0, // 시작 시간.
    lagTime: 0, curPacketLag: 0,
  },
  set: { autoCheckItem: {}, manCheckItem: [], general: [] },
};

function newInit() {
  // return JSON.parse(JSON.stringify(initState));
  return deepcopy(initState);
}

export default (state = {}, action) => {
  const { replay, onceChk, ettLogic } = state;
  let { history } = state, v, newHistory;
  switch (action.type) {
    case 'init':
      console.log('App Started  >>> HistoryActions');
      // const initStt = JSON.parse(JSON.stringify(initState));
      return newInit(); // _.defaults(initState); // initState;
    case 'propagateSetting':
      console.log('propagateSetting');
      console.log(action.payload);
      console.log(action.ettLogic);
      return { ...state, set: action.payload, ettLogic: action.ettLogic };
    case 'newBlePacket':
      return { ...state, ...historyOnceProcess(ettLogic, action.payload, onceChk, history) };
    case 'addHistory': // 매뉴얼 체크 할 때 발생하는 액션..
      history = [...history, { ...action.payload, ts: new Date() }];
      return { ...state, history };
    case 'examListViewMode': // true / false 에 관계 없이 리스트는 깨끗하게 세팅..
      return newInit();
    case 'resetEttExam':
      return newInit();

    case 'startStopExamin':
      console.log(`\n\n\n\n\n  HistoryReducer   start stop exam ::  ${action.payload}`);
      if (action.payload) { // Started
        return { ...state, playMode: action.payload, startedAt: new Date(), history: [], onceChk: {} };
      }
      const { brokenTooth, stomachMask, stomachGullet } = onceChk;

      newHistory = history;
      // 치아 파열 확인..
      if (!brokenTooth) { // 파열 없슴.
        newHistory = [...newHistory, getNewAutoHistory('brokenTooth', ettLogic)];
      }
      if (!stomachMask) { // // 복부 팽만 (백밸브 마스크)
        newHistory = [...newHistory, getNewAutoHistory('stomachMask', ettLogic)];
      }
      if (!stomachGullet) { // 복부 팽만 (식도 삽관)
        newHistory = [...newHistory, getNewAutoHistory('stomachGullet', ettLogic)];
      }
      return { ...state, playMode: action.payload, history: newHistory };
    case 'makeBrthHistory':
      console.log(` \n makeBrthHistory :: breath 객체 갯수 : ${action.payload.length}`);
      const arrBrth = _.filter(action.payload, o => o.state.finished),
        num = arrBrth.length;
      newHistory = history;

      if (num === 0) { return state; } // 호흡이 없을 때.. 점수 반영 .. 생략..

      // 기본 값 산출...
      const { startedAt } = state;
      const maxVolMean = _.chain(arrBrth)
        .map(o => o.state.maxVol)
        .reduce((sum, n) => sum + n, 0) / num;
      const endedAt = new Date(), examTimeMin = (endedAt - startedAt) * (0.001 / 60),
        respPerMin = num / examTimeMin;
      console.log(`   호흡 횟수 : ${num}   호흡량 평균 : ${maxVolMean}    분당 호흡 횟수 : ${respPerMin} `);

      let brthInfo = { count: num, respPerMin };

      // 호흡 횟수 정확도
      let { active, score } = ettLogic.getAutoCheckItem('brthCount');
      if (active) { //
        const ratio = ettLogic.getBrthCountScoreFactor(num);
        const { shr } = ettLogic.getItemString('brthCount');
        console.log(`\n ----------------  ${shr}  ${score} * ${ratio}   호흡 횟수 정확도 : ${num}`);
        newHistory = [...newHistory,
          getNewItem('brthCount', true, true, shr, _.round(score * ratio, 0))];
      }

      // 호흡량 정확도
      ({ active, score } = ettLogic.getAutoCheckItem('brthVolumn'));
      if (active) { //
        const arrVolumn = _.map(arrBrth, o => o.state.maxVol);
        const { ratio, passNum, overNum, belowNum } =
          ettLogic.getBrthVolumnRatio(arrVolumn);
        const avrVol = _.reduce(arrVolumn, (sum, n) => sum + n, 0) / num;
        const { shr } = ettLogic.getItemString('brthVolumn');
        brthInfo = { ...brthInfo, passNum, overNum, belowNum, avrVol };
        newHistory = [...newHistory,
          getNewItem('brthVolumn', true, true, shr, _.round(score * ratio, 0))];
        console.log(`\n ----------------  ${shr}  ${score} * ${ratio}   호흡량 정확도`);
      }

      // 호기 시간 정확도
      ({ active, score } = ettLogic.getAutoCheckItem('exhale'));
      if (active) { //
        const { ratio, passNum, overNum, belowNum, avrExhale } =
          ettLogic.getBrthExhale(arrBrth);
        const { shr } = ettLogic.getItemString('exhale');
        brthInfo = { ...brthInfo, exPass: passNum, exOver: overNum, exBelow: belowNum, avrExhale };
        newHistory = [...newHistory,
          getNewItem('exhale', true, true, shr, _.round(score * ratio, 0))];
        console.log(`\n ----------------  ${shr}  ${score} * ${ratio}   호기 시간 정확도`);
      }

      // 호흡 시간 편차
      ({ active, score } = ettLogic.getAutoCheckItem('brthTimeDiff'));
      if (active) { //
        const { ratio, passNum, overNum, belowNum } =
          ettLogic.getBrthDiff(arrBrth);
        const { shr } = ettLogic.getItemString('brthTimeDiff');
        brthInfo = { ...brthInfo, prdPass: passNum, prdOver: overNum, prdBelow: belowNum };
        console.log(`  xx150 ${passNum} `);

        newHistory = [...newHistory,
          getNewItem('brthTimeDiff', true, true, shr, _.round(score * ratio, 0))];
      }

      // 호흡간 시간 간격  brthPeriod
      ({ active, score } = ettLogic.getAutoCheckItem('brthPeriod'));
      if (active) { //
        const ratio = ettLogic.getBrthPeriod(arrBrth);
        const { shr } = ettLogic.getItemString('brthPeriod');
        newHistory = [...newHistory,
          getNewItem('brthPeriod', true, true, shr, _.round(score * ratio, 0))];
      }

      // 제한 시간 초과 여부   timeOutLimit
      ({ active, score } = ettLogic.getAutoCheckItem('totalTime'));
      if (active) {
        const ratio = ettLogic.getTimeoutRatio(state.startedAt);
        const { shr } = ettLogic.getItemString('totalTime');
        newHistory = [...newHistory,
          getNewItem('totalTime', true, true, shr, _.round(score * ratio, 0))];
      }
      return { ...state, history: newHistory, brthInfo };
    case 'examReplay': // Exam Replay 버튼 눌림.
      console.log('initState.uxRemain');
      // console.log(initState);
      const nObj = newInit();
      return { ...state,
        playMode: true,
        replay: {
          exam: action.payload, idx: 0,
          examStarted: (new Date(action.payload.startTime)).getTime(),
          replayStarted: (new Date()).getTime(),
        },
        history: [], ux: nObj.ux,
        uxRemain: nObj.uxRemain, // { bellyMask: false, bellyBronchus: false }
      };
    case 'examReplayStop':
      return { ...state,
        playMode: false,
        replay: {
          exam: null, idx: 0, examStarted: 0, replayStarted: 0,
        },
        history: [],
      };

    case 'replayTick':
      const { exam, idx, examStarted, replayStarted } = replay;
      const nowInMS = (new Date()).getTime();
      const lagTime = nowInMS - replayStarted;
      // exam.packets[idx].showMyself();
      const curPacketLag = exam.packets[idx].getMyTS() - examStarted;
      // console.log(`  time   ${lagTimeInSec}  ${curPckActionTime} `);
      return { ...state, replay: { ...state.replay, lagTime, curPacketLag } };
    case 'examReplayNext':
      if ((replay.idx + 1) === replay.exam.packets.length) {
        console.log('\n\n\n     Finished   ............\n\n\n');
        return { ...state, replay: { ...state.replay, exam: null, idx: 0 } };
      }
      return { ...state, replay: { ...state.replay, exam: replay.exam, idx: replay.idx + 1 } };
    default:
      return state;
  }

  function getNewAutoHistory(key, ettL) {
    const { score } = ettL.getAutoCheckItem(key);
    const { shr } = ettL.getItemString(key);
    return getNewItem('brokenTooth', true, true, shr, score);
  }

  function getNewItem(key, auto, pass, shr, score) {
    console.log('\n\n\n  New History Item Added   \n\n\n');
    return { key, auto, pass, shr, score, ts: (new Date()) };
  }

  function historyOnceProcess(ettLog, pack, pOnceChk, pHistory) {
    const { glcnbl, nckAng, nckDst, tth } = pack;
    const { secureRespTrack, pushChin, snippingPosition, brokenTooth,
      stomachMask, stomachGullet } = pOnceChk;
    let newOnceChk = onceChk, newHistory = pHistory;

    // 기도 확보
    if (!secureRespTrack) {
      const { active, score } = ettLog.getAutoCheckItem('secureRespTrack');
      if (active && ettLog.getNeckStr(nckAng, nckDst) !== 'F') {
        const { shr } = ettLog.getItemString('secureRespTrack');
        newOnceChk = { ...newOnceChk, secureRespTrack: true };
        newHistory = [...newHistory,
          getNewItem('secureRespTrack', true, true, shr, score)];
      }
    }
    // 턱 들기  glcnbl[1]
    if (!pushChin) {
      const { active, score } = ettLog.getAutoCheckItem('pushChin');
      if (active && glcnbl[1]) { // 턱들기 성공..
        const { shr } = ettLog.getItemString('pushChin');
        newOnceChk = { ...newOnceChk, pushChin: true };
        newHistory = [...newHistory,
          getNewItem('pushChin', true, true, shr, score)];
      }
    }
    // 스니핑..
    if (!snippingPosition) {
      const { active, score } = ettLog.getAutoCheckItem('snippingPosition');
      if (active && ettLog.getNeckStr(nckAng, nckDst) === 'P') { // 스니핑
        const { shr } = ettLog.getItemString('snippingPosition');
        newOnceChk = { ...newOnceChk, snippingPosition: true };
        newHistory = [...newHistory,
          getNewItem('snippingPosition', true, true, shr, score)];
      }
    }
    // 치아 파열
    newOnceChk = { ...newOnceChk, brokenTooth: ((ettLog.getTeethPercent(tth) >= 100) ? true : brokenTooth) };
    // 복부 팽만 (백밸브 마스크)
    newOnceChk = { ...newOnceChk, stomachMask: ((ettLog.getStomachMask(glcnbl)) ? true : stomachMask) };
    // 복부 팽만 (식도 삽관)
    newOnceChk = { ...newOnceChk, stomachGullet: ((ettLog.getStomachGullet(glcnbl)) ? true : stomachGullet) };

    return { history: newHistory, onceChk: newOnceChk };
  }

  // switch (action.type) {
  // case 'newBlePacket':
  //   let thrStr = '-';
  //   ux.curPacket = action.payload;
  //   const { thr } = action.payload;
  //   if (thr[2]) {
  //     thrStr = '과다 삽관';
  //   } else if (thr[1]) {
  //     thrStr = '정상 삽관';
  //   } else if (thr[0]) {
  //     thrStr = '삽관 부족';
  //   } else {
  //     thrStr = '삽관';
  //   }
  //   ux.thrStr = thrStr;
  //   return { ...state, ux };

  // case 'neckAngle':
  //
  //   console.log(' neck angle at history');
  //   v = (action.payload > 30);
  //   if (v !== ux.neckAngle) {
  //     history = [...history, newItem(0, v)];
  //   }
  //   ux.neckAngle = v;
  //   return { ...state, ux, history };
  // case 'chinPush':
  //   v = action.payload;
  //   if (!v) {
  //     return state; // 턱 밀기는 합격만 있슴.
  //   }
  //   if (v !== ux.chinPush) {
  //       history = [...history, newItem(1, v)];
  //   }
  //   ux.chinPush = v;
  //   return { ...state, ux, history };
  // case 'neckHeight':
  //   v = (action.payload > 70); // 스니핑 포지션
  //   if (v !== ux.neckHeight) {
  //     history = [...history, newItem(2, v)];
  //   }
  //   ux.neckHeight = v;
  //   return { ...state, ux, history };
  // case 'teethPress':
  //   v = (action.payload > 45);
  //   if (v !== ux.checkTeeth && v) { // 이빨이 이상이 있을 때만 기록..
  //     history = [...history, newItem(3, v)];
  //   }
  //   ux.checkTeeth = v;
  //   return { ...state, ux, history };
  // case 'bronchus':
  //   v = action.payload;
  //   if (v !== ux.bronchus && v) {
  //      history = [...history, newItem(4, v)];
  //   }
  //   ux.bronchus = v;
  //   return { ...state, ux, history };
  // case 'checkBelly': // glcnbl
  //   const aa4 = action.payload[0], aa6 = action.payload[2];
  //   const vMask = aa6 && !aa4, vBron = aa6 && aa4;
  //   if (vMask && vMask !== ux.bellyMask) {
  //     history = [...history, newItem(5, v)];
  //   }
  //   if (vBron && vMask !== ux.bellyBronchus) {
  //     history = [...history, newItem(6, v)];
  //   }
  //
  //   if (vMask) { // 두 조건이 맞을 때..
  //     uxRemain.bellyMask = true;
  //   }
  //   if (vBron) { // 두 조건이 맞을 때..
  //     uxRemain.bellyBronchus = true;
  //   }
  //   ux.bellyMask = vMask;
  //   ux.bellyBronchus = vBron;
  //   return { ...state, ux, history, uxRemain };
  // default:
  //   return state;
  // }
};


// History에 항목 추가..
// function newItem(idx, value) {
//   return { ...chks[idx], pass: value, scoreMarked: chks[idx].score, ts: new Date() };
// }
