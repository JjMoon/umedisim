/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / index.js
 *
 * Created by Jongwoo Moon on 2017. 1. 10..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
import { combineReducers } from 'redux';
import TimerReducer from './TimerReducer';
import PacketReducer from './PacketReducer';
import MainState from './MainState';
import BleReducer from './BleReducer';
import UiState from './UiState';
import CheckItemsReducer from './CheckItemsReducer';
import HistoryReducer from './HistoryReducer';
import BreathReducer from './BreathReducer';
import ExamReducer from './ExamReducer';
import OperateBPReducer from './OperateBPReducer';
import EttSettingReducer from './EttSettingReducer';

export default combineReducers({
  main: MainState,
  exam: ExamReducer,
  uxhistory: HistoryReducer,
  chkItems: CheckItemsReducer,
  timerCount: TimerReducer, // 여기서 상태를 리턴함.
  arrPacket: PacketReducer, // 패킷 어레이..
  bleRdcr: BleReducer,
  uiState: UiState,
  breath: BreathReducer,
  OperBP: OperateBPReducer,
  EttSetting: EttSettingReducer,
});
