/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git

  src / reducers / BreathLogic.js

 * Created by Jongwoo Moon on 2017. 2. 20..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';

const maximumVol = 1000, minimumVol = 600;

export default class BreathLogic {


  constructor() {
    this.arrVolumes = [];
    this.state = {
      isIncreasing: true,
      finished: false,
      // thHold, countMin,
      stateID: 0, // 0, 300, 500...  -500, -300,
      maxVol: 0,
      started: new Date()
    };
  }

  finalProcess() {
    console.log(`finalProcess : max :: ${maxVol}`);
    const ended = new Date();
    const { started, maxVol } = this.state;
    const periodInSec = _.round((ended - started) * 0.001, 1);
    this.state = { ...this.state, ended, periodInSec, finished: true };
    this.showMyself();
  }

  showMyself() {
    const { started, ended, peakTime, maxVol, stateID, periodInSec } = this.state;
    console.log(`======= BreathLogic :: showMyself  ${started} ~ ~ ${ended}`);
    console.log(`  peakTime : ${peakTime}    maxVol : ${maxVol}  ${typeof started}`);
    console.log(` stateID : ${stateID}  total : ${periodInSec} exhale: ${this.getExhaleSec()} `);
  }

  getExhaleSec() {
    const { peakTime, ended } = this.state;
    return _.round((ended - peakTime) * 0.001, 1); // 1.5
  }

  getTotalSec() {
    const { started, ended } = this.state;
    console.log(` ${started}  ${ended}  ${ended - started}`);
    return _.round((ended - started) * 0.001, 1); // 3.5
  }

  process(thHold, countMin, vol) {
    //console.log(` process with ${thHold} ${countMin}    vol : ${vol}`);
    this.arrVolumes = [...this.arrVolumes, vol]; // 추가..
    const { stateID, maxVol, isIncreasing } = this.state;

    //console.log(`  stateID : ${stateID}     vol ${vol} maxVol  ${maxVol}`);

    switch (stateID) {
      case 0:
        if (vol < thHold * 0.4) { // 시작 시점을 호흡 막 들어가는 때로..
          this.state.started = new Date();
        }
        if (thHold < vol) {
            this.state.stateID = 300;
        }
        break;
      case 300: // thHold를 넘어선 상태
        if (countMin < vol) {
            this.state.stateID = 500;
        }
        if (vol < thHold) { // 다시 내려갈 땐 초기화..
          this.state.started = new Date();
          this.state.stateID = 0;
        }
        break;
      case 500: // 카운트 대상..
        if (countMin < vol) {
            this.state.stateID = 700;
        }
        this.state.ended = new Date();
        break;
      case 700: // 하강 체크 시점..
        console.log(` vol ${vol} maxVol  ${maxVol}`);
        if (vol < maxVol) {
          this.state.isIncreasing = false;
          this.state.stateID = -500;
          console.log('  여기서 내려감.. ');
        }
        this.state.ended = new Date();
        break;
      case -500:
        if (vol > maxVol) { // 다시 상승할 때
          this.state.stateID = 700;
          console.log('  재 상승..    ');
        }
        if (thHold * 0.4 > vol) {
          this.finalProcess();
          return true;  // /////////////  /////////////  /////////////   한 사이클 완료...
        }
        break;
      default:
        break;
    }

    if (maxVol < vol) {
      this.state.maxVol = vol;
      this.state.peakTime = new Date();
    } // 최대값 체크..

    return false;
  }
}
