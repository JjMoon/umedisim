/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / ExamReducer.js
 *
 * Created by Jongwoo Moon on 2017. 2. 23..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';
import { HtLogicalRandom, HtPacketGenerator, RmExam, ExamRlmObj } from '../model';

const initState = {
  isPlaying: false, curExam: null, examList: ExamRlmObj.objects('RmExam')
};

export default (state = {}, action) => {
  switch (action.type) {
    case 'init':
      //const examList = ExamRlmObj.objects('RmExam');
      console.log(`init in Exam Reducer ....examList.length  ${initState.examList.length}`);
      return initState; // { isPlaying: false, curExam: null, examList }; // 초기 상태 ..
    case 'deleteExam':
      action.payload.deleteMyself();
      return { ...state, examList: ExamRlmObj.objects('RmExam') };
    case 'startStopExamin':
      console.log(`\n\n\n\n\n ====== ====== ====== ======  [[ startStopExamin :: start ? ${action.payload} ]] ...\n`);
      if (!action.payload) { // 마무리..
        return { ...state, isPlaying: false };



        //_.last(state.examList).deleteMyself(); // 지우고 리턴..
        //return { ...state, isPlaying: false, examList: ExamRlmObj.objects('RmExam') };
      }
      // 새로운 시작.
      const nowTs = new Date();
      const newName = `M${nowTs.getMonth() + 1}/${nowTs.getDate()} || ${nowTs.getHours()}:${nowTs.getMinutes()} `;
      RmExam.newExam(newName);
      const curExam = _.last(state.examList);
      return { ...state, isPlaying: true, curExam };
    case 'resetCurExam':
      if (state.curExam) {
        state.curExam.deleteMyself();
      }
      return { ...state, isPlaying: false, curExam: null };
    case 'newBlePacket':
      if (!state.isPlaying || state.curExam === null) { return state; }
      //console.log(`PacketReducer.js || Packet Number >> ${state.length}`);
      state.curExam.addPacket(action.payload);
      return state;
    case 'addHistory': // 매뉴얼 체크 할 때 발생하는 액션..
      if (!state.isPlaying || state.curExam === null) { return state; }
      state.curExam.addPacket(action.payload, false);
      //history = [...history, { ...action.payload, ts: new Date() }];
      return state; // { ...state, history };
    default:
      return state;
  }
};
