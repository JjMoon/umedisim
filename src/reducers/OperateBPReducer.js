/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / OperateBPReducer.js
 *

 * Created by Jongwoo Moon on 2017. 4. 4..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';
import BpResultReal from '../model/BpResultReal';
import BpResultTraining from '../model/BpResultTraining';
import { BpPrsrLogic } from '../model';

const initState = {
  isPlaying: false, mode: 'NONE', bpLogic: null, // 'TRAIN'  'REAL'
  prsrLogic: null,
  playObj: {
    startedAt: new Date(), playTmStr: '0', finAt: new Date(), // 2:35 sec
    presMax: 0, presMin: 0, bpm: 0, gapExist: false
  },
  setting: { real: null, train: null }, // Operation View 들어갈 때 세팅
  curBpVal: { high: 0, low: 0, bpm: 0, periodGap: null }, // 시험지
  curAnswr: { high: 0, low: 0, bpm: 0, periodGap: false }, // 답안지
  answ: { presMax: 0, presMin: 0, gapExist: false, bpm: 0 }, // 답안 값.
  ux: {
    touch: false, cuff: false, pressure: 0, prsrDescRate: 0,
    //pulseSense: 1, soundSense: 1
  },
  uxOnce: { touched: { v: false, ts: null }, cuffed: { v: false, ts: null },
    maxPres: { v: 0, ts: null }, bpDepressFail: { v: false, ts: null } },
  newResult: [], // real or train ...
  packets: []
};

const testState = {
  isPlaying: false, mode: 'REAL', // 'TRAIN'  'REAL'
  prsrLogic: null,
  playObj: {
    startedAt: new Date(), playTmStr: '0', finAt: new Date(), // 2:35 sec
    presMax: 145, presMin: 90, bpm: 89, gapExist: true
  },
  setting: { real: null, train: null }, // Operation View 들어갈 때 세팅
  answ: { presMax: 155, presMin: 80, gapExist: false, bpm: 70 }, // 답안 값.
  ux: {
    touch: false, cuff: false, pressure: 0, prsrDescRate: 0,
    //pulseSense: 1, soundSense: 1
  },
  uxOnce: { touched: false, cuffed: true, maxPres: 250, bpDepressFail: true },
  newResult: [], // real or train ...
  packets: []
};


// train setting :
// method: 0, pulseSense: 1, soundSense: 1, period: 1, examTime: 3, pressStandard: 100,
// real setting :
// bpLevel: 0, method: 0, pulseSense: 1, soundSense: 1, period: 1, // picker related...
// bloodShrink: 10, bloodRelax: 10, pulseNum: 5, examTimeMin: 3, pressStandard Val: 25,
// arrScore: [10, 10, 10, 10, 10, 10, 10, 10, 10]

// 트레이닝 시 + - 버튼 누르면 패킷 보내야 함.

// 평가 시 적용되는 상수값들..
const stdr = { depressRate: 5, maxPressOverLimit: 30 };
const realLogicObj = new BpResultReal(), trainLogicObj = new BpResultTraining();

export default (state = initState, action) => {
  const { isPlaying, mode, ux, uxOnce, packets, playObj, curBpVal, prsrLogic, setting } = state;
  const cnt = packets.length;
  const curSetObj = (mode === 'REAL') ? setting.real : setting.train;

  switch (action.type) {
    case 'init':
      return state;
    case 'propagateBP':
      console.log('propagateBP');
      return { ...state, bpLogic: action.payload };
    // case 'bpStartTest': //
    //   console.log(testState);
    //   console.log(setting);
    //   return realLogicObj.getResultScore({ ...testState, setting });
    case 'setBpMode': // 실행화면 들어옴..  초기값 세팅..
      console.log(`\n\n\n setBpMode :: ${action.mode}   \n\n\n`);
      return { ...state, mode: action.mode };
    case 'setBpValues':
      console.log('\n\n\n setBpValues  혈압 / 펄스 값 등 랜덤으로 값 설정.. curBpVal 에 담겨 있슴. :: ');
      console.log(action.payload);
      return { ...state, curBpVal: action.payload };
    case 'setBpAnswr':
      console.log(action.payload);
      return { ...state, curAnswr: { ...state.curAnswr, ...action.payload } };
    case 'setBpSetting':
      console.log(action.payload);
      const newSetting = { ...state.setting, ...action.payload };
      return { ...state, setting: newSetting };
    case 'bpPlayOrStop': // action.start : true > play
      if (action.start) { // 시작..
        return { ...state, isPlaying: action.start, packets: [],
          prsrLogic: new BpPrsrLogic(curBpVal.high, curBpVal.low),
          ux: initState.ux, uxOnce: initState.uxOnce,
          curAnswr: { high: 0, low: 0, bpm: 0, periodGap: false }, // 답안지 초기화
          playObj: { ...playObj, startedAt: new Date() } };
      }
      return { ...state, isPlaying: action.start, playObj: { ...playObj, finAt: new Date() } };
    case 'bpPlayTick':
      if (!isPlaying) {
        return { ...state, playObj: { ...playObj, playTmStr: '0' } };
      }
      const prda = periodMinSec(playObj.startedAt, new Date()); // 시간 계산
      console.log(prda);
      return { ...state, playObj: { ...playObj, playTmStr: prda } };
    case 'BpPacket':
    // { "pls" : 90, "prsr": 160, "plsItnst": 1, "sound": 4, "sndItnst": 5,
    //   "cff": true, "tch": [false, false], "gap": false },
      let newUxOnce = uxOnce;
      const newObj = addTimeStamp(action.payload);      //console.log(newObj);
      const { tch, cff, prsr } = action.payload;
      // 기본 파싱.
      const touchOr = (tch[0] || tch[1]); // 촉진 값은 두개의 OR 를 취한다..
      const newUX = { ...ux, touch: touchOr, cuff: cff, pressure: prsr };

      // 한번만 체크하면 되는 사항 처리...  Time Stamp 남겨야 함...
      const touched = checkBoolOnce(uxOnce.touched, touchOr); //  촉진....
      const cuffed = checkBoolOnce(uxOnce.cuffed, cff); // 커프....
      // 최대 값 저장
      const maxPres = checkMaxValueOnce(uxOnce.maxPres, prsr);

      // 감압 속도 체크
      if (cnt % 3 === 1) {
        newUX.prsrDescRate = calcDescreaseRate([...packets, newObj]); // 감압 속도
        const fail = (newUX.prsrDescRate > 5);
        newUxOnce.bpDepressFail = checkBoolOnce(uxOnce.bpDepressFail, fail); // 할당.
      }
      if (prsrLogic !== null) { prsrLogic.processPressure(prsr); } // 로직 객체 처리...
      newUxOnce = { ...newUxOnce, touched, cuffed, maxPres };
      return { ...state, ux: newUX, uxOnce: newUxOnce, packets: [...packets, newObj] };
    case 'bpFinishExam':
      console.log('\n\n\n bpFinishExam  BP 종료.. getResultScore 호출..');
      prsrLogic.finalize();
      if (mode === 'REAL') { return realLogicObj.getResultScore(state); }
      if (mode === 'TRAIN') { return trainLogicObj.getResultScore(state); }
      return state; // NONE case ...
    default:
      return state;
  }
};

function checkMaxValueOnce(obj, newValue) {
  const { v } = obj;
  if (v > newValue) { return obj; }
  return { v: newValue, ts: new Date() };
}

function checkBoolOnce(obj, newValue) {
  const { v } = obj;
  if (v) { return obj; }
  if (newValue) {
    console.log('\n\n\n 새로운 히스토리 생성 \n\n\n');
    return { v: true, ts: new Date() };
  }
  return obj;
}

function hasBeenTrue(curBool, prevBool) {
  return curBool ? true : prevBool; // 한번 true면 되돌아가지 않음..
}

function calcDescreaseRate(packs) {
  if (packs.length < 5) {
    return 0;
  }
  const objs = _.slice(packs, -5, packs.length)
  const sT = new Date(objs[0].ts), eT = new Date(objs[4].ts);
  const prd = eT.getTime() - sT.getTime();
  if (prd < 10) {
    console.log('  Error 시간 차이가 너무 적음..');
    return 0;
  }
  const rVal = (objs[0].prsr - objs[4].prsr) / (prd / 1000)
  if (rVal < 0) { return 0; }
  //console.log(` 감소 .      ${objs[0].prsr}  ${objs[1].prsr}       ${objs[4].prsr}`);
  //console.log(` ... ${objs[0].ts}   ${objs[4].ts}  ${rVal} `);
  return rVal.toFixed(2);
}

function addTimeStamp(pObj) {
  const dateNow = new Date();
  return { ...pObj, tsObj: dateNow, ts: dateNow.toString() };
}

function period(from, to) {
  return (to.getTime() - from.getTime()) / 1000;
}

function periodMinSec(from, to) {
  const ttlSec = period(from, to);
  const min = Math.round(ttlSec / 60), sec = Math.round(ttlSec % 60);
  if (min === 0) {
    return `${sec}`
  }
  return `${min}:${sec}`
}

/*
const initBpRealTestSetting = {
  bpLevel: 0, method: 0, pulseSense: 1, soundSense: 1, period: 1, // picker related...
  bloodShrink: 10, bloodRelax: 10, pulseNum: 5,
  examTimeMin: 3, pressStandard Val: 5,
  arrScore: [10, 10, 10, 10, 10, 10, 10, 10, 10]
};

const initBpTrainSetting = {
  method: 0, pulseSense: 1, soundSense: 1, period: 1,
  examTime: 3, pressStandard: 100,
};

*/
