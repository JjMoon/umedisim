/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / EttSettingReducer.js
 *
 * Created by Jongwoo Moon on 2017. 4. 4..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import _ from 'lodash';

const initState = {
  autoCheckItem: {}, manCheckItem: [], general: []
};

export default (state = initState, action) => {
  const { autoCheckItem, manCheckItem, general } = state;

  switch (action.type) {
    case 'setEttCheckItemValues': // 첫 화면에서 세팅.
      console.log('  HERE :: setEttCheckItemValues  .... ');
      console.log(action.payload);
      return { ...state, ...action.payload };
    case 'editAutoScoreActive': // {  "totalTime": { "score": 10, "active": true },
      console.log('editAutoScoreActive');
      console.log(action.payload);
      return { ...state, autoCheckItem: { ...autoCheckItem, ...action.payload } };
    case 'editManualScoreActive': // {  "totalTime": { "score": 10, "active": true },
      console.log('editManualScoreActive');
      console.log(action.payload);
      const { idx } = action.payload;
      const nArr = _.fill(manCheckItem, action.payload, idx, idx + 1);
      return { ...state, manCheckItem: nArr };
    case 'editCaliSetting':
      console.log(action.payload);
      const obj = _.filter(general, (o) => { return action.payload.key === o.key; })[0];
      const ix = _.indexOf(general, obj);
      const narr = _.fill(general, action.payload, ix, ix + 1);
      return { ...state, general: narr };
    default:
      return state;
  }
};
