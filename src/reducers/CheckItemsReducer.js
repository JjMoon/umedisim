/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / CheckItemsReducer.js
 *
 * Created by Jongwoo Moon on 2017. 2. 15..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.

 */
'use strict';

// { "shr": "폐 팽창", "long": "양쪽 폐가 팽창하는가?", "score": 10, "auto": false },

export default (state = [], action) => {
  switch (action.type) {
    case 'clearCheckList':
      console.log('\t\t\t  ===== ===== ===== =====     clearCheckList  ');
      return [];
    case 'addCheckList':
      const newItem = { ...action.payload, expanded: false, pass: false, scoreMarked: 0, disable: false };
      //console.log(newItem);
      return [...state, newItem];
    // case 'expandChkList':
    //   //console.log(action.payload);
    //   for (const itm of state) {
    //     if (itm.shr === action.payload) {
    //       if (itm.expanded) {  // 열려 있으면..
    //         itm.expanded = false;  // 닫는다.
    //       } else {
    //         itm.expanded = (itm.shr === action.payload);
    //         console.log(` itm.expanded ::  ${itm.expanded}`);
    //       }
    //     }
    //     console.log(`\n\n expandChkList :: ${action.payload}   ${itm.expanded} \n\n`);
    //   }
    //   return [...state];
    default:
      return state;
  }
};
