/**
 * Bit Src Project / UMediSim
 * git@bitbucket.org:JjMoon/umedisim.git
 *
 * src / reducers / BleReducer.js
 *
 * Created by Jongwoo Moon on 2017. 4. 12..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

const initState = {
  iIOS: false, iETT: false, ntvObj: null,
  conState: 'None', // Scanning, Connected
};

export default (state = initState, action) => {
  switch (action.type) {

    case 'startScan': // (iIOS, iETT, ntvObj, dN)
      const hdr = (action.iETT) ? 'MediETT' : 'MediBP';
      if (action.iIOS) {
        console.log(' start scan ios in ruducer');
        action.ntvObj.startScan(`${hdr}_${action.dN}`); // ios 는 이름을 여기서 조합.
      } // android 는 BleManView 에서 처리..
      return { ...state, iIOS: action.isIOS, iETT: action.iETT, ntvObj: action.ntvObj, conState: 'Scanning' };

    case 'setBleState': //  = (stt) => {'
      console.log(`\n\n BLE State 변경 ${action.stt} \n\n`);
      return { ...state, conState: action.stt };
    default:
      return state;
  }
};
