/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneExam / ReplayStatusVw.js
 *
 * Created by Jongwoo Moon on 2017. 3. 10. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import * as RnProgress from 'react-native-progress';
import RnSpinner from 'react-native-spinkit';
import * as actions from '../actions';
import { HtText, HtRectBttn, HtColor } from '../uicompo';

class ReplayStatusVw extends Component {
  /*
  replay: {  상태 구조.
    exam: null, idx: 0,
    examStarted: 0, replayStarted: 0, // 시작 시간.
    lagTime: 0, curPacketLag: 0
  }
  */

  renderDefault() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <HtText
          mandator={{ flex: 10, rad: 5, text: '재생할 실습을 선택하세요', margin: { t: 5, b: 8 } }}
          style={{ viewStyle: sty.vw, textStyle: sty.txt }}
        />
      </View>
    );
  }

  // measureView(event) {
  //   // onLayout={(event) => this.measureView(event)}
  //   console.log('event peroperties: ', event);
  //   const { x, y, width, height } = event.nativeEvent.layout;
  //   console.log(`measure >> ${width}  ${height}`);
  //   this.setState({
  //     x, y, width, height
  //   });
  // }

  stopPlay() {
    console.log('stop play');
    this.props.examReplayStop();
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const { exam, idx } = this.props.uxhistory.replay;
    if (exam === null) {
      return this.renderDefault();
    }
    const packetNum = exam.packets.length, percent = Math.round((idx / packetNum) * 100);

    // types: ['CircleFlip', 'Bounce', 'Wave', 'WanderingCubes', 'Pulse',
    // 'ChasingDots', 'ThreeBounce', 'Circle', '9CubeGrid', 'WordPress',
    // 'FadingCircle', 'FadingCircleAlt', 'Arc', 'ArcAlt'],
    return (
      <View style={sty.mainContainer}>
        {/* -------------------------  -------------------------  프로그레스, 스피너 .. ... */}
        <View style={sty.progress}>
          <RnSpinner
            style={{ flex: 10, margin: 10, marginBottom: 25 }}
            isVisible
            size={50}
            type={'ThreeBounce'}
            color={'#ACD'}
          />
          <RnProgress.Bar
            style={{ flex: 50 }}
            height={20}
            width={this.props.main.scr.width * 0.32}
            progress={idx / packetNum}
          />
        </View>
        {/* -------------------------  -------------------------  34 % .. ... */}
        <HtText
          mandator={{ flex: 10, rad: 5, text: `${percent} %`, margin: { l: 5, r: 5 } }}
          style={{ viewStyle: sty.vw, textStyle: sty.txt }}
        />

        <HtRectBttn
          len={40}
          borderR={5}
          bgCol={HtColor.bgBlue}
          txtCol={'#EEFFEE'}
          txtRatio={0.3}
          ptext={'멈춤'}
          onPressCallback={this.stopPlay.bind(this)}
        />
      </View>
    );
  }
}

/*
회전 스피너 예..
<View style={{ width: circleSize, height: circleSize }} >
  <RnProgress.CircleSnail
    animating
    size={circleSize - 10}
    thickness={5}
    duration={1000}
    spinDuration={2000}
  />
</View>

*/

const sty = StyleSheet.create({
  mainContainer: {
    flex: 1, flexDirection: 'row',
    alignItems: 'stretch', justifyContent: 'center',
    padding: 10
  },
  progress: {
    flex: 50, flexDirection: 'row',
    alignItems: 'center', justifyContent: 'center',
  },
  vw: { backgroundColor: 'white', margin: 3 },
  txt: { color: HtColor.bgDarkGray, fontWeight: '200', fontSize: 18 },
});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    uxhistory: state.uxhistory, // HistoryReducer
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(ReplayStatusVw);
