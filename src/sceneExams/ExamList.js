/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneExams / ExamList.js
 *
 * Created by Jongwoo Moon on 2017. 2. 25. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */

import React, { Component } from 'react';
import { StyleSheet, ListView, LayoutAnimation } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtColor, HtRectBttn } from '../uicompo';
import ExamCell from './ExamCell';

// const signs = require('./HistorySigns.json');

class ExamList extends Component {
  componentWillUpdate() {
    LayoutAnimation.easeInEaseOut(); //spring();
  }

  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.time = new Date();
    this.state = {
        dataSource: ds.cloneWithRows(this.props.exam.examList),  //main.actions),
    };
  }

  getDataSource(datum) {
     return this.state.dataSource.cloneWithRows(datum);
  }

  renderRow(dt) {
    //console.log(dt.showMyself());
    return (
      <ExamCell item={dt} />
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    this.time = new Date();
    return (
      <ListView
        style={sty.container}
        enableEmptySections
        dataSource={this.getDataSource(this.props.exam.examList)}
        renderRow={this.renderRow.bind(this)}
      />
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    backgroundColor: '#FAFFFF',
    borderColor: '#EAEFEF', borderWidth: 2,
  },
  rowcontainer: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 5,
    margin: 3
  },
  title: {
    flex: 5,
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '500',
    color: HtColor.txtDarkGray,
    margin: 5
  },
  score: {
    textAlign: 'center',
    fontSize: 13,
    fontWeight: '400',
    color: HtColor.txtScoreRed,
    margin: 2
  },
  normal: {
    textAlign: 'center',
    fontSize: 13,
    fontWeight: '100',
    color: HtColor.txtDarkGray,
    margin: 1
  }
});

const mapStateToProps = state => {
  return {
    main: state.main,
    uxhistory: state.uxhistory,
    exam: state.exam
  };
};

export default connect(mapStateToProps, actions)(ExamList);
