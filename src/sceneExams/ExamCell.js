/**
 * Bit Src Project / Bleu Men
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneExams / ExamCell.js
 *
 * Created by Jongwoo Moon on 2017. 2. 25. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { HtColor, HtRectBttn } from '../uicompo';

/// 실행 화면에서 실습 중에 체크 아이템...
class ExamCell extends Component {

  state = {
    name: this.props.item.examName,
    expanded: false,
    disable: false,
  };

  getDataSource(datum) {
     return this.state.dataSource.cloneWithRows(datum);
  }

  bttnPlayClick() {
    console.log('clicked ....');
    this.props.examReplay(this.props.item);
  }

  // 내 객체를 삭제.
  deleteButtonClick() {
    // this.props.item.deleteMyself();
    this.props.deleteExam(this.props.item);
  }

  changeName(txt) {
    this.setState({ name: txt });
    this.props.item.setName(txt);
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    const backgroundColor = '#EEEAF5';
    const totalScore = this.props.item.totalScore();
    const durationSec = this.props.item.examDuration();
    // 점수 : {totalScore}, 패킷 : {this.props.item.packets.length}

    return (
      <View style={sty.container}>
        <View style={[sty.rowcontainer, { flex: 8, backgroundColor }]}>
          <TextInput
            style={[sty.title, sty.grayBoarder]}
            onChangeText={(text) => this.changeName(text)}
            value={this.state.name}
          />

        </View>

        <View style={[sty.rowcontainer, { flex: 9 }]}>
          <Text style={sty.score}>
            점수 : {totalScore}, 시간 : {durationSec} 초
          </Text>
          <HtRectBttn
            len={30}
            borderR={4} padding={0} margin={0}
            bgCol={HtColor.bgBlue}
            txtCol={'#EEFFEE'}
            txtRatio={0.4}
            ptext={'재생'}
            onPressCallback={this.bttnPlayClick.bind(this)}
          />

          <HtRectBttn
            len={30}
            borderR={4} padding={0} margin={0}
            bgCol={'#800'}
            txtCol={'#EEFFEE'}
            txtRatio={0.4}
            ptext={'삭제'}
            onPressCallback={this.deleteButtonClick.bind(this)}
          />
        </View>
      </View>
    );
  }
}

///-------------------------   -------------------------     ### custom style ###
const sty = StyleSheet.create({
  container: {
    flex: 1, alignSelf: 'stretch',
    backgroundColor: '#EEEAF5',
    borderColor: '#EAEFEF', borderWidth: 0,
    margin: 3,
    height: 80
  },
  rowcontainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
    margin: 1
  },
  title: {
    flex: 1,
    alignSelf: 'stretch',
    fontSize: 15,
    fontWeight: '500',
    color: HtColor.txtDarkGray,
    backgroundColor: '#FFF',
    marginLeft: 5, marginRight: 5
  },
  grayBoarder: {
    borderWidth: 1,
    margin: 1,
    borderColor: 'gainsboro', //(#dcdcdc)
    borderRadius: 5
  },
  score: {
    flex: 10,
    textAlign: 'center',
    fontSize: 13,
    fontWeight: '400',
    color: HtColor.txtScoreRed,
    margin: 2
  },
  normal: {
    textAlign: 'center',
    fontSize: 13,
    fontWeight: '100',
    color: HtColor.txtDarkGray,
    margin: 1
  }
});

const mapStateToProps = state => {
  return {
    main: state.main,
    uxhistory: state.uxhistory
  };
};

export default connect(mapStateToProps, actions)(ExamCell);
