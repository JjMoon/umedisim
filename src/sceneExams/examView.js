/**
 * Bit Src Project / MediSimETT
 * git@bitbucket.org:JjMoon/medisim_ett.git
 *
 * src / sceneExam / examView.js
 *
 * Created by Jongwoo Moon on 2017. 2. 25. ..
 * Copyright © 2017년 Jongwoo Moon. All rights reserved.
 */
'use strict';

import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { connect } from 'react-redux';
//import { Actions } from 'react-native-router-flux';
import Realm from 'realm';
import * as actions from '../actions';
import TitleBarOperate from '../sceneOper/TitleBarOperate';
import ControlBar from '../sceneOper/ControlBar';
import ImageSection from '../sceneOper/ImageSection';
import * as styl from '../uicompo/HtStyles';
import HistoryList from '../simulations/HistoryList';
import Progress from '../simulations/Progress';
import CheckItemsList from '../sceneOper/CheckItemsList';
import { RealmGObj, AppMetaData } from '../model';
import ExamList from './ExamList';
import ReplayStatusVw from './ReplayStatusVw';

class SceneExams extends Component {
  constructor(props) {
    super(props);
    this.interval = setInterval(this.tickRealTime.bind(this), 100);
  }

  // 이 타이머는 계속 돌며.. exam 이 있을 때는 플레이 수행..
  tickRealTime() {
    //console.log('\n --------------------------------   ---------   --------- >> Tick  Real Time ....');
    const { replay } = this.props.uxhistory;
    if (replay.exam === null) { // replay.exam === undefined
      return;
    }
    if (replay.exam.packets.length === 0) {
      return;
    }
    this.tickPacketProcess(replay);
  }

  tickPacketProcess(replay) {
    const { lagTime, curPacketLag, exam, examStarted, replayStarted } = this.props.uxhistory.replay;
    const { isPacket, contentJson } = exam.packets[replay.idx];

    // <<<<  리플레이 관견 시간 :: in Milliseconds  >>>>
    // examStarted :: exam 시작 시간. (과거의 어느 시점)
    // replayStarted :: 리플레이 시작한 시간. (버튼 누른 시간)
    // lagTime:: 현재 시간 - replayStarted
    // curPacketLag :: 현재 패킷 시간 - examStarted

    this.props.replayTick();

    //console.log(` 시간 비교 :: ${lagTime} > ${curPacketLag} ${examStarted} ${replayStarted} `);

    if (lagTime > curPacketLag) {
      //console.log(` 패킷 실행 ...   ${replay.idx} ${exam.packets.length} `);
      const pck = JSON.parse(contentJson);
      this.packetProcess(isPacket, pck);
    }
  }

  tickFunction() {
    console.log('\n ------------------------------------------------------- >> Tick   in .... examView ....  디버깅 용 임시 tick');
    const { replay } = this.props.uxhistory;

    if (replay.exam === null) {
      return;
    }
    const { isPacket, contentJson } = replay.exam.packets[replay.idx];
    const pck = JSON.parse(contentJson);

    this.packetProcess(isPacket, pck);
  }

  packetProcess(isPacket, pck) {
    if (isPacket) {
      this.props.newBlePacket(pck);

      this.props.changeNeckAngle(pck.nckAng);
      this.props.changeChinPush(pck.glcnbl[1]);
      this.props.changeNeckHeight(pck.nckDst);
      this.props.checkTeeth(pck.tth);
      this.props.checkBronchus(pck.thr[2]);
      this.props.checkBelly(pck.glcnbl);
    } else {
      //console.log(pck);
      // { "shr": "기록 명령", "long": "수행한 술기를 의말하는가?", "score": 10, "auto": false }
      this.props.addHistory(pck);
    }
    this.props.examReplayNext();
  }

  ////////////////////////////////////////////////////   _//////////////////_   component life cycle
  componentDidMount() {
    this.props.examListViewMode(true);
    this.props.changeMainState('exam');
  }

  componentWillUnmount() {
    clearInterval(this.interval); // 타이머 죽이기
    this.props.startStopExamin(false);
  }

  componentWillUpdate() {
    //LayoutAnimation.easeInEaseOut(); //spring(); // easeInEaseOut();
  }

  getOperationView() {
    const myFlex = 75;
    return (
      <View style={[styl.rowContainer, { flex: myFlex }]}>
        <View style={{ flex: 40 }} >
          <ExamList />
        </View>

        <View style={{ flex: 100 }}>
          <View style={{ flex: 10 }}>
            <ReplayStatusVw />
          </View>

        <ImageSection
          flex={90}
        />
        </View>

        <View style={{ flex: 40 }} >
          <HistoryList />
        </View>

      </View>
    );
  }

  ////////////////////////////////////////////////////   _//////////////////_   render
  render() {
    return (
      <View style={{ marginTop: 1, flex: 100 }}>
        <TitleBarOperate
          flex={10}
          title={'실습 재실행'}
          examCase
        />
        <View style={sty.seperateLine} />
        {/* -------------------------  -------------------------  분리선.. ... */}
        {this.getOperationView()  /* { flex: 80 } */}
        {/* -------------------------  -------------------------  분리선.. ... */}
        <View style={sty.seperateLine} />


      </View>
    );
  }
}

////////////////////////////////////////////////////   _//////////////////_     style
const sty = StyleSheet.create({
  mainContainer: {
    alignItems: 'stretch', justifyContent: 'center', flex: 1, margin: 0, marginTop: 20
  },
  container: {
    alignItems: 'stretch',
    justifyContent: 'center',

  },
  imageView: {
    backgroundColor: '#389',
    alignSelf: 'stretch',
  },
  historySection: {
    backgroundColor: '#ABC',
    alignSelf: 'stretch',
  },
  seperateLine: {
    height: 2, backgroundColor: '#AAAAAA'
  },

});

const mapStateToProps = (state) => {
  return {
    main: state.main,
    theCounter: state.timerCount,
    uxhistory: state.uxhistory, // HistoryReducer
    uiState: state.uiState
  };
};

export default connect(mapStateToProps, actions)(SceneExams);
