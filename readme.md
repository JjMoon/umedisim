Link : https://bitbucket.org/JjMoon/umedisim/src/6e724d2b8bf8465d2546bd7c30f7408d01395c4f/readme.md?at=develop&fileviewer=file-view-default
# ETT Logic

### Packet

Look for >> Readme:Packet:ETT:Sample

기도1 홀센서 전압 (mV)	= atof(buf[0]);
기도2 홀센서 전압 (mV)	= atof(buf[1]);
기도3 홀센서 전압 (mV)	= atof(buf[2]);
식도 홀센서 전압  (mV)	= atof(buf[3]);
목각도가변저항 전압(mV) = atof(buf[4]);
목거리 센서 전압  (mV)	= atof(buf[5]);
복부 거리센서 전압 (mV)	= atof(buf[6]);
턱 홀센서 전압    (mV) = atof(buf[7]);
배 홀센서 전압    (mV) = atof(buf[8]);
이빨 면압센서 전압 (mV) = atoi(buf[9]);

```json
# Parsed Packet Format
{ blly: 110, glcnbl: [true, false, true], nckAng: 3, nckDst: 30, thr: [false, true, false], tth: 27 }
```

## Ett Logic

모델 클래스 (Model / EttAutoCheck) 를 UiState Reducer 에서 UI 상태로 만들어서 View 에서 렌더링.
